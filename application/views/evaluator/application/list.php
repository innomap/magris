<script type="text/javascript">
	var alert = "<?=$alert?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 table-responsive">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		
		<form id="form-list-evaluation" action="<?= base_url().PATH_TO_EVALUATOR.'applications/approve' ?>" method="post">
			<table id="table-application" class="table table-bordered table-striped table-dark">
			    <thead>
				    <tr>
						<th>No</th>
						<th><?= lang('team_leader') ?></th>
						<th><?= lang('project_title') ?></th>
						<th><?= lang('description') ?></th>
						<th><?= lang('status') ?></th>
						<th><?= lang('action') ?></th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($applications as $value) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $value['innovator'] ?></td>
				    <td><?= $value['title'] ?></td>
				    <td><?= ellipsis($value['description'], 100) ?></td>
				    <td class="<?= $status[$value['status']]['class'] ?>"><?= $status[$value['status']]['name'] ?></td>
				    <td>
						<button type="button" class="btn btn-default btn-action btn-view-application" title="<?= lang('view_detail') ?>" data-id="<?= $value['application_id'] ?>">
						    <span class="fa fa-search fa-lg"></span>
						</button>

						<?php if(!$value['has_evaluation_id'] || !$value['is_complete']){ ?>
							<button type="button" class="btn btn-default btn-action btn-evaluate-application" title="<?= lang('evaluate') ?>" data-id="<?= $value['application_id'] ?>">
							    <span class="fa fa-edit fa-lg"></span>
							</button>
						<?php }else{ ?>
							<button type="button" class="btn btn-default btn-action btn-view-evaluation" title="<?= lang('view_evaluation_result') ?>" data-id="<?= $value['application_id'] ?>">
							    <span class="fa fa-file-text-o fa-lg"></span>
							</button>
						<?php } ?>
						
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>
		</form>
    </div>
</div>