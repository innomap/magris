<div class="col-md-12">
	<div class="col-md-9 no-padding padding-right-5">
		<a href="<?= base_url() ?>">
			<img src="<?= base_url().ASSETS_IMG."header.jpg" ?>" width="100%">
		</a>
	</div>
	<div class="col-md-3 login-box-blue">
    <?php   if(!$this->ol_user){
	           $this->load->view('account/form_login',array('alert' => $alert));
            }else{ ?>
                <div class="col-md-12 margin-top-20">
                    <label>User Info</label><br/>
                    <label>Nama : <?= $this->ol_user['name'] ?></label>
                </div>
                <div class="col-md-12 text-right">
                    <a href="<?= base_url().'accounts/logout' ?>" class="btn btn-default">Logout</a>
                </div>
    <?php   } ?>
	</div>
</div>
<div class="col-md-12">
	 <nav class="menu navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar-header" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= $this->submenu == "intro" ? "active" : "" ?>"><a href="<?= base_url().'intro' ?>">Pengenalan</a></li>
                <li class="<?= $this->submenu == "registration" ? "active" : "" ?>"><a href="<?= base_url().'registration' ?>">Pendaftaran</a></li>
                <li class="<?= $this->submenu == "activity" ? "active" : "" ?>"><a href="<?= base_url().'activity' ?>">Aktiviti</a></li>
                <li class="<?= $this->submenu == "term" ? "active" : "" ?>"><a href="<?= base_url().'term' ?>">Terma & Syarat</a></li>
                <li class="<?= $this->submenu == "faq" ? "active" : "" ?>"><a href="<?= base_url().'faq' ?>">Soalan Lazim</a></li>
                <li class="<?= $this->submenu == "contact" ? "active" : "" ?>"><a href="<?= base_url().'contact' ?>">Hubungi Sekretariat</a></li>
            </ul>
        </div>
    </nav>
</div>