<script type="text/javascript">
	var alert = "<?= $alert ?>",
		alert_dialog = "",
		lang_send_innovation_for_approval = "<?= lang('send_innovation_for_approval') ?>",
		lang_send_for_approval_confirm_message = "<?= lang('send_for_approval_confirm_message') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_send_for_approval = "<?= lang('send_for_approval') ?>",
		lang_yes = "<?= lang('yes') ?>",
		lang_application_submitted_msg = "<?= lang('application_submitted_msg') ?>";

	var view_mode = "<?= ($view_mode == 1 ? true : false) ?>";
</script>
<div class="col-md-8 col-md-offset-2">
	<div class="col-md-12 text-center margin-btm-50">
		<h2><?= lang('application_form') ?></h2>
	</div>

	<div class="col-md-12">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
	</div>

	<form id="form-application" class="form-horizontal" action="<?= base_url().$action_url ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= (isset($application) ? $application['id'] : 0) ?>">

		<?php if($view_mode == 1){ ?>
		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('submission_date') ?> *</label>
			<div class="col-md-9">
				<input type="text" class="form-control" placeholder="<?= lang('submission_date') ?>" value="<?= (isset($application) ? $application['submission_date'] : "") ?>">
			</div>
		</div>
		<?php } ?>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('team_leader') ?> *</label>
			<div class="col-md-9">
				<input type="text" class="form-control" placeholder="<?= lang('team_leader') ?>" value="<?= $innovator['name'] ?>" readonly>
				<input type="hidden" name="innovator_id" value="<?= $innovator['id'] ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('kp_no') ?> *</label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['kp_no'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('application') ?> *</label>
			<div class="col-md-9">
				<?php foreach ($application_type as $key => $value) { ?>
					<div class="radio"> 
						<label> <input type="radio" name="application_type" value="<?= $key ?>" <?= (isset($application) ? ($application['application_type'] == $key ? "checked" : "") : ($key == 0 ? "checked" : "")) ?>> <?= $value['name'] ?> </label> 
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('team_member') ?></label>
			<div class="col-md-9 team-member-wrap">
				<?php if(isset($application)){
						foreach ($i_team as $key => $value) { ?> 
							<input type="hidden" class="team-member-id" name="team_member_id[]" value="<?= $value['id'] ?>">
							<div class="col-md-11 team-member-item">
								<input type="hidden" name="i_deleted_team_member_<?= $key ?>">
								<div class="col-md-6">
									<input type="text"class="form-control" name="team_member_<?= $key ?>" placeholder="<?= lang('member_name') ?>" value="<?= $value['name'] ?>">
								</div>
								<div class="col-md-3">
									<input type="text"class="form-control" name="team_member_kp_<?= $key ?>" placeholder="<?= lang('kp_no') ?>" value="<?= $value['kp_no'] ?>">
								</div>
								<div class="col-md-3">
									<input type="text"class="form-control" name="team_member_telp_<?= $key ?>" placeholder="<?= lang('telp_no') ?>" value="<?= $value['telp_no'] ?>">
								</div>
							</div>
							<?php if($key == 0){ ?>
								<div class="col-md-1">
									<button type="button" class="btn btn-grey add-team-member"><span class="fa fa-plus"></span></button>
								</div>
				<?php 		}else{ ?>
								<div class="col-md-1">
									<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>
								</div>
				<?php		}
						}
					  }else{ ?>
					  	<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">
						<div class="col-md-11 team-member-item">	
							<input type="hidden" name="h_team_member_pic_0" value="">
							<input type="hidden" name="i_deleted_team_member_0">
							<div class="col-md-6">
								<input type="text"class="form-control" name="team_member_0" placeholder="<?= lang('member_name') ?>">
							</div>
							<div class="col-md-3">
								<input type="text"class="form-control" name="team_member_kp_0" placeholder="<?= lang('kp_no') ?>">
							</div>
							<div class="col-md-3">
								<input type="text"class="form-control" name="team_member_telp_0" placeholder="<?= lang('telp_no') ?>">
							</div>
						</div>
						<div class="col-md-1">
							<button type="button" class="btn btn-grey add-team-member"><span class="fa fa-plus"></span></button>
						</div>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('address') ?> *</label>
			<div class="col-md-9">
				<textarea class="form-control" readonly><?= $innovator['address'] ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('postcode') ?> *</label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['postcode'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('state') ?> *</label>
			<div class="col-md-9">
				<select name="state_id" class="form-control" disabled>
					<?php foreach ($states as $key => $value) { ?>
						<option value="<?= $value['id'] ?>" <?= (isset($innovator) ? ($innovator['state_id'] == $value['id'] ? 'selected' : '') : '') ?>><?= $value['name'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('country') ?> *</label>
			<div class="col-md-9">
				<select name="country_id" class="form-control" disabled>
					<?php foreach ($countries as $key => $value) { ?>
						<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('telp_no') ?> *</label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['telp_no'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('email') ?> *</label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['email'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('project_title') ?> *</label>
			<div class="col-md-9">
				<input type="text" name="title" class="form-control" placeholder="<?= lang('placeholder_project_title') ?>" value="<?= (isset($application) ? $application['title'] : "") ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('description') ?> *</label>
			<div class="col-md-9">
				<textarea class="form-control" name="description" rows="5" placeholder="<?= lang('placeholder_description') ?>"><?= (isset($application) ? $application['description'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('problem_encountered') ?> *</label>
			<div class="col-md-9">
				<textarea class="form-control" name="problems_encountered" rows="5" placeholder="<?= lang('placeholder_problem_encountered') ?>"><?= (isset($application) ? $application['problems_encountered'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('award_purpose') ?></label>
			<div class="col-md-9">
				<textarea name="award_purpose" class="form-control" rows="5" placeholder="<?= lang('placeholder_award_purpose') ?>"><?= (isset($application) ? $application['award_purpose'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('how_to_use') ?> *</label>
			<div class="col-md-9">
				<textarea class="form-control" name="how_to_use" rows="5" placeholder="<?= lang('placeholder_how_to_use') ?>"><?= (isset($application) ? $application['how_to_use'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovation_novelty') ?> *</label>
			<div class="col-md-9">
				<textarea class="form-control" name="innovation_novelty" rows="5" placeholder="<?= lang('placeholder_innovation_novelty') ?>"><?= (isset($application) ? $application['innovation_novelty'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovation_category') ?> *</label>
			<div class="col-md-9">
				<select name="categories[]" class="form-control">
				<?php foreach ($category_innovation as $key => $value) { ?>
					<option value="<?= $value['id'] ?>" <?= isset($application) ? ($i_categories['category_innovation_id'] == $value['id'] ? "selected" : "") : "" ?>><?= $value['name'] ?></option>
				<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('impact') ?> *</label>
			<div class="col-md-9">
				<textarea class="form-control" name="impact" rows="5" placeholder="<?= lang('placeholder_impact') ?>"><?= (isset($application) ? $application['impact'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('community_involvement') ?></label>
			<div class="col-md-9">
				<textarea name="community_involvement" class="form-control" rows="5" placeholder="<?= lang('placeholder_community_involvement') ?>"><?= (isset($application) ? $application['community_involvement'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('previous_grants') ?></label>
			<div class="col-md-9">
				<textarea name="previous_grants" class="form-control" rows="5" placeholder="<?= lang('placeholder_previous_grants') ?>"><?= (isset($application) ? $application['previous_grants'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('special_achievement') ?></label>
			<div class="col-md-9">
				<textarea name="special_achievement" class="form-control" rows="5" placeholder="<?= lang('placeholder_special_achievement') ?>"><?= (isset($application) ? $application['special_achievement'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('myipo_protection') ?> *</label>
			<div class="col-md-9">
				<div class="radio"> 
					<label> <input type="radio" name="myipo_protection" value="1" <?= (isset($application) ? ($application['myipo_protection'] == 1 ? "checked" : "") : "checked") ?>> <?= lang('yes') ?> </label> 
				</div>
				<textarea name="myipo_protection_desc" class="form-control" rows="5" placeholder="<?= lang('placeholder_myipo_protection_desc') ?>"><?= (isset($application) ? $application['myipo_protection_desc'] : "") ?></textarea>
				<div class="radio"> 
					<label> <input type="radio" name="myipo_protection" value="2" <?= (isset($application) ? ($application['myipo_protection'] == 2 ? "checked" : "") : "") ?>> <?= lang('no') ?> </label> 
				</div>
			</div>
		</div>

		<div class="form-group picture-wrapper">
			<div class="col-md-3">
				<label class="col-md-12 no-padding control-label"><?= lang('project_picture') ?> *</label>
				<small class="form-text text-muted">(Sila muat naik gambar dari depan, dari belakang, sebelah kiri, sebelah kanan, atas dan bawah).</small>
			</div>
			<div class="col-md-9 item">
				<?php if(isset($application)){
						foreach ($i_picture as $key => $value) { 
							if(file_exists(realpath(APPPATH . '../'.PATH_TO_APPLICATION_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['filename'])) {
								$url = site_url() . 'assets/attachment/application_picture/thumbnail/' . $value['filename'];
							}else{
								$url = site_url() . 'assets/attachment/application_picture/thumbnail/default.jpg';
							} ?>
							<div class="col-md-12">
								<a href="<?= $url ?>" class="fancyboxs">
									<img src="<?= $url ?>" width="300px" class="img-app">
								</a>
								<button type="button" class="btn btn-danger delete-picture" data-id="<?= $value['id'] ?>" data-name="<?= $value['filename']?>"><span class="fa fa-times"></span></button>
							</div>
				<?php 	}
					} ?>

				<div class="col-md-11">
					<input type="file" class="form-control picture-item" name="picture_0">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-grey add-picture"><span class="fa fa-plus"></span></button>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('organization_support') ?></label>
			<div class="col-md-9">
				<textarea name="organization_support" class="form-control" rows="5" placeholder="<?= lang('placeholder_organization_support') ?>"><?= (isset($application) ? $application['organization_support'] : "") ?></textarea>
			</div>
		</div>

		<?php if(!isset($application)){ ?>
		<div class="form-group">
			<div class="col-md-9 col-md-offset-3">
				<div class="radio"> 
					<label> <input type="radio" class="agreement" name="agreement" value="1"> <?= lang('agreement_confirm') ?> </label> 
				</div>
				<div class="radio"> 
					<label> <input type="radio" class="agreement" name="agreement" value="0"> <?= lang('agreement_cancel') ?> </label> 
				</div>
			</div>
		</div>
		<?php } ?>

		<div class="col-md-12 text-center">
			<button type="button" class="btn btn-default btn-back flat" onclick="window.history.back()"><?= ($this->userdata['role_id'] == ROLE_INNOVATOR && $view_mode != 1 ? lang('cancel') : lang('back')) ?></button>
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-success flat" value="<?= lang('save') ?>">
			<?php 
			date_default_timezone_set("Asia/Kuala_Lumpur");
			if((strtotime($round['deadline']) >= strtotime(date("Y-m-d H:i:s"))) && !isset($is_admin)){ ?>
				<input type="submit" name="btn_send_for_approval" data-id="btn_send_for_approval" class="btn btn-success flat btn-send-for-approval" data-id="<?= isset($application) ? $application['id'] : 0 ?>" value="<?= lang('send_for_approval') ?>">
			<?php } ?>
		</div>
	</form>

	<?php if($view_mode != 1){ ?>
	<div class="col-md-6">
		<div class="panel">
			<div class="panel-body"><b>
				<ul>
					<li class="text-red"><i>Anda wajib mengisi butiran di setiap ruang</i></li>
				</ul>
			</b></div>
		</div>
	</div>
	<?php } ?>
</div>