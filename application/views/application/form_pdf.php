<style>
table tr td table th{
	background: #888d97;
	color:#FFF;
}

table tr td table.bordered, table tr td table.bordered tr td{
	border:1px solid #ddd !important;
}
</style>
<?php $length = 1000; ?>
<table border="0" width="100%">
	<tr>
		<td colspan="2" style="text-align:center"><h2><?= lang('application_form') ?></h2></td>
	</tr>
	<tr>
		<td width="40%"><?= lang('submission_date') ?> : </td>
		<td width="60%"><?= $application['submission_date'] ?></td>
	</tr>

	<tr>
		<td><?= lang('team_leader') ?> : </td>
		<td><?= $innovator['name'] ?></td>
	</tr>

	<tr>
		<td><?= lang('kp_no') ?> : </td>
		<td><?= $innovator['kp_no'] ?></td>
	</tr>

	<tr>
		<td><?= lang('application') ?> : </td>
		<td><?= $application_type[$application['application_type']]['name'] ?></td>
	</tr>

	<?php if(count($i_team) > 0){
			if($i_team[0]['name'] != ""){ ?>
	<tr>
		<td colspan="2"><?= lang('team_member') ?> : </td>
	</tr>

	<tr>
		<td colspan="2">
			<table border="1" class="bordered" width="100%">
				<tr>
					<th>No</th>
					<th><?= lang('member_name') ?></th>
					<th><?= lang('kp_no') ?></th>
					<th><?= lang('telp_no') ?></th>
				</tr>
			<?php $no=1;
				foreach ($i_team as $key => $value) { ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?= $value['name']; ?></td>
						<td><?= $value['kp_no']; ?></td>
						<td><?= $value['telp_no']; ?></td>
					</tr>

			<?php $no++;} ?> 
			</table>
		</td>
	</tr>
	<?php } } ?>

	<tr>
		<td><?= lang('address') ?> : </td>
		<td><?= $innovator['address'] ?></td>
	</tr>

	<tr>
		<td><?= lang('postcode') ?> : </td>
		<td><?= $innovator['postcode'] ?></td>
	</tr>

	<tr>
		<td><?= lang('state') ?> : </td>
		<td><?= $i_state; ?></td>
	</tr>

	<tr>
		<td><?= lang('country') ?> : </td>
		<td><?= $i_country; ?></td>
	</tr>

	<tr>
		<td><?= lang('telp_no') ?> : </td>
		<td><?= $innovator['telp_no'] ?></td>
	</tr>

	<tr>
		<td><?= lang('email') ?> : </td>
		<td><?= $innovator['email'] ?></td>
	</tr>

	<tr>
		<td width="40%"><?= lang('project_title') ?> : </td>
		<td width="60%"><?= $application['title'] ?></td>
	</tr>

	<?php 
		$desc_length = ceil(strlen($application['description'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('description')." : " : "") ?> </td>
		<td><?= substr($application['description'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$desc_length = ceil(strlen($application['problems_encountered'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('problems_encountered')." : " : "") ?> </td>
		<td><?= substr($application['problems_encountered'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<tr>
		<td><?= lang('award_purpose') ?></td>
		<td><?= $application['award_purpose'] ?></td>
	</tr>

	<?php 
		$desc_length = ceil(strlen($application['how_to_use'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('how_to_use')." : " : "") ?> </td>
		<td><?= substr($application['how_to_use'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$desc_length = ceil(strlen($application['innovation_novelty'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('innovation_novelty')." : " : "") ?> </td>
		<td><?= substr($application['innovation_novelty'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<tr>
		<td><?= lang('innovation_category') ?> : </td>
		<td><?= $i_category['name'] ?></td>
	</tr>

	<?php 
		$desc_length = ceil(strlen($application['impact'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('impact')." : " : "") ?> </td>
		<td><?= substr($application['impact'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$desc_length = ceil(strlen($application['community_involvement'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('community_involvement')." : " : "") ?> </td>
		<td><?= substr($application['community_involvement'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$desc_length = ceil(strlen($application['previous_grants'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('previous_grants')." : " : "") ?> </td>
		<td><?= substr($application['previous_grants'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$desc_length = ceil(strlen($application['special_achievement'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('special_achievement')." : " : "") ?> </td>
		<td><?= substr($application['special_achievement'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>	

	<tr>
		<td><?= lang('myipo_protection') ?> : </td>
		<td><?= $application['myipo_protection'] == 1 ? lang('yes') : lang('no') ?></td>
	</tr>

	<tr>
		<td><?= lang('myipo_protection_desc') ?> : </td>
		<td><?= $application['myipo_protection_desc']; ?></td>
	</tr>

	<?php if(count($i_picture) > 0){ ?>
	<tr>
		<td colspan="2"><?= lang('project_picture') ?></td>
	</tr>
	<?php } ?>

	<?php foreach ($i_picture as $key => $value) { 
			if(file_exists(realpath(APPPATH . '../'.PATH_TO_APPLICATION_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['filename'])) {
				$url = site_url() . 'assets/attachment/application_picture/thumbnail/' . $value['filename'];
			}else{
				$url = site_url() . 'assets/attachment/application_picture/thumbnail/default.jpg';
			} ?>
	<tr>
		<td colspan="2"><img src="<?= $url ?>" width="100%"></td>
	</tr>
	<?php } ?>

	<?php 
		$desc_length = ceil(strlen($application['organization_support'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('organization_support')." : " : "") ?> </td>
		<td><?= substr($application['organization_support'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>	
	
</table>