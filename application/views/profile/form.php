<script type="text/javascript">
	var alert = "<?= $alert ?>",
		userId = "<?= $this->userdata['id'] ?>";
</script>
<div class="col-md-8 col-md-offset-2">
	<div class="col-md-12 text-center margin-btm-50">
		<h2><?= lang('edit_profile') ?></h2>
	</div>

	<div class="col-md-12 alert alert-info hide alert-dismissable">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>

	<form id="form-profile" class="form-horizontal" action="<?= base_url().'profile/save' ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= (isset($innovation) ? $innovation['innovation_id'] : 0) ?>">
		<input type="hidden" name="username" value="<?= (isset($innovation) ? $innovation['username'] : 0) ?>">

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('email') ?></label>
			<div class="col-md-9">
				<input type="text" name="email" class="form-control" placeholder="<?= lang('email') ?>" value="<?= (isset($innovator) ? $innovator['email'] : '') ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-9 col-md-offset-3">
				<button type="button" class="btn btn-default btn-change-pass">Change Password</button>
			</div>
		</div>
		<div class="collapse-group collapse">
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('password') ?></label>
				<div class="col-md-9">
					<input type="password" name="password" placeholder="<?= lang('password') ?>" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
				<div class="col-md-9">
					<input type="password" name="retype_password" placeholder="<?= lang('retype_password') ?>" class="form-control">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('name') ?></label>
			<div class="col-md-9">
				<input type="text" name="name" class="form-control" placeholder="<?= lang('name') ?>" value="<?= (isset($innovator) ? $innovator['name'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('kp_no') ?></label>
			<div class="col-md-9">
				<div class="input-group">
					<input type="text" class="form-control" name="kp_no_1" placeholder="<?= lang('kp_no') ?>" value="<?= (isset($innovator) ? $innovator['kp_no_1'] : '') ?>">
					<span class="input-group-addon">-</span>
					<input type="text" class="form-control" name="kp_no_2" value="<?= (isset($innovator) ? $innovator['kp_no_2'] : '') ?>">
					<span class="input-group-addon">-</span>
					<input type="text" class="form-control" name="kp_no_3" value="<?= (isset($innovator) ? $innovator['kp_no_3'] : '') ?>">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="telp_no" class="form-control" placeholder="<?= lang('telp_no') ?>" value="<?= (isset($innovator) ? $innovator['telp_no'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('address') ?></label>
			<div class="col-md-9">
				<textarea name="address" class="form-control" placeholder="<?= lang('address') ?>"><?= (isset($innovator) ? $innovator['address'] : '') ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('postcode') ?></label>
			<div class="col-md-9">
				<input type="text" name="postcode" class="form-control" placeholder="<?= lang('postcode') ?>" value="<?= (isset($innovator) ? $innovator['postcode'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('country') ?></label>
			<div class="col-md-9">
				<select name="country_id" class="form-control">
					<?php foreach ($countries as $key => $value) { ?>
						<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('state') ?></label>
			<div class="col-md-9">
				<select name="state_id" class="form-control">
					<?php foreach ($states as $key => $value) { ?>
						<option value="<?= $value['id'] ?>" <?= (isset($innovator) ? ($innovator['state_id'] == $value['id'] ? 'selected' : '') : '') ?>><?= $value['name'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="col-md-12 text-center">
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-success flat" value="<?= lang('save') ?>">
		</div>
	</form>
</div>