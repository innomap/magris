<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>
<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-6 col-md-offset-3">		
				<div class="col-md-12 text-center">
					<h3><b><?= lang('reset_password') ?></b></h3>
				</div>
				<div class="col-md-12 login-box bg-grey-2">
					<div class="alert alert-info alert-dismissable hide">
					    <i class="fa fa-info"></i>
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					    <?= $alert ?>
					</div>
					<form id="form-forgot-password" action="<?= base_url().'accounts/forgot_password_handler' ?>" method="POST">
						<p><?= lang('forgot_password_msg') ?></p>
						<div class="form-group">
							<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>">
						</div>

						<div class="form-group text-center">
							<input type="submit" class="btn btn-success rounded" name="btn_login" value="<?= lang('send_email') ?>">
						</div>
					</form>
					<div class="col-md-12 text-center">
						<a href="<?= base_url() ?>">Log In</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 text-center footer">
			<p>Copyright © 2010 - 2016 Yayasan Inovasi Malaysia / Malaysian Foundation for Innovation. All Rights Reserved.</p>
		</div>
	</div>
</div>

