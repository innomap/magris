<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-12">
				<h3 class="text-center"><b>Terima Kasih</b></h3><br/>
				<p class="text-center">Kami mengucapkan terima kasih di atas penyertaan anda di dalam program MaGRIs dan berharap agar inovasi anda akan menjadi salah satu dari inovasi terpilih yang akan dibangunkan sebagai produk komersial.</p>
				<p class="text-center">Inovasi yang terpilih untuk peringkat saringan awal, akan dihubungi oleh pihak sekretriat.</p>
			</div>
		</div>
	</div>

	<div class="col-md-12 text-center footer">
		<p>Copyright © 2010 - 2016 Yayasan Inovasi Malaysia / Malaysian Foundation for Innovation. All Rights Reserved.</p>
	</div>
</div>
