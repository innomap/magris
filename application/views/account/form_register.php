<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>

<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-10 col-md-offset-1">
				<div class="col-md-12 text-center">
					<h3><b><?= lang('registration_form') ?></b></h3>
				</div>

				<div class="col-md-12 login-box bg-grey-2">
					<div class="alert alert-info alert-dismissable hide">
					    <i class="fa fa-info"></i>
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					    <?= $alert ?>
					</div>

					<form id="form-register" action="<?= base_url().'accounts/register_handler' ?>" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label><?= lang('email') ?></label>
							<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>">
						</div>

						<div class="form-group">
							<label><?= lang('password') ?></label>
							<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
						</div>

						<div class="form-group">
							<label><?= lang('retype_password') ?></label>
							<input type="password" class="form-control" name="retype_password" placeholder="<?= lang('retype_password') ?>">	
						</div>

						<div class="form-group">
							<label><?= lang('name') ?></label>
							<input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>">
						</div>

						<div class="form-group">
							<label><?= lang('kp_no') ?></label>
							<div class="input-group">
								<input type="text" class="form-control" name="kp_no_1" placeholder="<?= lang('kp_no') ?>">
								<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="kp_no_2">
								<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="kp_no_3">
							</div>
						</div>

						<div class="form-group">
							<label><?= lang('telp_no') ?></label>
							<input type="text" class="form-control" name="telp_no" placeholder="<?= lang('telp_no') ?>">
						</div>

						<div class="form-group">
							<label><?= lang('address') ?></label>
							<textarea class="form-control" name="address" placeholder="<?= lang('address') ?>"></textarea>
						</div>

						<div class="form-group">
							<label><?= lang('postcode') ?></label>
							<input type="text" class="form-control" name="postcode" placeholder="<?= lang('postcode') ?>">
						</div>

						<div class="form-group">
							<label><?= lang('country') ?></label>
							<select name="country_id" class="form-control">
								<?php foreach ($countries as $key => $value) { ?>
									<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
								<?php } ?>
							</select>
						</div>

						<div class="form-group">
							<label><?= lang('state') ?></label>
							<select name="state_id" class="form-control">
								<?php foreach ($states as $key => $value) { ?>
									<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
								<?php } ?>
							</select>
						</div>

						<div class="form-group text-center">
							<input type="submit" name="btn_register" class="btn btn-success rounded" value="<?= lang('register') ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php $this->load->view('partial/footer') ?>
	</div>
</div>