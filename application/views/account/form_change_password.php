<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>
<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-6 col-md-offset-3">		
			<div class="col-md-12 text-center">
				<h2><?= lang('reset_password') ?></h2>
			</div>
			<div class="col-md-12 login-box bg-grey-2">
				<?php if($is_token_exist){ ?>
					<div class="alert alert-info alert-dismissable hide">
					    <i class="fa fa-info"></i>
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					    <?= $alert ?>
					</div>
					<form id="form-create-password" action="<?= base_url().'accounts/create_new_password_handler' ?>" method="POST">
						<div class="form-group">
							<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="retype_password" placeholder="<?= lang('retype_password') ?>">
						</div>
						<input type="hidden" name="token" value="<?= $token ?>">
						<div class="form-group text-center">
							<input type="submit" class="btn btn-success rounded" name="btn_login" value="<?= lang('save') ?>">
						</div>
					</form>
					<?php }else{ ?>
						<p><?= lang('page_not_found_msg') ?></p>
					<?php } ?>
				<div class="col-md-12 text-center">
					<a href="<?= base_url().'login' ?>">Log In</a>
				</div>
			</div>
		</div>
	</div>
</div>

