<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>		
<div class="col-md-12 no-padding">
	<h4 class="text-center"><b>Log Masuk</b></h4>
	<div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>
	<form id="form-login" autocomplete="off" action="<?= base_url().'accounts/login_auth' ?>" method="POST">
		<div class="form-group">
			<label><?= lang('email') ?></label>
			<input type="text" autocomplete="off" class="form-control" name="email" placeholder="<?= lang('email') ?>" value="">
		</div>

		<div class="form-group">
			<label><?= lang('password') ?></label>
			<input type="password" autocomplete="off" class="form-control" name="password" placeholder="<?= lang('password') ?>" value="">
		</div>

		<div class="col-md-12 no-padding text-left action-btn">
			<input type="submit" class="btn btn-default" name="btn_login" value="Log In">
			<a href="<?= base_url().'forgot_password' ?>"><?= lang('forgot_password') ?></a> |
			<a href="<?= base_url().'registration' ?>"><?= lang('registration') ?></a>
		</div>
	</form>
</div>
