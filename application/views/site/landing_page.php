<!DOCTYPE html>
<html lang="en">

<head>
	<title>Yayasan Inovasi Malaysia / Malaysian Foundation for Innovation - MaGRIs </title>
	
	<meta name="description" content="Yayasan Inovasi Malaysia (YIM) - the Malaysian Foundation for Innovation, invites you on a journey towards a Nation of Innovation! With close support from the Ministry of Science, Technology & Innovation (MOSTI), YIM was established to promote and champion Innovation, and by doing so, to mobilize Malaysians to embrace and practice Creativity and Innovation." />
	<meta name="keywords" content="YIM,  Inovasi, Innovation, Malaysia, Kreativiti, Creativity, Innovation Toolkit,Teh Tarik Talk, 3T, WIFKL, CATS, PopInnovation, MyIdeas, MyIdeas@school, Jejak inovasi, MyIdeas, YIM, MOSTI, 1Malaysia, MAGIC, im4u, innovative culture, Cornerstone CMS, cornerstone.my, global innovation index, competitive, big idea, leading" />
	

	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	
	<link rel="stylesheet" type="text/css" href="<?= base_url().ASSETS_CSS.'landing/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url().ASSETS_CSS.'landing/main_responsive.css' ?>">
    <script type="text/javascript" src="js/jquery.js"></script>
    
    <script type="text/javascript" src="js/carouFredSel.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    
	
</head>

<body>



<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>


<script language="JavaScript" src="<?= base_url().ASSETS_JS.'landing/sidemenu.js' ?>"></script>



<div  style=" margin:auto; background-color:#cdcdcd; ">
<div class="wrapper"><a href="<?= base_url() ?>"><img src="<?= base_url().ASSETS_IMG.'landing/YIM_Website_Header2016_version01.jpg' ?>" style="max-width:100%; border:0;" /></a></div>
</div>
<header>
	<div class="wrapper">
		<a href="<?= base_url() ?>"><img class="logo" alt="HOME" src="<?= base_url().ASSETS_IMG.'landing/logo.png' ?>" border="0"></a>
		<a href="http://yim.my/yim_v3/index.cfm-&menuid=132&parentid=92.html#" target="_blank" class="menu_icon" id="menu_icon"></a>
		<nav id="nav_menu">
			<ul>
				
				<li>
					
					
					
					
					
					<a href="http://yim.my/yim_v3/index.cfm-&menuid=37&lang=en.html" target="_blank">About Us</a>
					
					
					
					
					
					
					
					
				</li>
				
				<li>
					
					
					
					
					
					<a href="http://yim.my/yim_v3/newsmaster.cfm-&action=news&menuid=6&lang=en.html" target="_blank">YIM News</a>
					
					
					
					
					
					
					
					
				</li>
				
				<li>
					
					
					
					
					
					<a href="http://yim.my/yim_v3/index.cfm-&menuid=120&lang=en.html" target="_blank">Visual Gallery</a>
					
					
					
					
					
					
					
					
				</li>
				
				<li>
					
					
					
					
					
					<a href="http://yim.my/yim_v3/index.cfm-&menuid=11&lang=en.html" target="_blank">Contact Us</a>
					
					
					
					
					
					
					
					
				</li>
				
			</ul>
		</nav>
			
		<ul class="social">
			<li><a class="fb" href="https://www.facebook.com/YayasanInovasi" target="_blank" Title="YIM Facbook"></a></li>
			<li><a class="twitter" href="https://twitter.com/YayasanInovasi" target="_blank" title="YIM Twitter Tweets"></a></li>
			<li><a class="gplus" href="https://plus.google.com/114838810858570763126/posts" target="_blank" Title="YIM Google+"></a></li>
		</ul>
		
	</div>
</header>




<script language="javascript">

	var prefix = 'en';

</script>




<div  class="info_content">
<p><style type="text/css">
#hafcontainer{
    margin: 0 auto;
    max-width: 1200px;
    clear: both;
}

.hafimg {
	width:100%;
}

#hafleft20 {
    float: left;
    padding: 1%;
    width: 20%;
	
}

#hafleft40 {
    float: left;
    padding: 1%;
    width: 40%;
}

#hafleft50 {
    float: left;
    padding: 1%;
    width: 50%;
}

#hafleft60 {
    float: left;
    padding: 1%;
    width: 60%;
}

#hafleft80 {
    float: left;
    padding: 1%;
    width: 80%;
}

#hafright20 {
    float: right;
    padding: 1%;
    width: 20%;
}

#hafright40 {
    float: right;
    padding: 1%;
    width: 40%;
}

#hafright50 {
    float: right;
    padding: 1%;
    width: 50%;
}

#hafright60 {
    float: right;
    padding: 1%;
    width: 60%;
}

#hafright80 {
    float: right;
    padding: 1%;
    width: 80%;
}

p	{
        font-family: calibri;
	font-size: 22px;
}

@media all and (max-width : 768px) {
    #hafleft20, #hafleft40, #hafleft50, #hafleft60, #hafleft80{
        width: 100%;
        padding: 3%;
    }
    #hafright20, #hafright40, #hafright50, #hafright60, #hafright80 {
        width: 100%;
        padding: 3%;
    }
}

</style></p>
<div id="hafcontainer" align="center"><img class="hafimg" style="width: 40%" align="center" alt="" src="<?= base_url().ASSETS_IMG.'landing/logo_magris.jpg' ?>" />

<div align="center" style="width: 100%; padding: 1%;">
<p><font size="5">TERIMA KASIH KERANA MINAT ANDA</font></p>
<font size="3">   </font>
<br />
<p><font size="4">Penyertaan program Pencarian Inovasi Akar Umbi atau Mainstreaming Grassroots Innovations (MaGRIs) akan dibuka semula pada 1hb November 2016.<br /><br />
<!--Pra-daftar di sini untuk menerima emel dan maklumat dari Yayasan Inovasi Malaysia.<br />
Nama, Emel, No. Tel Bimbit dan Alamat<br /><br />
<p align="center"><a href="http://www.magris.innomap.my/registration/"><img width="30%" class="hafimg1" alt="" src="aeimages/Image/2016/magris/Images/Klik_disini_big.jpg" /></a></p>
<br />
<p><font size="3">DASAR PRIVASI DAN DASAR KESELAMATAN<br />
Maklumat anda akan digunakan bagi penyediaan maklum balas dari Yayasan Inovasi Malaysia (YIM) dengan tujuan memaklumkan projek-project YIM di masa hadapan.</font></p>
<br />
<p><font size="3">KESELAMATAN STORAN<br />
Semua storan elektronik dan penghantaran data peribadi akan dilindungi dan disimpan dengan menggunakan teknologi keselamatan yang sesuai.</font></p>-->
</div>
</div> 
</div>
<footer>
	<p class="rights">Copyright &copy; 2010 - 2016 Yayasan Inovasi Malaysia / Malaysian Foundation for Innovation. All Rights Reserved.</p>
	<img src="<?= base_url().ASSETS_IMG.'landing/logo-mosti-footer.jpg' ?>" />&nbsp;<img src="<?= base_url().ASSETS_IMG.'landing/logo-yim-footer.jpg' ?>" />
</footer>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-765613-51', 'auto');
  ga('send', 'pageview');

</script>


</body>
</html>
