<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-12">
				<h3 class="text-center"><b>PENGUMUMAN</b></h3><br/>
				<p class="center">Terima kasih atas sambutan yang anda berikan kepada MaGRIs Pencarian Inovasi Akar Umbi. Untuk memberi peluang kepada lebih ramai peserta, tarikh tutup penyertaan telah dilanjutkan kepada 10 Mac 2017.</p>
				<p class="center">Jangan lepaskan peluang ini.</p> 

				<h3 class="text-center"><b>Terma & Syarat</b></h3><br/>
				<ul class="text-justify">
					<li>
						<p>Penyertaan terbuka kepada rakyat Malaysia yang berumur dari 18 tahun dan ke atas; secara Individu atau Berkumpulan.</p>
					</li>
					<li>
						<p>Inovasi tersebut mesti memenuhi definisi <b>“Inovasi  Akar  Umbi”</b> yang ditetapkan seperti di bawah:</p>
						<p class="text-center">Produk atau proses inovasi yang dibangunkan oleh golongan berpendapatan rendah, biasanya bertujuan untuk memenuhi keperluan asas serta mengatasi kesusahan dan cabaran hidup</p>
						<p class="text-center">atau</p>
						<p class="text-center">Inovasi diterajui komuniti untuk mencapai kelestarian hidup yang sering mengalami kesukaran untuk berkembang luas ke pasaran-pasaran lain.</p>
					</li>
					<li>
						<p>Inovasi yang mempunyai nilai tinggi, berimpak positif dan menyumbang kearah kesejahteraan komuniti luar bandar akan diberi keutamaan. </p>
						<p>Inovasi yang dipertandingkan merupakan Idea asal atau penambahbaikan daripada inovasi sedia ada.</p>
					</li>
					<li>
						<p>Inovasi yang disertakan mesti telah mencapai fasa protataip (hampir siap) atau siap dibangunkan. Setiap penyertaan harus dilengkapkan dengan maklumat yang mencukupi untuk memudahkan proses pemilihan.</p>
					</li>
					
					<li>
						<p>Penyertaan boleh dihantar secara pos ke:</p>
						<p class="text-center"><b>Yayasan Inovasi Malaysia<br>
							Unit E001, Ground Floor, Block 3440,<br>
							Enterprise Building 1, Jalan Teknokrat 3,<br>
							63000, Cyberjaya, Selangor.
							</b>
						</p>
						<p class="text-center">atau</p>
						<p class="text-center">melalui laman web <b><i><u><a href="<?= base_url().'registration' ?>" class="link">magris.innomap.my/registration</a></u></i></b></p>						
					</li>
					
					<li>
						<p>Semua penyertaan mesti diterima pihak penganjur selewat-lewatnya pada 10 Mac 2017, akan dinilai pada tahun yang sama. Penyertaan yang diterima selepas tarikh tersebut akan dinilai pada tahun berikutnya.</p>
					</li>
					
					<li>
						<p>Inovasi terpilih oleh Jawatankuasa Penilaian MaGRIs akan diberi bimbingan dan sokongan pembangunan ke tahap pra-pengkomersialan, seterusnya dilengkapi dengan pelan perniagaan strategik (tertakluk kepada kebolehlaksanaan) </p>
					</li>
					
					<li>
						<p>Semua peserta terpilih mesti bersetuju mengambil bahagian dan melengkapkan keseluruhan program serta memenuhi terma dan syarat yang ditetapkan oleh pihak penganjur.</p>
					</li>
					
					<li>
						<p>Keputusan Jawatankuasa Penilaian MaGRIs adalah muktamad dan pengumuman akan dibuat melalui surat menyurat ataupun emel, dalam bulan Mac setiap tahun. </p>
					</li>
				</ul>
			</div>
			<div class="col-md-12 text-right social-link">
				<a href="http://www.facebook.com/sharer.php?u=http://yim.my/magris" target="_blank" title="Facebook">
					<img src="<?= base_url().ASSETS_IMG.'ico-fb.png' ?>"/>
				</a>
				<a href="http://yim.my/magris" target="_blank" title="Instagram">
					<img src="<?= base_url().ASSETS_IMG.'ico-instagram.png' ?>"/>
				</a>
			</div>
		</div>
	</div>

	<?php $this->load->view('partial/footer') ?>
</div>

