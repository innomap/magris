<script type="text/javascript">
	var alert_contact = "<?= $alert_contact ?>",
		alert = "";
</script>
<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-12">
				<div class="col-md-6">
					<h3 class="text-center"><b>Borang Hubungi Kami</b></h3><br/>

					<div class="col-md-12">
						<div class="alert alert-info alert-dismissable hide">
						    <i class="fa fa-info"></i>
						    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						    <?= $alert_contact ?>
						</div>

						<form id="form-contact-us" action="<?= base_url().'site/contact_us_handler' ?>" method="POST">
							<div class="form-group">
								<label><?= lang('name') ?></label>
								<input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>">
							</div>

							<div class="form-group">
								<label><?= lang('email') ?></label>
								<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>">
							</div>

							<div class="form-group">
								<label><?= lang('subject') ?></label>
								<input type="text" class="form-control" name="subject" placeholder="<?= lang('subject') ?>">
							</div>

							<div class="form-group">
								<label><?= lang('comments') ?></label>
								<textarea class="form-control" name="comments" placeholder="<?= lang('comments') ?>" rows="5"></textarea>
							</div>

							<div class="form-group">
								<input type="submit" name="btn_submit" class="btn btn-primary" value="<?= lang('submit') ?>">
								<button type="button" name="btn_cancel" class="btn btn-default"><?= lang('cancel') ?></button>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-6">
					<h3 class="text-center"><b>ATAU hantarkan kepada</b></h3><br/>
					<p>
						Yayasan Inovasi Malaysia<br/>
						Unit E001, Ground Floor<br/>
						Block 3440, Enterprise Building 1<br/>
						Jalan Teknokrat 3<br/>
						63000, Cyberjaya, Selangor Darul Ehsan<br/>
						Malaysia
					</p>
					<br/><br/>
					<div class="col-md-6 no-padding">
						Tel: +603 - 8319 1714
					</div>
					<div class="col-md-6">
						Fax: +603 - 8319 1715 
					</div>

					<div class="col-md-6 no-padding">
						URL: www.yim.my/magris
					</div>
					<div class="col-md-6">
						Emel: magris@yim.my 
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('partial/footer') ?>
</div>

