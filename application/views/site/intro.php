<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-12">
				<h3 class="text-center"><b>Mengenai MaGRIs</b></h3><br/>
				<p class="text-justify">MaGRIs merupakan sebuah projek yang bermatlamat untuk mempertingkatkan dan memacu usaha Pembangunan dan Pengkomersilan Inovasi Akar Umbi yang berpotensi di Malaysia melalui kolaborasi dengan pelbagai pihak seperti kerajaan, industri, masyarakat akar umbi dan golongan muda termasuk menyediakan peluang pembinaan kapasiti kepada pihak-pihak yang terlibat!</p>
			</div>

			<div class="col-md-6">
				<div class="col-md-12">
					<h3 class="text-center"><b>Objektif:</b></h3><br/>
					<ul>
						<li class="text-justify"><p>Menyediakan persekitaran yang kondusif bagi inovator akar umbi yang mempunyai inovasi yang berpotensi untuk dibangunkan dan membimbing mereka untuk menjadi model tauladan kepada masyarakat setempat.</p></li>
						<li class="text-justify"><p>Merintis jalan baru dalam usaha merangsang aktiviti pengkomersilan akar umbi menjadi aktiviti pengkomersilan arus perdana.</p></li>
						<li class="text-justify"><p>Membuka peluang kepada inovator akar umbi untuk mempertingkatkan inovasi mereka menuju ekonomi arus perdana.</p></li>
					</ul>
				</div>
				<div class="col-md-12">
					<h3 class="text-center"><b>Definisi Inovasi Akar Umbi</b></h3><br/>
					<p class="text-justify">Produk atau proses inovatif yang dibangunkan oleh golongan berpendapatan rendah, biasanya bertujuan untuk memenuhi keperluan asas serta mengatasi kesusahan dan cabaran hidup.</p>
					<p class="text-center">atau</p>
					<p class="text-justify">Inovasi diterajui komuniti untuk mencapai kelestarian hidup yang sering mengalami kesukaran untuk berkembang luas ke pasaran-pasaran lain.</p>
				</div>
			</div>

			<div class="col-md-6">
				<h3 class="text-center"><b>Cerita Kejayaan Inovator</b></h3><br/>
				<img src="<?= base_url().ASSETS_IMG.'wasinah.png' ?>" width="100%"/>
			</div>
		</div>
	</div>

	<?php $this->load->view('partial/footer') ?>
</div>

