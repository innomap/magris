<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-6">
				<h3 class="text-center"><b>Mengenai MaGRIs</b></h3><br/>
				<p class="text-justify">MaGRIs merupakan sebuah projek yang bermatlamat untuk mempertingkatkan dan memacu usaha Pembangunan Inovasi Akar Umbi yang berpotensi di Malaysia melalui kolaborasi dengan pelbagai pihak seperti kerajaan, industri, masyarakat akar umbi dan golongan muda termasuk menyediakan peluang pembinaan kapasiti kepada pihak-pihak yang terlibat!</p>
			</div>

			<div class="col-md-6">
				<h3 class="text-center"><b>Kami Sedang Mencari Inovasi Akar Umbi (GRI)</b></h3><br/>
				<p class="text-justify">MaGRIs sedang mencari inovator akar umbi yang memiliki semangat yang tinggi untuk terus berinovasi dan berhasrat untuk bekerjasama dengan peneraju industri serta para cendekiawan untuk membangun dan memasarkan inovasi mereka.</p>
				<p class="text-justify">Melalui penyertaan di dalam MaGRIs, anda akan berpeluang untuk mempertingkatkan tahap pengetahuan, keupayaan, jalinan kerjasama dan kemahiran berinovasi yang menyumbang ke arah penjanaan kekayaan melalui inovasi. </p>
				<p class="text-justify">Bagi inovasi yang terpilih, kami akan memberikan sokongan serta sumber untuk mempertingkatkan inovasi anda sehingga mencapai tahap pra-pengkomersialan. </p>
				<p class="text-justify">Inovasi yang memiliki nilai pasaran yang tinggi akan menerima geran pembangunan bernilai RM30,000. </p>
				<a href="<?= base_url().'registration' ?>"><img src="<?= base_url().ASSETS_IMG.'click_rm.png' ?>" width="100%"/></a>
			</div>
		</div>
	</div>

	<?php $this->load->view('partial/footer') ?>
</div>