<div class="col-md-10 col-md-offset-1">
	<?php $this->load->view('partial/header') ?>
	<div class="col-md-12">
		<div class="col-md-12 landing-content">
			<div class="col-md-12">
				<h3 class="text-center"><b>Soalan Lazim</b></h3><br/>
				<dl>
					<dt>
						<b>01. Mengenai MaGRis?</b>
					</dt>
					<dd>
						<p>MaGRIs merupakan sebuah projek yang bermatlamat untuk mempertingkatkan dan memacu usaha Pembangunan dan Pengkomersialan Inovasi Akar Umbi yang berpotensi di Malaysia melalui kolaborasi dengan pelbagai pihak seperti kerajaan, industri, masyarakat akar umbi dan golongan muda termasuk menyediakan peluang pembinaan kapasiti kepada pihak-pihak yang terlibat!.</p>
					</dd>
					<dt>
						<b>02. Apakah manfaat yang saya dapat dengan menyertai MaGRIs?</b>
					</dt>
					<dd>
						<p>Sekiranya inovasi anda dipilih oleh Jawatankuasa Penilaian MaGRIs, anda akan diberi sokongan dalam bentuk inisiatif pembangunan produk, jalinan hubung rangkaian dan penerapan pengetahuan di mana inovasi yang memiliki potensi nilai pasaran yang tinggi akan diberi pendedahan sepenuhnya kepada aktiviti sokongan pembangunan pasaran pada peringkat selanjutnya.</p>
						<p>Pada masa yang sama semua peserta akan diundang untuk menyertai bengkel pembinaan kapasiti yang dianjurkan oleh YIM sepanjang tempoh berlangsungnya program.</p>
					</dd>
					<dt>
						<b>03. Bolehkah mereka yang bukan warganegara Malaysia menyertai MaGRIs?</b>
					</dt>
					<dd>
						<p>Tidak, hanya warganegara Malaysia berusia 18 tahun dan ke atas layak untuk menyertai.  SIla rujuk kepada terma & syarat untuk mendapatkan maklumat lanjut.</p>
					</dd>
					<dt>
						<b>04. Bolehkah saya menyertai program ini melalui syarikat/organisasi saya?</b>
					</dt>
					<dd>
						<p>Tidak, anda hanya boleh menyertai program ini di bawah nama individu iaitu secara persendirian atau berkumpulan (maksima 4 orang)</p>
					</dd>
					<dt>
						<b>05. Adakah terdapat sebarang kelulusan akademik minima yang diperlukan untuk menyertai program ini?</b>
					</dt>
					<dd>
						<p>TIDAK.  Kami menerima penyertaan dari mana-mana individu selagi mana inovasi yang dikemukakan untuk penyertaan adalah berlandaskan kepada inisiatif yang memenuhi keperluan masyarakat.</p>
					</dd>
					<dt>
						<b>06. Bolehkah saya mengambil bahagian dengan menghantar lebih dari satu inovasi?</b>
					</dt>
					<dd>
						<p>YA. Namun anda perlu menghantar setiap inovasi secara berasingan.</p>
					</dd>
					<dt>
						<b>07. Sekiranya saya dipilih untuk program ini, apakah yang akan diharapkan dari saya? </b>
					</dt>
					<dd>
						<p>Anda perlu menjalani beberapa peringkat aktiviti seperti yang tercatat di bawah dalam tempoh 6 bulan.  Bagaimanapun, tarikh pelaksanaan aktiviti-aktiviti ini adalah tertakluk kepada persetujuan dan kelapangan anda seboleh mungkin.<p>
						<p>a. Pembinaan produk bersama pakar dari YIM.</p>
						<p>b. Menghadiri bengkel yang bermatlamat untuk membantu mempertingkatkan kualiti, kebolehpasaran dan kemampanan inovasi anda.</p>
					</dd>
					<dt>
						<b>08. Adakah terdapat jenis-jenis inovasi akar umbi yang tertentu yang tidak layak untuk menyertai program ini? </b>
					</dt>
					<dd>
						<p>TIDAK, melainkan inovasi anda tidak memenuhi definisi Inovasi Akar Umbi.</p>
					</dd>
					<dt>
						<b>09. Bagaimana saya boleh menyertai program ini? </b>
					</dt>
					<dd>
						<p>a. Muat turun Borang Penyertaan MaGRIs dari <b>magris.innomap.my</b>  dan hantarkan melalui pos kepada:</p>
						<p class="text-center">
							<b>Yayasan Inovasi Malaysia</br>
							Unit E001, Ground Floor</br>
							Block 3440, Enterprise Building 1</br>
							Jalan Teknokrat 3<br>
							63000, Cyberjaya, Selangor</b>
							</br></br>
							<a href="<?= base_url().ASSETS.'attachment/MaGRIs_Borang_Permohonan_in_BM_amendment.pdf' ?>" target="_blank"><img src="<?= base_url().ASSETS_IMG.'btn_download_form.png' ?>" width="28%"/></a>
							<br/><br/>
							<b><i>ATAU</i></b>
						</p>
						
						<p>
						b. Lengkapkan borang penyertaan secara online di <b><a href="<?= base_url() ?>" class="link">magris.innomap.my</a></b>
						</p>
					</dd>
					<dt>
						<b>10. Bolehkah saya menyertai program ini sekiranya inovasi saya berada pada peringkat idea sahaja?</b>
					</dt>
					<dd>
						<p>TIDAK.  Kelayakan minima adalah inovasi akar umbi tersebut mesti berada sekurang-kurangnya pada peringkat prototaip (hampir siap) atau siap sepenuhnya.</p>
					</dd>
					<dt>
						<b>11. Adakah terdapat sebarang terma & syarat sebelum geran pengkomersialan bernilai RM30,000 tersebut diberikan kepada saya?</b>
					</dt>
					<dd>
						<p>YA. Anda perlu bersama-sama membangunkan Pelan Perniagaan yang turut merangkumi bajet (bimbingan akan diberikan oleh YIM) sebelum geran tersebut diberikan kepada anda untuk digunakan.  Bagi invator yang memilih untuk melaksanakan inovasi sahaja tanpa melibatkan sebarang aktiviti pengkomersialan, YIM akan menghubungkan anda dengan usahawan tempatan yang berpotensi sebagai rakan kongsi (tertakluk kepada persetujuan dari kedua-dua pihak).</p>
					</dd>
					<dt>
						<b>12. Apa yang akan berlaku sekiranya saya tidak memiliki sebarang pengetahuan dalam pengurusan perniagaan namun ingin mengkomersilkan inovasi saya?</b>
					</dt>
					<dd>
						<p>Jangan bimbang, kerana YIM akan memberi bimbingan sepanjang berlangsungnya program ini.</p>
					</dd>
					<dt>
						<b>13. Siapakah yang akan memiliki Hak Intelektual kepada inovasi saya ini?</b>
					</dt>
					<dd>
						<p>Anda akan memiliki hak 100% kepada inovasi anda.</p>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<?php $this->load->view('partial/footer') ?>
</div>

