<div class="col-md-12">
    <div class="col-md-12 table-responsive content-box">
		<table id="table-contact" class="table table-bordered table-striped table-dark">
		    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('name') ?></th>
				<th><?= lang('email') ?></th>
				<th><?= lang('subject') ?></th>
				<th><?= lang('comments') ?></th>
				<th><?= lang('created_at') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($contacts as $key=>$value) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $value['name'] ?></td>
			    <td><?= $value['email'] ?></td>
			    <td><?= $value['subject'] ?></td>
			    <td><?= $value['comments'] ?></td>
			    <td><?= $value['created_at'] ?></td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>
</div>