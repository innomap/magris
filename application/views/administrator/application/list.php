<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_application = "<?= lang('delete_application') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		lang_cancel = "<?= lang('cancel') ?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 table-responsive">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<form action="<?= base_url().PATH_TO_ADMIN.'applications/index' ?>" method="POST">
			<div class="row">
				<div class="col-md-12 no-padding">
					<div class="form-group col-md-6">
						<label class="col-md-3"><?= lang('filter_by_status') ?></label>
						<div class="col-md-6">
							<select id="dt-filter-status" name="status" class="form-control">
								<option value="0">All</option>
								<?php foreach ($status as $key => $value) { ?>
										<option value="<?= $key ?>"><?= $value['name'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12 no-padding">
					<div class="form-group col-md-6">
						<div class="col-md-12 no-padding">
							<label class="col-md-3"><?= lang('filter_by_state') ?></label>
							<div class="col-md-6">
								<select id="dt-filter-state" name="state_id" class="form-control">
									<option value="0">All</option>
									<?php foreach ($states as $key => $value) { ?>
											<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group col-md-6">
					<div class="col-md-12 no-padding">
						<label class="col-md-3"><?= lang('filter_by_year') ?></label>
						<div class="col-md-6">
							<select id="dt-filter-year" name="year_id" class="form-control">
								<option value="0">All</option>
								<?php foreach ($year as $key => $value) { ?>
										<option value="<?= $value['year'] ?>"><?= $value['year'] ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-3">
							<input type="submit" name="btn_export" class="btn btn-success" value="Export to Excel">
						</div>
					</div>
				</div>
			</div>
		</form>
		
		<form id="form-application-list" action="<?= base_url().PATH_TO_ADMIN.'applications/list_submit_handler' ?>" method="post">
			<table id="table-application" class="table table-bordered table-striped table-dark">
			    <thead>
				    <tr>
				    	<th></th>
						<th>No</th>
						<th><?= lang('innovator') ?></th>
						<th><?= lang('innovation_product') ?></th>
						<th><?= lang('description') ?></th>
						<th><?= lang('innovation_category') ?></th>
						<th><?= lang('state') ?></th>
						<th><?= lang('submission_date') ?></th>
						<th><?= lang('status') ?></th>
						<th><?= lang('average_score') ?></th>
						<th width="14%"><?= lang('action') ?></th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($applications as $key=>$value) { ?>
				<tr>
					<td><input type="checkbox" name="ids[]" data-is-evaluated="<?= ($value['is_evaluation_done'] ? 1 : 0) ?>" data-has-evaluation="<?= $value['has_evaluation'] ?>" value="<?= $value['id'] ?>" <?= ($value['status'] == APPLICATION_STATUS_APPROVED ? 'disabled' : '') ?>></td>
				    <td><?= $no ?></td>
				    <td><?= $value['innovator']['name'] ?></td>
				    <td><?= $value['title'] ?></td>
				    <td><?= ellipsis($value['description'], 100) ?></td>
				    <td><?= $value['category']['name']; ?></td>
				    <td><?= $value['state']['name']; ?></td>
				    <td><?= $value['submission_date']; ?></td>
				    <td class="<?= $status[$value['status']]['class'] ?>"><?= $status[$value['status']]['name'] ?></td>
				    <td><?= $value['average_score']; ?></td>
				    <td>
				    	<input type="hidden" name="action">
				    	<button type="button" class="btn btn-default btn-action btn-edit-application" title="<?= lang('edit') ?>" data-id="<?= $value['id'] ?>">
						    <span class="fa fa-pencil fa-lg"></span>
						</button>

						<button type="button" class="btn btn-default btn-action btn-view-application" title="<?= lang('view_innovation_detail') ?>" data-id="<?= $value['id'] ?>">
						    <span class="fa fa-search fa-lg"></span>
						</button>

						<button type="button" class="btn btn-default btn-action btn-delete-application text-red" data-id="<?= $value['id'] ?>"
						data-name="<?= $value['title'] ?>" title="<?= lang('delete') ?>">
						    <span class="fa fa-trash-o fa-lg"></span>
						</button>

						<?php if($value['status'] > APPLICATION_STATUS_SENT_APPROVAL){ ?>
							<button type="button" class="btn btn-default btn-action btn-view-evaluation-list" title="<?= lang('view_evaluation_result') ?>" data-id="<?= $value['id'] ?>">
							    <span class="fa fa-files-o fa-lg"></span>
							</button>
						<?php } ?>

						<?php if($value['status'] <= APPLICATION_STATUS_SENT_APPROVAL){ ?>
							<button type="button" class="btn btn-default btn-action btn-assign-application" title="<?= lang('assign_to_evaluator') ?>" data-id="<?= $value['id'] ?>">
								    <span class="fa fa-user fa-lg"></span>
							</button>
						<?php } ?>
						<button type="button" class="btn btn-default btn-action btn-download-pdf" title="<?= lang('download_pdf') ?>" data-id="<?= $value['id'] ?>">
						    <span class="fa fa-download fa-lg"></span> 
						</button>

                    	<button type="button" class="btn btn-default btn-action btn-print-pdf" title="Print pdf" data-id="<?= $value['id'] ?>" >
                            <span class="fa fa-print fa-lg"></span> 
                    	</button>
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>
			<?php if(count($applications) > 0){ ?>
				<div class="form-group">
					<label><?= lang('with_selected')." : " ?></label>
					<input type="submit" value="<?= lang('proceed_to_next_round') ?>" data-id="btn-proceed" class="btn btn-success">
				</div>
			<?php } ?>
		</form>
    </div>
</div>
<?= $form_assign ?>

<iframe id="iframeprint" style="display:none"></iframe>