<div class="col-md-12">
    <div class="col-md-12 table-responsive">
    	<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-12"><?= lang('innovation_product')." : ".$application['title'] ?></label>
			</div>
		</div>

		<form id="form-evaluation-list" action="<?= base_url().PATH_TO_ADMIN.'applications/list_submit_handler' ?>" method="post">
			<table id="table-evaluation-list" class="table table-bordered table-striped table-dark">
			    <thead>
				    <tr>
						<th>No</th>
						<th><?= lang('evaluator') ?></th>
						<th><?= lang('score') ?></th>
						<th><?= lang('action') ?></th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($evaluations as $key=>$value) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $value['evaluator'] ?></td>
				    <td><?= $value['score'] ?></td>
				    <td>
						<?php if($value['evaluation_id'] > 0){ ?>
							<button type="button" class="btn btn-default btn-action btn-view-evaluation" title="<?= lang('view_evaluation_result') ?>" data-id="<?= $value['evaluation_id'] ?>">
							    <span class="fa fa-file-text-o fa-lg"></span>
							</button>
						<?php } ?>
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>

			<div class="form-field">
				<button type="button" class="btn btn-default flat btn-cancel"><?= lang('back') ?></button>
			</div>
		</form>
    </div>
</div>