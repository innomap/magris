<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('cancel') ?></span></button>
				<h4 class="modal-title"><?= lang('form') ?></h4>
			</div>
			<form role="form" id="form-form" action="<?= site_url(PATH_TO_ADMIN.'forms/store/') ?>" method="POST">
			<input type="hidden" name="id" />
				<div class="modal-body">
					<!-- text input -->
					<div class="form-group">
						<label><?= lang('name') ?></label>
						<input type="text" name="name" class="form-control" placeholder="<?= lang('name') ?>" />
					</div>
					<!-- text input -->
					<div class="form-group">
						<label><?= lang('description') ?></label>
						<textarea name="description" class="form-control" placeholder="<?= lang('description') ?>" /></textarea>
					</div>

					<div class="panel panel-default">
						<div class="panel-body">
							<div class="form-group">
								<label><?= lang('question') ?></label>
								<div class="col-md-12 question-wrap no-padding">
									<div class="col-md-12 no-padding question-item">
										<div class="col-md-11 no-padding">
											<input type="text" name="questions[]" class="form-control" placeholder="<?= lang('question') ?>" />
										</div>
										<div class="col-md-1">
											<button type="button" class="btn btn-grey add-question"><span class="fa fa-plus"></span></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default flat" data-dismiss="modal"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>