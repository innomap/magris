<script type="text/javascript">
	var alert = "<?=$alert?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 table-responsive content-box">
    	<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<table id="table-innovator" class="table table-bordered table-striped table-dark">
		    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('email') ?></th>
				<th><?= lang('name') ?></th>
				<th><?= lang('kp_no') ?></th>
				<th><?= lang('telp_no') ?></th>
				<th><?= lang('address') ?></th>
				<th><?= lang('postcode') ?></th>
				<th><?= lang('state') ?></th>
				<th><?= lang('country') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($innovators as $key=>$value) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $value['email'] ?></td>
			    <td><?= $value['name'] ?></td>
			    <td><?= $value['kp_no'] ?></td>
			    <td><?= $value['telp_no'] ?></td>
			    <td><?= $value['address'] ?></td>
			    <td><?= $value['postcode'] ?></td>
			    <td><?= $value['state_name'] ?></td>
			    <td><?= $value['country_name'] ?></td>
			    <td>
			    <?php if($value['status'] == USER_STATUS_NON_ACTIVE){ ?>
			    	<a href="<?= base_url().PATH_TO_ADMIN.'innovators/activate/'.$value['user_id'] ?>">Activate</a>
			    <?php } ?>
			    </td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>
</div>