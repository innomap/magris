<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_group = "<?= lang('delete_group') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>
<div class="col-md-12">
    <div class="col-md-12">
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-group">
			<span class="fa fa-plus"></span> <?= lang('new_group') ?>
		</button>
    </div>
    <div class="col-md-12 table-responsive content-box">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<table id="table-group" class="table table-bordered table-striped table-dark">
		    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('group_name') ?></th>
				<th><?= lang('group_description') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($groups as $key=>$value) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $value['name'] ?></td>
			    <td><?= $value['description'] ?></td>
			    <td>
				<button type="button" class="btn btn-default btn-action btn-edit-group" title="<?= lang('edit') ?>" data-id="<?= $value['id'] ?>">
				    <span class="fa fa-pencil fa-lg"></span>
				</button>
				<button type="button" class="btn btn-default btn-action btn-delete-group text-red" title="<?= lang('delete') ?>" data-id="<?= $value['id'] ?>"
					data-name="<?= $value['name'] ?>">
				    <span class="fa fa-trash-o fa-lg"></span>
				</button>
			    </td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>
    <?= $form_view; ?>
</div>