<div class="modal fade" id="modal-evaluator" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('cancel') ?></span></button>
				<h4 class="modal-title"><?= lang('evaluator') ?></h4>
			</div>
			<form role="form" id="form-evaluator" action="<?= site_url(PATH_TO_ADMIN.'evaluators/store/') ?>" method="POST">
			<input type="hidden" name="id" />
				<div class="modal-body">
					<!-- text input -->
					<div class="form-group">
						<label><?= lang('email') ?></label>
						<input type="text" name="email" class="form-control" placeholder="<?= lang('email') ?>" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-default hide btn-change-pass"><?= lang('change_password') ?></button>
					</div>
					<div class="collapse-group">
						<!-- text input -->
						<div class="form-group pass-group">
							<label><?= lang('password') ?></label>
							<input type="password" name="password" class="form-control" placeholder="<?= lang('password') ?>" />
						</div>
						<!-- text input -->
						<div class="form-group pass-group">
							<label><?= lang('retype_password') ?></label>
							<input type="password" name="retype_password" class="form-control" placeholder="<?= lang('retype_password') ?>" />
						</div>
					</div>
					<!-- text input -->
					<div class="form-group">
						<label><?= lang('name') ?></label>
						<input type="text" name="name" class="form-control" placeholder="<?= lang('name') ?>" />
					</div>
					<div class="form-group">
						<label><?= lang('group_name') ?></label>
						<select name="group_id" class="form-control">
							<?php foreach ($groups as $key => $value) { ?>
								<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default flat" data-dismiss="modal"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>