<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_evaluation_focus = "<?= lang('delete_evaluation_focus') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>
<div class="col-md-12">
	<div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>
	<form id="form-evaluation-form" class="form-horizontal" action="<?= base_url().PATH_TO_ADMIN.'evaluation_form/update' ?>" method="POST">
		<input type="hidden" name="form_id" value="<?= $evaluation_form['id'] ?>">
		<div class="form-group">
			<label class="col-md-1 control-label"><?= lang('name') ?></label>
			<div class="col-md-4">
				<input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>" value="<?= $evaluation_form['name'] ?>">
			</div>
		</div>

		<div class="col-md-5 text-right margin-btm-50">
			<button type="button" class="btn btn-default btn-back flat" onclick="window.history.back()"><?= lang('cancel') ?></button>
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-success flat" value="<?= lang('save') ?>">
		</div>
	</form>

	    <div class="col-md-12">
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-evaluation-focus">
				<span class="fa fa-plus"></span> <?= lang('new_evaluation_focus') ?>
			</button>
	    </div>
	    <div class="col-md-12 table-responsive content-box">
			
			<table id="table-evaluation-focus" class="table table-bordered table-striped table-dark">
			    <thead>
			    <tr>
					<th>No</th>
					<th><?= lang('evaluation_focus') ?></th>
					<th><?= lang('percentage') ?></th>
					<th><?= lang('action') ?></th>
			    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($evaluation_focus as $key=>$value) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $value['label'] ?></td>
				    <td><?= $value['percentage'] ?></td>
				    <td>
					<button type="button" class="btn btn-default btn-action btn-edit-focus" title="<?= lang('edit') ?>" data-id="<?= $value['id'] ?>">
					    <span class="fa fa-pencil fa-lg"></span>
					</button>
					<button type="button" class="btn btn-default btn-action btn-delete-focus text-red" title="<?= lang('delete') ?>" data-id="<?= $value['id'] ?>"
						data-name="<?= $value['label'] ?>" data-form-id="<?= $evaluation_form['id'] ?>">
					    <span class="fa fa-trash-o fa-lg"></span>
					</button>
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>
	    </div>
	</div>
    <?= $form_view; ?>
</div>