<div class="modal fade" id="modal-round" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('cancel') ?></span></button>
				<h4 class="modal-title"><?= lang('round') ?></h4>
			</div>
			<form role="form" id="form-round" action="<?= site_url(PATH_TO_ADMIN.'rounds/store/') ?>" method="POST">
			<input type="hidden" name="id" />
				<div class="modal-body">
					<!-- text input -->
					<div class="form-group">
						<label><?= lang('title') ?></label>
						<input type="text" name="title" class="form-control" placeholder="<?= lang('title') ?>" />
					</div>
					<!-- text input -->
					<div class="form-group">
						<label><?= lang('deadline') ?></label>
						<input type="text" name="deadline" class="form-control datepicker" placeholder="<?= lang('deadline') ?>" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default flat" data-dismiss="modal"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>