<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluation extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluation";
		$this->primary_field = "id";
	}

	function get_average_score($where = NULL){
		$arg = "SELECT ROUND(AVG(evaluation.total),1) as score FROM `evaluation` 
				WHERE evaluation.total != 0
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY evaluation.application_id";

		$query = $this->db->query($arg);
            
        return $query;
	}

	function get_top_score_by_group_list($where = NULL, $limit = 100){
		$arg = "SELECT evaluation.application_id,evaluation.evaluator_id,application.title as innovation_name, innovator.name as innovator_name,user.email, innovator.telp_no,innovator.kp_no,innovator.address, ROUND(AVG(evaluation.total),1) as score, application_expert_team.id as team_id FROM `evaluation` 
					JOIN application ON application.id = evaluation.application_id
					JOIN application_category_innovation ON application.id = application_category_innovation.application_id
					JOIN innovator ON innovator.user_id = application.user_id
					JOIN user ON innovator.user_id = user.id
					JOIN evaluator_expert_group ON evaluator_expert_group.evaluator_id = evaluation.evaluator_id
					JOIN application_expert_team ON application_expert_team.application_id = application.id
					WHERE evaluation.total != 0 AND application_expert_team.id IS NOT NULL
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY evaluation.application_id ORDER BY score DESC LIMIT 0,{$limit}";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>