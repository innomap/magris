<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluation_form_model extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluation_form";
		$this->primary_field = "id";
	}
}

?>