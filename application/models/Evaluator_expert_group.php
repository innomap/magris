<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluator_expert_group extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluator_expert_group";
		$this->primary_field = "id";
	}

	function delete_by_evaluator($evaluator_id){
		$this->db->where("evaluator_id", $evaluator_id);
		return $this->db->delete($this->table_name);
	}
}

?>