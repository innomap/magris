<?php

require_once(APPPATH . 'models/General_model.php');
class Evaluator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "evaluator";
		$this->primary_field = "user_id";
	}

	function get_with_user($where = NULL){
		$this->db->select('user.*, evaluator.*, expert_group.name as group_name');
		$this->db->from('evaluator');
		$this->db->join('user', 'user.id = evaluator.user_id');
		$this->db->join('evaluator_expert_group', 'evaluator_expert_group.evaluator_id = evaluator.user_id');
		$this->db->join('expert_group', 'evaluator_expert_group.expert_group_id = expert_group.id');
		if($where != NULL){
			$this->db->where($where);
		}

		$q = $this->db->get();
		return $q->result_array();
	}
}

?>