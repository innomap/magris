<?php

require_once(APPPATH . 'models/General_model.php');
class Application extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "application";
		$this->primary_field = "id";
	}

	function get_number($where = NULL, $group_by = NULL){
        	$this->db->select("*");
                $this->db->from($this->table_name);
                if($where != NULL){
                	$this->db->where($where);
                }

                if($group_by != NULL){
                	$this->db->group_by($group_by);
                }
                $q = $this->db->get();

                return $q->num_rows();
	}

        function get_join($where = NULL, $limit = NULL, $order_by = NULL){
                $this->db->select("application.*,innovator.*,user.*, innovator.name as innovator_name, application.id as id, application.status as status");
                $this->db->from("application");
                $this->db->join('innovator','innovator.user_id = application.user_id');
                $this->db->join('user','innovator.user_id = user.id');
                $this->db->join('application_category_innovation','application_category_innovation.application_id = application.id','LEFT');
                if($where != NULL){
                        $this->db->where($where);
                }
                if($limit != NULL){
                        $this->db->limit($limit);
                }
                if($order_by != NULL){
                        $this->db->order_by($order_by);
                }
                return $this->db->get();
        }

        function get_list_submission_year(){
                $arg = "SELECT DISTINCT YEAR( submission_date ) as year FROM application WHERE YEAR( submission_date ) != 0";
                $query = $this->db->query($arg);

                return $query->result_array();
        }
}

?>