<?php

require_once(APPPATH . 'models/General_model.php');
class Application_category_innovation extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "application_category_innovation";
		$this->primary_field = "id";
	}

	function update_by_application_id($application_id, $data){
		$this->db->where('application_id',$application_id);
		$this->db->update($this->table_name, $data);
	}

	function find_join($where = NULL){
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->join('category_innovation','category_innovation.id = '.$this->table_name.'.category_innovation_id');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}
}

?>