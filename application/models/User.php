<?php

require_once(APPPATH . 'models/General_model.php');
class User extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "user";
		$this->primary_field = "id";
	}

	function is_email_exist($email, $id = 0){
		$user = $this->find_one('email = "'.$email.'"'.($id != 0 ? ' AND id != '.$id : ''));
		if($user != NULL){
			return true;
		}else{
			return false;
		}
	}

	function insert_user($data, $role = ROLE_INNOVATOR){
		$data["password"] = $this->get_hash($data["email"], $data["password"]);
		$id = $this->insert($data);

		return $id;
	}

	function update_user($id, $data){
		if(isset($data['password'])){
			$data["password"] = $this->get_hash($data["email"], $data["password"]);	
		}
		
		return $this->update($id, $data);
	}

	function get_hash($email, $password) {
		return md5($email.':'.$password);
	}
}

?>