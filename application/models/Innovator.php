<?php

require_once(APPPATH . 'models/General_model.php');
class Innovator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "innovator";
		$this->primary_field = "user_id";
	}

	function get_innovator_name($user_id){
		$innovator = $this->find_one("user_id = ".$user_id);
        if($innovator){
            return $innovator['name'];
        }else{
        	return "";
        }
	}

	function get_one_join($user_id){
		$this->db->select("innovator.*, user.email");
		$this->db->from("innovator");
		$this->db->join('user','user.id = innovator.user_id');
		$this->db->where('innovator.user_id', $user_id);
		$q = $this->db->get();

		return $q->row_array();
	}

	function get_join($where = NULL, $limit = NULL, $order_by = NULL){
		$this->db->select("innovator.*, user.email, user.created_at, user.status, state.name as state_name, country.name as country_name");
		$this->db->from("innovator");
		$this->db->join('user','user.id = innovator.user_id');
		$this->db->join('state','innovator.state_id = state.id');
		$this->db->join('country','country.id = state.country_id');
		if($where != NULL){
			$this->db->where($where);
		}
		if($limit != NULL){
			$this->db->limit($limit);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by);
		}
		return $this->db->get();
	}
}

?>