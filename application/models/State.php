<?php

require_once(APPPATH . 'models/General_model.php');
class State extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "state";
		$this->primary_field = "id";
	}

	function get_join($where = NULL){
		$this->db->select("state.*, country.name as country_name");
		$this->db->from("state");
		$this->db->join('country','country.id = state.country_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$q = $this->db->get();

		return $q;
	}
}

?>