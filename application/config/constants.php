<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*PATH DIR*/
define('ASSETS',    'assets/');
define('ASSETS_CSS',    'assets/css/');
define('ASSETS_JS',    'assets/js/');
define('ASSETS_IMG', 'assets/img/');

define('DEFAULT_LAYOUT', 'default');
define('DEFAULT_ADMIN_LAYOUT', 'admin_default');
define('DEFAULT_EVALUATOR_LAYOUT', 'evaluator_default');

define('LANGUAGE_MELAYU','melayu');
define('LANGUAGE_ENGLISH','english');

/*MAIL CONFIG*/
define('MAIL_HOST_SERVER', 'server.innomap.my');
define('MAIL_PORT_SERVER',     587);
define('MAIL_AUTH_SERVER',     TRUE);
define('MAIL_SECURE_SERVER',   'tls');
define('MAIL_USERNAME_SERVER', 'support@innomap.my');
define('MAIL_PASSWORD_SERVER', 'supportinnomap123');
define('MAIL_FROM_SERVER',     'support@innomap.my');
define('MAIL_FROMNAME_SERVER', 'Innomap Support');
define('MAIL_WORDWRAP_SERVER', 65);
//password: ]0ACubbQ*FPo

/*USER ROLE*/
define('ROLE_INNOVATOR', 0);
define('ROLE_ADMINISTRATOR', 1);
define('ROLE_EVALUATOR', 2);

/*USER STATUS*/
define('USER_STATUS_NON_ACTIVE', 0);
define('USER_STATUS_ACTIVE',1);

/*FILE PATH*/
define('PATH_TO_ADMIN', 'administrator/');
define('PATH_TO_EVALUATOR', 'evaluator/');
define('PATH_TO_APPLICATION_PICTURE', 'assets/attachment/application_picture/');
define('PATH_TO_APPLICATION_PICTURE_THUMB', 'assets/attachment/application_picture/thumbnail/');
define('PATH_TO_INNOVATION_ATTACHMENT', 'assets/attachment/innovation_attachment/');
define('PATH_TO_INNOVATOR_PHOTO', 'assets/attachment/innovator_photo/');
define('PATH_TO_INNOVATOR_PHOTO_THUMB', 'assets/attachment/innovator_photo/thumbnail/');
define('PATH_TO_TEAM_MEMBER_PHOTO', 'assets/attachment/team_member/');
define('PATH_TO_TEAM_MEMBER_PHOTO_THUMB', 'assets/attachment/team_member/thumbnail/');
define('PATH_TO_APPLICATION_FILE', 'assets/attachment/application_file/');

/*INNOVATION STATUS*/
define('APPLICATION_STATUS_DRAFT', 0);
define('APPLICATION_STATUS_SENT_APPROVAL', 1);
define('APPLICATION_STATUS_EVALUATED', 2);
define('APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR', 3);
define('APPLICATION_STATUS_APPROVED', 4);

define('APPLICATION_STATUS',serialize(array(
	0 => array('name' => "Draft", 'class' => "text-blue"),
	1 => array('name' => "Submitted", 'class' => "text-green"),
	2 => array('name' => "Evaluated", 'class' => "text-green"),
	3 => array('name' => "Assigned to Evaluator", 'class' => "text-green"),
	4 => array('name' => "Approved", 'class' => "text-green")
)));

define('APPLICATION_STATUS_ADMIN',serialize(array(
	1 => array('name' => "New", 'class' => "text-blue"),
	2 => array('name' => "Evaluated", 'class' => "text-green"),
	3 => array('name' => "Assigned to Evaluator", 'class' => "text-green"),
	4 => array('name' => "Approved", 'class' => "text-green")
)));

define('APPLICATION_STATUS_EVALUATOR',serialize(array(
	2 => array('name' => "Evaluated", 'class' => "text-green"),
	3 => array('name' => "New", 'class' => "text-green"),
	4 => array('name' => "Evaluated", 'class' => "text-green")
)));

define('APPLICATION_TYPE', serialize(array(
	0 => array('name' => 'Individual'),
	1 => array('name' => 'Berkumpulan')
)));

define('JOB_TYPE', serialize(array(
	0 => array('name' => 'Seorang Peniaga'),
	1 => array('name' => 'Seorang Pekerja Tetap'),
	2 => array('name' => 'Lain-lain, sila huraikan'),
)));

define('BUSINESS_MODEL', serialize(array(
	0 => array('name' => 'Saya berharap agar dapat menjadi usahawan untuk inovasi saya'),
	1 => array('name' => 'Saya lebih suka memberi hak produk kepada usahawan yang lain untuk dikomersialkan'),
	2 => array('name' => 'Saya sudi menerima kedua-dua pilihan diatas'),
)));

