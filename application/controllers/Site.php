<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Site extends Common {

	function __construct() {
		parent::__construct("site");

		$this->layout = DEFAULT_LAYOUT;
		$this->load->model('user_session');
        $this->lang->load('account',$this->language);

		$this->title = "Site";
		$this->menu = "site";
    }

    public function index(){
    }

    function intro(){
        $this->submenu = "intro";
        $data['alert'] = $this->session->flashdata('alert');
        $this->load->view('site/intro',$data);
    }

    function activity(){
        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = 'site/site_activity';

        $this->styles[] = 'fancybox/jquery.fancybox';
        $this->submenu = "activity";
        $data['alert'] = $this->session->flashdata('alert');
        $this->load->view('site/activity',$data);
    }

    function term(){
        $this->submenu = "term";
        $data['alert'] = $this->session->flashdata('alert');
        $this->load->view('site/term',$data);
    }

    function faq(){
        $this->submenu = "faq";
        $data['alert'] = $this->session->flashdata('alert');
        $this->load->view('site/faq',$data);
    }

    function contact(){
        $this->scripts[] = 'site/site';
        $this->submenu = "contact";
        $data['alert'] = $this->session->flashdata('alert');
        $data['alert_contact'] = $this->session->flashdata('alert_contact');
        $this->load->view('site/contact',$data);
    }

    function contact_us_handler(){
        $this->layout = FALSE;
        $this->load->model('contact_us');

        $postdata = $this->postdata();

        if($postdata['name'] != "" && $postdata['email'] != "" && $postdata['subject'] != "" && $postdata['comments'] != ""){
            $data = array('name' => $postdata['name'],
                           'email' => $postdata['email'],
                           'subject' => $postdata['subject'],
                           'comments' => $postdata['comments']);
            if($id = $this->contact_us->insert($data)){
                $this->session->set_flashdata('alert_contact','Your data has been successfully submitted.');
            }
        }else{
            $this->session->set_flashdata('alert_contact','Some fields are required.');
        }

        redirect(base_url().'contact');
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url());
    }
}
