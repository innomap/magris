<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Expert_groups extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Group";
		$this->menu = "expert_group";

		$this->load->model('evaluator');
        $this->load->model('expert_group');
        $this->load->model('evaluator_expert_group');

		$this->lang->load(PATH_TO_ADMIN.'expert_group',$this->language);

		$this->scripts[] = 'administrator/expert_group';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['groups'] = $this->expert_group->find_all();
        $data['form_view'] = $this->load->view(PATH_TO_ADMIN.'expert_group/form', NULL, TRUE);

		$this->load->view(PATH_TO_ADMIN.'expert_group/list', $data);
    }

    function store(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['name'] == ""){
            $this->session->set_flashdata('alert','Sorry, the fields are required.');
        }else{
            $data = array(
                "name" => $postdata['name'],
                "description" => $postdata['description']);
        
            if($id = $this->expert_group->insert($data)){
                $this->session->set_flashdata('alert','New Group has been created');
            }else{
                $this->session->set_flashdata('alert','An error occured, please try again later');
            }
        }

    	redirect(base_url().PATH_TO_ADMIN.'expert_groups');
    }

    public function edit($id = 0){
		$this->layout = FALSE;

        $group = $this->expert_group->find_one("id = ".$id);
        if($group){
            $ret = array(
                'status' => '1',
                'data' => $group
            );
        }else{
            $ret = array(
                'status' => '0'
            );
        }
        
        echo json_encode($ret);
	}

	function update(){
		$this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['name'] == ""){
            $this->session->set_flashdata('alert','Sorry, the fields are required.');
        }else{
            $data = array(
                "name" => $postdata['name'],
                "description" => $postdata['description']);
        
            if($this->expert_group->update($postdata['id'],$data)){
                $this->session->set_flashdata('alert','New Group has been created');
            }else{
                $this->session->set_flashdata('alert','An error occured, please try again later');
            }
        }

        redirect(base_url().PATH_TO_ADMIN.'expert_groups');
	}

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'expert_groups');
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->expert_group->delete($id)){
            $this->session->set_flashdata('alert','Group has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Group can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'expert_groups');
    }
}
