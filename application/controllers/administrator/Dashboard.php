<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Dashboard extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Dashboard";
		$this->menu = "dashboard";

		$this->load->model('application');

		$this->scripts[] = 'plugins/highcharts/highcharts';
		$this->scripts[] = 'administrator/dashboard';
    }

    public function index(){
        $data['draft_number'] = $this->application->get_number("status = ".APPLICATION_STATUS_DRAFT);
		$this->load->view(PATH_TO_ADMIN.'dashboard/index', $data);
    }

    function report_number_by_day_handler(){
    	$this->layout = FALSE;
    	
    	$xAxis = array();
    	$data = array();
    	for ($i=6; $i >= 0; $i--) { 
    		$xAxis[] = date('d-m-Y',strtotime($i." days ago"));;
    		$date = date('Y-m-d',strtotime($i." days ago"));
    		$data[] = $this->application->get_number("DATE( submission_date ) =  '".$date."'");
    	}

    	$result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Draft Number', 'data' => $data)));
    	echo json_encode($result);
    }
}
