<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Rounds extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Application Rounds";
		$this->menu = "round";

		$this->load->model('round');

		$this->lang->load(PATH_TO_ADMIN.'round',$this->language);

		$this->scripts[] = 'plugins/bootstrap-datepicker/bootstrap-datetimepicker.min';
		$this->scripts[] = 'administrator/round';

		$this->styles[] = 'bootstrap-datepicker/bootstrap-datetimepicker.min';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['rounds'] = $this->round->find_all();
    	$data['form_view'] = $this->load->view(PATH_TO_ADMIN.'round/form', '', TRUE);

		$this->load->view(PATH_TO_ADMIN.'round/list', $data);
    }

    function store(){
    	$this->layout = FALSE;

    	$title = $this->input->post('title');
    	$deadline = $this->input->post('deadline');

    	$data = array('title' => $title,
    				  'deadline' => $deadline);

    	if($this->round->insert($data)){
    		$this->session->set_flashdata('alert','New Round has been created');
    	}else{
    		$this->session->set_flashdata('alert','An error occured, please try again later');
    	}

    	redirect(base_url().PATH_TO_ADMIN.'rounds');
    }

    public function edit($id = 0){
    	$this->layout = FALSE;

		$round = $this->round->find_by_id($id);
		if($round){
			$ret = array(
				'status' => '1',
				'data' => $round
			);
		}else{
			$ret = array(
				'status' => '0'
			);
		}
		
		echo json_encode($ret);
	}

	function update(){
		$this->layout = FALSE;

		$id = $this->input->post('id');
		$title = $this->input->post('title');
    	$deadline = $this->input->post('deadline');

    	$data = array('title' => $title,
    				  'deadline' => $deadline);

    	if($this->round->update($id, $data)){
    		$this->session->set_flashdata('alert','New Round has been updated');
    	}else{
    		$this->session->set_flashdata('alert','An error occured, please try again later');
    	}

    	redirect(base_url().PATH_TO_ADMIN.'rounds');
	}

    function delete($id){
        $this->layout = FALSE;
        if($this->round->delete($id)){
            $this->session->set_flashdata('alert','Round has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Round can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'rounds');
    }
}
