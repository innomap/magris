<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluators extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Evaluator";
		$this->menu = "evaluator";

		$this->load->model('evaluator');
        $this->load->model('user');
        $this->load->model('expert_group');
        $this->load->model('evaluator_expert_group');

		$this->lang->load(PATH_TO_ADMIN.'evaluator',$this->language);

		$this->scripts[] = 'administrator/evaluator';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['evaluators'] = $this->evaluator->get_with_user();
    	$data['form_view'] = $this->load->view(PATH_TO_ADMIN.'evaluator/form', array('groups' => $this->expert_group->find_all()), TRUE);

		$this->load->view(PATH_TO_ADMIN.'evaluator/list', $data);
    }

    function store(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['email'] != "" && $postdata['name'] != ""){
            if($this->user->is_email_exist($postdata['email'])){
                $this->session->set_flashdata('alert','Sorry, your email has been registered.');
            }else{
                $data = array(
                    "email" => $postdata['email'],
                    "password" => $postdata['password'],
                    "status" => USER_STATUS_ACTIVE,
                    "role_id" => ROLE_EVALUATOR,
                    "created_at" => date('Y-m-d H:i:s'));
            
                if($id = $this->user->insert_user($data, ROLE_EVALUATOR)){
                    $evaluator = array('user_id' => $id, 'name' => $postdata['name']);
                    $this->evaluator->insert($evaluator);

                    $group_evaluator = array('expert_group_id' => $postdata['group_id'], 'evaluator_id' => $id);
                    $this->evaluator_expert_group->insert($group_evaluator);

                    $this->session->set_flashdata('alert','New Evaluator has been created');
                }else{
                    $this->session->set_flashdata('alert','An error occured, please try again later');
                }
            }
        }else{
            $this->session->set_flashdata('alert','Some fields are required');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

    public function edit($id = 0){
    	$this->layout = FALSE;

		$evaluator = $this->evaluator->get_with_user("user_id = ".$id);
		if($evaluator){
            $evaluator[0]['group'] = $this->evaluator_expert_group->find_one("evaluator_id = ".$id); 
			$ret = array(
				'status' => '1',
				'data' => $evaluator[0]
			);
		}else{
			$ret = array(
				'status' => '0'
			);
		}
		
		echo json_encode($ret);
	}

	function update(){
		$this->layout = FALSE;

		$postdata = $this->postdata();

        $data = array(
            "email" => $postdata['email'],
            "password" => $postdata['password']);
    
        if($this->user->update_user($postdata['id'], $data)){
            if($postdata['email'] != "" && $postdata['name'] != ""){
                $evaluator = array('user_id' => $postdata['id'], 'name' => $postdata['name']);
                $this->evaluator->update($postdata['id'], $evaluator);

                $this->evaluator_expert_group->delete_by_evaluator($postdata['id']);
                $group_evaluator = array('expert_group_id' => $postdata['group_id'], 'evaluator_id' => $postdata['id']);
                $this->evaluator_expert_group->insert($group_evaluator);

                $this->session->set_flashdata('alert','New Evaluator has been updated');
            }
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluators');
	}

    function check_email_exist(){
        $this->layout = FALSE;

        $email = $this->input->post('email');
        $user_id = $this->input->post('user_id');
        if($this->user->is_email_exist($email, $user_id)){
            $retval = false;
        }else{
            $retval = true;
        }

        echo json_encode(array('valid' => $retval));
    }

    private function generate_username($name){
        return str_replace(' ', '_', $name).rand();
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

    function delete($id){
        $this->layout = FALSE;
        
        if($this->user->delete($id)){
            $this->session->set_flashdata('alert','Evaluator has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Evaluator can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }
}
