<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Reports extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Reports";
		$this->menu = "report";

        $this->lang->load(PATH_TO_ADMIN.'report',$this->language);

        $this->load->model('innovator');
        $this->load->model('application');
        $this->load->model('state');
        $this->load->model('application_category_innovation');

		$this->scripts[] = 'plugins/highcharts/highcharts';
        $this->scripts[] = 'plugins/bootstrap-datepicker/bootstrap-datepicker.min';
		$this->scripts[] = 'administrator/report';

        $this->styles[] = 'bootstrap-datepicker/bootstrap-datepicker.min';
    }

    public function index(){}

    function registration_number_by_date(){
        $user = $this->innovator->get_join(NULL, 1,"user_id DESC")->row_array();
        $data['from_date'] = date('Y-m-d',strtotime('-29 days',strtotime($user['created_at'])));
        $data['to_date'] = date('Y-m-d',strtotime($user['created_at']));
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_registration_number_by_date'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/registration_number_by_date',$data);
    }

    function registration_number_by_date_handler(){
        $this->layout = FALSE;
        
        $xAxis = array();
        $data = array();
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');

        $days = $this->count_interval($date_from, $date_to);
        
        for ($i=0; $i <= $days; $i++) { 
            $raw_date = strtotime('+'.$i.' days',strtotime($date_from));
            $xAxis[] = date('d-m-Y',$raw_date);
            $date = date('Y-m-d', $raw_date);
            $data[] = $this->innovator->get_join("DATE( created_at ) =  '".$date."'")->num_rows();
        }

        $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Registration Number', 'data' => $data)));
        echo json_encode($result);
    }

    function submission_number_by_date(){
        $application = $this->application->find("status >= ".APPLICATION_STATUS_SENT_APPROVAL, 1, 0,"id DESC");
        $data['from_date'] = date('Y-m-d',strtotime('-29 days',strtotime($application[0]['created_at'])));
        $data['to_date'] = date('Y-m-d',strtotime($application[0]['created_at']));
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_submission_number_by_date'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/submission_number_by_date',$data);
    }

    function submission_number_by_date_handler(){
        $this->layout = FALSE;
        
        $xAxis = array();
        $data = array();
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');

        $days = $this->count_interval($date_from, $date_to);
        
        for ($i=0; $i <= $days; $i++) { 
            $raw_date = strtotime('+'.$i.' days',strtotime($date_from));
            $xAxis[] = date('d-m-Y',$raw_date);
            $date = date('Y-m-d', $raw_date);
            $data[] = $this->application->get_number("status >= ".APPLICATION_STATUS_SENT_APPROVAL." AND DATE( created_at ) =  '".$date."'");
        }

        $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Submission Number', 'data' => $data)));
        echo json_encode($result);
    }

    private function count_interval($date_from, $date_to){
        $diff = strtotime($date_to) - strtotime($date_from);
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        return $days;
    }

    function get_detail(){
        $this->layout = FALSE;

        $date = $this->input->post('date');
        $type = $this->input->post('type');

        if($type == 1){ // registration by number
            $this->lang->load('account',$this->language);

            $columns = array("No",lang('email'),lang('name'),lang('kp_no'));
            $fields = array("no","email","name","kp_no");
            $data = $this->innovator->get_join("DATE( created_at ) =  '".date('Y-m-d',strtotime($date))."'")->result_array();
        }else if($type == 3){ //submission by number
            $columns = array("No","Innovator","Innovation","Tindakan");
            $fields = array("no","innovator_name","title","detail_innovation");
            $data = $this->application->get_join("application.status >= ".APPLICATION_STATUS_SENT_APPROVAL." AND DATE( application.created_at ) =  '".date('Y-m-d',strtotime($date))."'")->result_array();
        }else if($type == 5){
            $category = $this->input->post('category');
            $columns = array("No","Innovator","Innovation","Tindakan");
            $fields = array("no","innovator_name","title","detail_innovation");
            $data = $this->application->get_join("category_innovation_id = ".$category." AND application.status >= ".APPLICATION_STATUS_SENT_APPROVAL)->result_array();
        }

        $result = array('columns' => $columns,'data' => $data, 'fields' => $fields);
        echo json_encode($result);
    }

    function export_registration_number_by_date(){
        $this->layout = FALSE;
        $this->lang->load('account',$this->language);

        $date = $this->input->post('date');
        $from = $this->input->post('date_from');
        $to = $this->input->post('date_to');

        if($date != ""){
            $where = "DATE( created_at ) = '".date('Y-m-d',strtotime($date))."'";
        }else{
            $where = "DATE( created_at ) BETWEEN '".date('Y-m-d',strtotime($from))."' AND '".date('Y-m-d',strtotime($to))."'";
        }

        $data = $this->innovator->get_join($where)->result_array();
        $contents = $this->generate_innovator_data($data);

        $file_name = "Registration_Number_by_Date";
        $title = "Registration Number by Date";
        $columns = array("No",lang('email'),lang('name'), lang('telp_no'), lang('kp_no'),lang('address'));
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/registration_number_by_date');
        }
    }

    function export_submission_number_by_date(){
        $this->layout = FALSE;
        $this->lang->load('application',$this->language);

        $date = $this->input->post('date');
        $from = $this->input->post('date_from');
        $to = $this->input->post('date_to');

        if($date != ""){
            $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL." AND DATE( application.created_at ) =  '".date('Y-m-d',strtotime($date))."'";
        }else{
            $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL." AND DATE( application.created_at ) BETWEEN '".date('Y-m-d',strtotime($from))."' AND '".date('Y-m-d',strtotime($to))."'";
        }

        $data = $this->application->get_join($where)->result_array();
        foreach ($data as $key => $value) {
            $data[$key]['innovator'] = $this->innovator->find_one("user_id = ".$value['user_id']);
            $data[$key]['category'] = $this->application_category_innovation->find_join('application_id = '.$value['id'])->row_array();
            $data[$key]['state'] = $this->state->find_by_id($data[$key]['innovator']['state_id']);
        }
        $contents = $this->generate_innovation_data($data);

        $file_name = "Submission_Number_by_Date";
        $title = "Submission Number by Date";
        $columns = array("No",lang('team_leader'),lang('kp_no'),lang('application'),lang('team_member')."(".lang('name')."/".lang('kp_no')."/".lang('telp_no').")",lang('address'),lang('postcode'),lang('state'),lang('telp_no'),lang('email'),lang('project_title'),lang('description'),lang('problem_encountered'),lang('award_purpose'),lang('how_to_use'),lang('innovation_novelty'),lang('innovation_category'),lang('impact'),lang('community_involvement'),lang('previous_grants'),lang('special_achievement'),lang('myipo_protection'), lang('myipo_protection_desc'),lang('organization_support'),lang('submission_date'));
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/submission_number_by_date');
        }
    }

    private function generate_innovator_data($data){
        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['no'] = $no;
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['name'] = $value['name'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['kp_no'] = $value['kp_no'];
            $contents[$key]['address'] = $value['address'];
        $no++;}

        return $contents;
    }

    private function generate_innovation_data($data){
        $this->load->model('application_expert_team');
        $this->load->model('category_innovation');
        $this->load->model('application_category_innovation');
        $this->lang->load('application',$this->language);
        $application_type = unserialize(APPLICATION_TYPE);

        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['no'] = $no;
            $contents[$key]['innovator'] = $value['innovator_name'];            
            $contents[$key]['kp_no'] = $value['kp_no'];
            $contents[$key]['application'] = $application_type[$value['application_type']]['name'];
            $team_member = $this->application_expert_team->find("application_id = ".$value['id']);
            $team_member_str = "";
            foreach ($team_member as $val) {
                if($val['name'] != ""){
                    $team_member_str = "- ".$val['name']."(".$val['kp_no']."/".$val['telp_no'].")";
                }
            }

            $contents[$key]['team_member'] = $team_member_str;
            $contents[$key]['address'] = $value['address'];
            $contents[$key]['postcode'] = $value['postcode'];
            $contents[$key]['state'] = $value['state']['name'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['innovation'] = $value['title'];
            $contents[$key]['description'] = $value['description'];
            $contents[$key]['problems_encountered'] = $value['problems_encountered'];
            $contents[$key]['award_purpose'] = $value['award_purpose'];
            $contents[$key]['how_to_use'] = $value['how_to_use'];
            $contents[$key]['innovation_novelty'] = $value['innovation_novelty'];
            $app_category = $this->application_category_innovation->find_one("application_id = ".$value['id']);
            $category = $this->category_innovation->find_one("id = ".$app_category['category_innovation_id']);
            $contents[$key]['category'] = $category['name'];
            $contents[$key]['impact'] = $value['impact'];
            $contents[$key]['community_involvement'] = $value['community_involvement'];
            $contents[$key]['previous_grants'] = $value['previous_grants'];
            $contents[$key]['special_achievement'] = $value['special_achievement'];            
            $contents[$key]['myipo_protection'] = ($value['myipo_protection'] == 1 ? lang('yes') : lang('no'));
            $contents[$key]['myipo_protection_desc'] = $value['myipo_protection_desc'];
            $contents[$key]['organization_support'] = $value['organization_support'];
            $contents[$key]['submission_date'] = $value['submission_date'];
        $no++;}

        return $contents;
    }

    private function export_to_excel($file_name,$title,$columns,$fields,$contents){
        $this->load->library('PHPExcel');

        if(!$fields) {
            echo 'No result. <a href="'.base_url().PATH_TO_ADMIN.'">Back</a>'; die();
        }
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
        header('Cache-Control: max-age=0');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Magris");
        $objPHPExcel->getProperties()->setLastModifiedBy("Magris");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject("Magris");
        
        #Column Width
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
        
        #Column Title Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        #Wrap Text
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true); 
        
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $col = 0;
        $row = 1;

        foreach($columns as $column) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
            $col++;
        }
        $col = 0;
        $row++;
        foreach($contents as $content) {
            foreach($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
                $col++;
            }
            $col = 0;
            $row++;
        }
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

        // Save it as an excel 2003 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
        $objWriter->save('php://output');
        exit;
    }

    function application_number_by_category(){
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_application_number_by_category'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/application_number_by_category',$data);
    }

    function application_number_by_category_handler(){
        $this->layout = FALSE;
        $this->load->model('category_innovation');

        $data = array();
        $categories = $this->category_innovation->find_all();
        foreach ($categories as $key => $value) {
            $data[$key]['name'] = $value['name'];
            $data[$key]['y'] = $this->application->get_join("category_innovation_id = ".$value['id']." AND application.status >= ".APPLICATION_STATUS_SENT_APPROVAL)->num_rows();
            $data[$key]['val_id'] = $value['id'];
        }

        $result = array("data" => $data);
        echo json_encode($result);
    }

    function export_application_number_by_category(){
        $this->layout = FALSE;

        $category = $this->input->post('category');

        if($category != ""){
            $where = "category_innovation_id = ".$category." AND application.status >= ".APPLICATION_STATUS_SENT_APPROVAL;
        }else{
            $where = "application.status >= ".APPLICATION_STATUS_SENT_APPROVAL;
        }

        $data = $this->application->get_join($where)->result_array();
        foreach ($data as $key => $value) {
            $data[$key]['innovator'] = $this->innovator->find_one("user_id = ".$value['user_id']);
            $data[$key]['category'] = $this->application_category_innovation->find_join('application_id = '.$value['id'])->row_array();
            $data[$key]['state'] = $this->state->find_by_id($data[$key]['innovator']['state_id']);
        }
        $contents = $this->generate_innovation_data($data);

        $file_name = "Application_Number_by_Category";
        $title = "Application Number by Category";
        $columns = array("No",lang('team_leader'),lang('kp_no'),lang('application'),lang('team_member')."(".lang('name')."/".lang('kp_no')."/".lang('telp_no').")",lang('address'),lang('postcode'),lang('state'),lang('telp_no'),lang('email'),lang('project_title'),lang('description'),lang('problem_encountered'),lang('award_purpose'),lang('how_to_use'),lang('innovation_novelty'),lang('innovation_category'),lang('impact'),lang('community_involvement'),lang('previous_grants'),lang('special_achievement'),lang('myipo_protection'), lang('myipo_protection_desc'),lang('organization_support'),lang('submission_date'));
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/application_number_by_category');
        }
    }

    function top_score_by_evaluator_group(){
        $this->load->model('expert_group');

        $data['groups'] = $this->expert_group->find_all();
        $group = $this->input->post('group');
        $data['selected_group'] = $group;

        if($group != ""){
           $where = "expert_group_id = ".$group;
        }else{
            $where = "expert_group_id = 1";
        }

        $data['submissions'] = $this->top_score_by_evaluator_group_handler($where);

        if(isset($_POST['btn_export'])){
            $contents = $this->generate_top_score_data_by_group($data['submissions']);
            $group_data = $this->expert_group->find_one("id = ".$data['selected_group']);
            
            $file_name = "Top_100_Scores_by_".$group_data['name'];
            $title = "Top 100 Scores by ".$group_data['name'];
            $columns = array(lang('ranking'),lang('project_name'),lang('team_leader'),lang('scores_by_individual_evaluator'),lang('average_score'));
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }
        
        $this->load->view(PATH_TO_ADMIN.'report/top_score_by_evaluator_group', $data);
    }

    private function top_score_by_evaluator_group_handler($where = NULL){
        $this->load->model('evaluation');
        $this->load->model('evaluator_expert_group');
        $this->load->model('expert_group');
        $this->load->model('evaluator');

        $submissions = $this->evaluation->get_top_score_by_group_list($where)->result_array();
        foreach ($submissions as $key => $value) {
            $individual_eval = $this->evaluation->find('application_id = '.$value['application_id']);
            $str_individual_score = "";
            foreach ($individual_eval as $i => $eval) {
                $evaluator = $this->evaluator->find_one('user_id = '.$eval['evaluator_id']);
                $str_individual_score .= $evaluator['name']." : ".$eval['total']."<br>";
            }
            $submissions[$key]['score_by_individual'] = $str_individual_score;
        }

        return $submissions;
    }

    private function generate_top_score_data_by_group($data){
        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['rangking'] = $no;
            $contents[$key]['innovation_name'] = $value['innovation_name'];
            $contents[$key]['innovator_name'] = $value['innovator_name'];
            $contents[$key]['score_by_individual'] = str_replace("<br>", "\n", $value['score_by_individual']);
            $contents[$key]['score'] = $value['score'];
        $no++;}

        return $contents;
    }
}