<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Applications extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Applications";
		$this->menu = "application";

        $this->lang->load(PATH_TO_ADMIN.'application',$this->language);

		$this->load->model('application');
        $this->load->model('category_innovation');
        $this->load->model('innovator');
        $this->load->model('application_category_innovation');
        $this->load->model('application_picture');
        $this->load->model('application_link');
        $this->load->model('round');
        $this->load->model('country');
        $this->load->model('state');

		$this->scripts[] = 'administrator/application';
        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
        $this->load->helper('mystring_helper');
        $this->load->model('expert_group');
        $this->load->model('evaluation');
        $this->load->model('application_evaluator');
        $this->load->model('state');
        
        $categories = $this->category_innovation->find_all();
        
        $where['application.status >='] = APPLICATION_STATUS_SENT_APPROVAL;
        if(isset($_POST['status']) || isset($_POST['state_id']) || isset($_POST['year_id'])){
            $postdata = $this->postdata();
            /*if($postdata['status'] != 0 || $postdata['state_id'] != 0 || $postdata['year_id'] != 0){
                $where = array();
            }*/

            if($postdata['status'] != 0){
                $where['application.status'] = $postdata['status'];    
            }

            if($postdata['state_id'] != 0){
                $where['state_id'] = $postdata['state_id'];    
            }

            if($postdata['year_id'] != 0){
                $where['YEAR(submission_date)'] = $postdata['year_id'];    
            }
        }
        
        $applications = $this->application->get_join($where)->result_array();
        foreach ($applications as $key => $value) {
            $applications[$key]['innovator'] = $this->innovator->find_one("user_id = ".$value['user_id']);
            $applications[$key]['category'] = $this->application_category_innovation->find_join('application_id = '.$value['id'])->row_array();
            if($value['status'] == APPLICATION_STATUS_SENT_APPROVAL){
                $applications[$key]['is_evaluation_done'] = false;
            }else{
                $applications[$key]['is_evaluation_done'] = $this->application_evaluator->is_evaluation_done($value['id']);
                if(!$applications[$key]['is_evaluation_done']){
                    $applications[$key]['status'] = APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR;
                }
            }
            $applications[$key]['average_score'] = $this->get_average_score($value['id']);
            $get_evaluation = $this->evaluation->find_one("application_id = ".$value['id']);
            $applications[$key]['has_evaluation'] = ($get_evaluation ? 1 : 0);
            $applications[$key]['state'] = $this->state->find_by_id($applications[$key]['innovator']['state_id']);
        }

        $data['alert'] = $this->session->flashdata('alert');
        $data['applications'] = $applications;
        $data['status'] = unserialize(APPLICATION_STATUS_ADMIN);
        $data['states'] = $this->state->find_all();
        $data['year'] = $this->application->get_list_submission_year();

        if(isset($_POST['btn_export'])){
           $contents = $this->generate_innovation_data($applications);

            $file_name = "Submission_list";
            $title = "Submission List";
            $columns = array("No", lang('team_leader'),lang('kp_no'),lang('application'),lang('team_member')."(".lang('name')."/".lang('kp_no')."/".lang('telp_no').")",lang('address'),lang('postcode'),lang('state'),lang('telp_no'),lang('email'),lang('project_title'),lang('description'),lang('problem_encountered'),lang('award_purpose'),lang('how_to_use'),lang('innovation_novelty'),lang('innovation_category'),lang('impact'),lang('community_involvement'),lang('previous_grants'),lang('special_achievement'),lang('myipo_protection'),lang('myipo_protection_desc'),lang('organization_support'),lang('submission_date'), lang('average_score'));
            if(count($contents) > 0){
                $fields = array_keys($contents[0]);

                $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
            }else{
                redirect(base_url().PATH_TO_ADMIN.'reports/submission_number_by_date');
            }
        }else{
            $data_assign['groups'] = $this->expert_group->find_all(); 
            $data['form_assign'] = $this->load->view(PATH_TO_ADMIN.'application/form_assign', $data_assign, TRUE);

    		$this->load->view(PATH_TO_ADMIN.'application/list', $data);
        }
    }

    function view_evaluation_list($application_id){
        $this->load->model('application_evaluator');
        $this->load->model('evaluation');

        $data['application'] = $this->application->find_by_id($application_id);
        $data['evaluations'] = $this->application_evaluator->get_list_evaluation($application_id);

        if($data['evaluations'] > 0){
            foreach ($data['evaluations'] as $key => $value) {
                $evaluation = $this->evaluation->find_one("application_id = ".$value['application_id']." AND evaluator_id = ".$value['evaluator_id']);
                if($evaluation){
                    $data['evaluations'][$key]['evaluation_id'] = $evaluation['id'];
                    $data['evaluations'][$key]['score'] = $evaluation['total'];
                }else{
                    $data['evaluations'][$key]['evaluation_id'] = 0;
                    $data['evaluations'][$key]['score'] = 0;
                }
            }
        }

        $this->load->view(PATH_TO_ADMIN.'application/evaluation_list',$data);
    }

    function view_evaluation($evaluation_id){
        $this->load->model('evaluation');
        $this->load->model('evaluation_detail');
        $data['alert'] = $this->session->flashdata('alert');

        $evaluation = $this->evaluation->find_by_id($evaluation_id);
        $data['evaluation_focus'] = $this->get_evaluation_focus($evaluation['application_id']);
        $data['application'] = $this->application->find_by_id($evaluation['application_id']);
        $data['innovator'] = $this->innovator->find_by_id($data['application']['user_id']);
        $data['view_mode'] = 1;
        $evaluator_id = $evaluation['evaluator_id'];
        $data['evaluator'] = $this->evaluator->find_one('user_id = '.$evaluator_id);

        if($evaluation){
            $data['evaluation'] = $evaluation;
            $data['evaluation_detail'] = $this->evaluation_detail->find('evaluation_id = '.$evaluation['id']);
            $evaluator_id = $evaluation['evaluator_id'];
        }

        $this->load->view(PATH_TO_EVALUATOR.'application/evaluation_form',$data);
    }

    private function get_evaluation_focus($innovation_id){
        $this->load->model('evaluation_form_model');
        $this->load->model('evaluation_focus_model');
        $this->load->model('evaluation_criteria');
        $this->load->model('evaluator');

        $form = $this->evaluation_form_model->find_one();
        $evaluation_focus = $this->evaluation_focus_model->find('evaluation_form_id = '.$form['id']);
        foreach ($evaluation_focus as $key => $value) {
            $evaluation_focus[$key]['criteria'] = $this->evaluation_criteria->find("evaluation_focus_id = ".$value['id']);
        }

        return $evaluation_focus;
    }

    function view_application($id){
        $this->load->model('round');
        $this->lang->load('application',$this->language);
        $this->load->model('application_category_innovation');
        $this->load->model('application_expert_team');

        $data['category_innovation'] = $this->category_innovation->find_all();
        $data['action_url'] = 'applications/update';
        $data['alert'] = $this->session->flashdata('alert');

        if($data['application'] = $this->application->find_by_id($id)){
            $data['i_categories'] = $this->application_category_innovation->find_one("application_id = ".$id);
            $data['i_team'] = $this->application_expert_team->find("application_id = ".$id);
            $data['innovator'] = $this->innovator->get_one_join($data['application']['user_id']);
            $data['i_picture'] = $this->application_picture->find("application_id = ".$id);
            $data['i_link'] = $this->application_link->find("application_id = ".$id);
        }
        $data['view_mode'] = 1;
        $data['round'] = $this->round->find_by_id(1);
        $data['countries'] = $this->country->find_all();
        $data['states'] = $this->state->find_all();
        $data['application_type'] = unserialize(APPLICATION_TYPE);
        $data['job_type'] = unserialize(JOB_TYPE);
        $data['business_model'] = unserialize(BUSINESS_MODEL);

        $this->load->view('application/form',$data);
    }

    private function get_average_score($application_id){
        $data_score = $this->evaluation->get_average_score("application_id = ".$application_id)->row_array();
        
        return $data_score['score'];
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    function assign_evaluation(){
        $this->layout = FALSE;
        $this->load->model("application_evaluator");
        $this->load->model("evaluator_expert_group");
        $this->load->model("application_status_history");

        $postdata = $this->postdata();
        $evaluators = $this->evaluator_expert_group->find("expert_group_id = ".$postdata['group']);
        foreach ($evaluators as $evaluator) {
            $row = $this->application_evaluator->find_one("application_id = ".$postdata['application_id']." AND evaluator_id = ".$evaluator['evaluator_id']);
            if(!$row){
                $this->application_evaluator->insert(array('application_id' => $postdata['application_id'], 'evaluator_id' => $evaluator['evaluator_id']));
            }
        }

        //save approval status
        if(count($evaluators) > 0){
            $this->application->update($postdata['application_id'], array('status' =>  APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR));
            $this->application_status_history->insert(array('application_id' => $postdata['application_id'],'round_id' => 1,'status' => APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR));
        }

        $this->session->set_flashdata('alert', "Your data has been updated.");
        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    function generate_pdf($id, $auto_download = TRUE){
        $this->layout = FALSE;
        $this->lang->load('application',$this->language);
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('application_category_innovation');
        $this->load->model('application_expert_team');
        $this->load->model('application_picture');
        $this->load->model('application_link');
        $this->load->model('state');

        if($data['application'] = $this->application->find_by_id($id)){
            $get_category = $this->application_category_innovation->find_one("application_id = ".$id);
            $data['i_category'] = $this->category_innovation->find_one("id = ".$get_category['category_innovation_id']);
            $data['i_team'] = $this->application_expert_team->find("application_id = ".$id);
            $data['innovator'] = $this->innovator->get_one_join($data['application']['user_id']);
            $data['i_picture'] = $this->application_picture->find("application_id = ".$id);
            $data['i_link'] = $this->application_link->find("application_id = ".$id);
            $state = $this->state->get_join("state.id = ".$data['innovator']['state_id'])->row_array();
            $data['i_state'] = $state['name'];
            $data['i_country'] = $state['country_name'];
        }
        $data['application_type'] = unserialize(APPLICATION_TYPE);
        $data['job_type'] = unserialize(JOB_TYPE);
        $data['business_model'] = unserialize(BUSINESS_MODEL);
        
        $filename = $id."_".str_replace(" ","_",$data['application']['title']).".pdf";
        $this->application->update($id, array(
            "pdf" => $filename,
        ));

        $html = $this->load->view('application/form_pdf', $data, true);
        
        generate_pdf($html, $filename,$auto_download); 
    }

    function list_submit_handler(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['action'] == "proceed"){
            $this->proceed_to_next_round($postdata);
        }

        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    private function proceed_to_next_round($postdata){
        $this->layout = FALSE;
        $this->load->model('application_status_history');

        foreach ($postdata['ids'] as $key => $value) {
            $this->application->update($value, array('status' => APPLICATION_STATUS_APPROVED));
            $this->application_status_history->insert(array('application_id' => $value,'round_id' => 1,'status' => APPLICATION_STATUS_APPROVED));
        }

        $this->session->set_flashdata('alert', "Your data has been updated.");
    }

    private function generate_innovation_data($data){
        $this->load->model('application_expert_team');
        $this->load->model('category_innovation');
        $this->load->model('application_category_innovation');
        $this->lang->load('application',$this->language);
        $application_type = unserialize(APPLICATION_TYPE);

        $contents = array();
        $no = 1; 
    
        foreach ($data as $key => $value) {
            $contents[$key]['no'] = $no;
            $contents[$key]['innovator'] = $value['innovator_name'];            
            $contents[$key]['kp_no'] = $value['kp_no'];
            $contents[$key]['application'] = $application_type[$value['application_type']]['name'];
            $team_member = $this->application_expert_team->find("application_id = ".$value['id']);
            $team_member_str = "";
            foreach ($team_member as $val) {
                if($val['name'] != ""){
                    $team_member_str = "- ".$val['name']."(".$val['kp_no']."/".$val['telp_no'].")";
                }
            }

            $contents[$key]['team_member'] = $team_member_str;
            $contents[$key]['address'] = $value['address'];
            $contents[$key]['postcode'] = $value['postcode'];
            $contents[$key]['state'] = $value['state']['name'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['innovation'] = $value['title'];
            $contents[$key]['description'] = $value['description'];
            $contents[$key]['problems_encountered'] = $value['problems_encountered'];
            $contents[$key]['award_purpose'] = $value['award_purpose'];
            $contents[$key]['how_to_use'] = $value['how_to_use'];
            $contents[$key]['innovation_novelty'] = $value['innovation_novelty'];
            $app_category = $this->application_category_innovation->find_one("application_id = ".$value['id']);
            $category = $this->category_innovation->find_one("id = ".$app_category['category_innovation_id']);
            $contents[$key]['category'] = $category['name'];
            $contents[$key]['impact'] = $value['impact'];
            $contents[$key]['community_involvement'] = $value['community_involvement'];
            $contents[$key]['previous_grants'] = $value['previous_grants'];
            $contents[$key]['special_achievement'] = $value['special_achievement'];            
            $contents[$key]['myipo_protection'] = ($value['myipo_protection'] == 1 ? lang('yes') : lang('no'));
            $contents[$key]['myipo_protection_desc'] = $value['myipo_protection_desc'];
            $contents[$key]['organization_support'] = $value['organization_support'];
            $contents[$key]['submission_date'] = $value['submission_date'];
            $contents[$key]['average_score'] = $this->get_average_score($value['id']);
        $no++;}

        return $contents;
    }

    private function export_to_excel($file_name,$title,$columns,$fields,$contents){
        $this->load->library('PHPExcel');

        if(!$fields) {
            echo 'No result. <a href="'.base_url().PATH_TO_ADMIN.'">Back</a>'; die();
        }
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
        header('Cache-Control: max-age=0');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Magris");
        $objPHPExcel->getProperties()->setLastModifiedBy("Magris");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject("Magris");
        
        #Column Width
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
        
        #Column Title Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        #Wrap Text
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true); 
        
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $col = 0;
        $row = 1;

        foreach($columns as $column) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
            $col++;
        }
        $col = 0;
        $row++;
        foreach($contents as $content) {
            foreach($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
                $col++;
            }
            $col = 0;
            $row++;
        }
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

        // Save it as an excel 2003 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
        $objWriter->save('php://output');
        exit;
    }

    private function generate_data_application($postdata){
        $data_application = array("title" => $postdata['title'],
                                        "description" => $postdata['description'],
                                        "myipo_protection" => $postdata['myipo_protection'],
                                        "how_to_use" => $postdata['how_to_use'],
                                        "special_achievement" => $postdata['special_achievement'],
                                        "application_type" => $postdata['application_type'],
                                        "impact" => $postdata['impact'],
                                        "award_purpose" => $postdata['award_purpose'],
                                        "problems_encountered" => $postdata['problems_encountered'],
                                        "innovation_novelty" => $postdata['innovation_novelty'],
                                        "community_involvement" => $postdata['community_involvement'],
                                        "previous_grants" => $postdata['previous_grants'],
                                        "organization_support" => $postdata['organization_support'],
                                        );
        return $data_application;
    }

    private function save_category($id, $postdata){
        $this->load->model('application_category_innovation');

        foreach ($postdata['categories'] as $key => $value) {
            $data = array('application_id' => $id,
                            'category_innovation_id' => $value);
            
            $is_exist = $this->application_category_innovation->find_one('application_id = '.$id);
            if($is_exist){
                $this->application_category_innovation->update_by_application_id($id, $data);
            }else{
                $this->application_category_innovation->insert($data);
            }
        }
    }

    private function save_team_member($id, $postdata){
        $this->load->model('application_expert_team');

        foreach ($postdata['team_member_id'] as $key => $value) {
            if(isset($postdata['i_deleted_team_member_'.$key])){
                $data_member = array('application_id' => $id,
                                'name' => $postdata['team_member_'.$key],
                                'kp_no' => $postdata['team_member_kp_'.$key],
                                'telp_no' => $postdata['team_member_telp_'.$key]);

                if($postdata['team_member_id'][$key] > 0){
                    $this->application_expert_team->update($postdata['team_member_id'][$key], $data_member);
                }else{
                    $this->application_expert_team->insert($data_member);
                }
            }else{
                if($postdata['team_member_id'][$key] > 0){
                    $team_member = $this->application_expert_team->find_by_id($postdata['team_member_id'][$key]);
                    $this->application_expert_team->delete($postdata['team_member_id'][$key]);
                }
            }
        }
    }

    private function save_link($id, $postdata){
        $this->load->model('application_link');
        foreach ($postdata['link_id'] as $key => $value) {
            if(isset($postdata['i_deleted_link_'.$key])){
                $data_member = array('application_id' => $id,
                                'url' => $postdata['link_'.$key]);

                if($postdata['link_id'][$key] > 0){
                    $this->application_link->update($postdata['link_id'][$key], $data_member);
                }else{
                    $this->application_link->insert($data_member);
                }
            }else{
                if($postdata['link_id'][$key] > 0){
                    $team_member = $this->application_link->find_by_id($postdata['link_id'][$key]);
                    $this->application_link->delete($postdata['link_id'][$key]);
                }
            }
        }
    }

    function edit($id, $view_mode = 0){
        $this->lang->load('application',$this->language);
        $this->load->model('application_category_innovation');
        $this->load->model('application_expert_team');

        $data['category_innovation'] = $this->category_innovation->find_all();
        $data['action_url'] = PATH_TO_ADMIN.'applications/update';
        $data['alert'] = $this->session->flashdata('alert');

        if($data['application'] = $this->application->find_by_id($id)){
            $data['i_categories'] = $this->application_category_innovation->find_one("application_id = ".$id);
            $data['i_team'] = $this->application_expert_team->find("application_id = ".$id);
            $data['i_picture'] = $this->application_picture->find("application_id = ".$id);
            $data['i_link'] = $this->application_link->find("application_id = ".$id);
            $data['innovator'] = $this->innovator->get_one_join($data['application']['user_id']);
        }

        $data['view_mode'] = $view_mode;
        $data['round'] = $this->round->find_by_id(1);
        $data['countries'] = $this->country->find_all();
        $data['states'] = $this->state->find_all();
        $data['application_type'] = unserialize(APPLICATION_TYPE);
        $data['job_type'] = unserialize(JOB_TYPE);
        $data['business_model'] = unserialize(BUSINESS_MODEL);
        $data['is_admin'] = 1;

        $this->load->view('application/form', $data);
    }

    function update(){
        $this->load->model('application_status_history');        
        $this->layout = FALSE;

        $postdata = $this->postdata();

        if($postdata['title'] != ""){
            $data_application = $this->generate_data_application($postdata);
            $id = $postdata['id'];

            if($this->application->update($postdata["id"], $data_application)){
                //save category
                $this->save_category($id,$postdata);

                //save team member
                $this->save_team_member($id, $postdata);

                //save link
               // $this->save_link($id, $postdata);

                //save picture
                if(isset($postdata['deleted_picture'])){
                    $deleted_pict = $postdata['deleted_picture'];
                    $deleted_pict_name = $postdata['deleted_picture_name'];
                    foreach ($deleted_pict as $key => $value) {
                        if($this->application_picture->delete($value)){
                            $this->_remove($deleted_pict_name[$key],PATH_TO_APPLICATION_PICTURE);
                            $this->_remove($deleted_pict_name[$key],PATH_TO_APPLICATION_PICTURE_THUMB);
                        }
                    }
                }

                $pict_num = count($_FILES);
                for($i=0;$i<=$pict_num;$i++){
                    if(isset($_FILES['picture_'.$i])){
                        if($_FILES['picture_'.$i]['name'] != NULL){
                            $picture_name = preg_replace('/\s+/', '_', $_FILES['picture_'.$i]['name']);
                            $filename         = $id."_".$picture_name;
                            $uploaded_picture = $this->_upload($filename,'picture_'.$i,PATH_TO_APPLICATION_PICTURE,PATH_TO_APPLICATION_PICTURE_THUMB);
                            $this->application_picture->insert(array('application_id' => $id, 'filename' => $uploaded_picture));
                        }
                    }
                }

                $this->session->set_flashdata('alert','Application has been updated.');
                redirect(base_url().PATH_TO_ADMIN.'applications/edit/'.$id);          
            }else{
                $this->session->set_flashdata('alert','Appication cannot be updated.');
                redirect(base_url().PATH_TO_ADMIN.'applications/edit/'.$id);
            }
        }else{
            $this->session->set_flashdata('alert','Some fields are required.');
            redirect(base_url().PATH_TO_ADMIN.'applications/edit/'.$id);
        }
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL,$is_file = 0) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        if($is_file == 0){
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';    
        }else{
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg|pdf|doc';
        }
        
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }
    
    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".rand()."_".$attachment_name;

        return $retval;
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->application->delete($id)){
            $this->session->set_flashdata('alert','Application has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Application can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    /*Temporary function to export data*/
    function export_draft_application(){
        $this->load->helper('mystring_helper');
        $this->load->model('expert_group');
        $this->load->model('evaluation');
        $this->load->model('application_evaluator');
        $this->load->model('state');
        
        $categories = $this->category_innovation->find_all();
        
        $where = "application.status = ".APPLICATION_STATUS_DRAFT;
        
        $applications = $this->application->get_join($where)->result_array();
        foreach ($applications as $key => $value) {
            $applications[$key]['innovator'] = $this->innovator->find_one("user_id = ".$value['user_id']);
            $applications[$key]['category'] = $this->application_category_innovation->find_join('application_id = '.$value['id'])->row_array();
            $applications[$key]['state'] = $this->state->find_by_id($applications[$key]['innovator']['state_id']);
        }

        $data['applications'] = $applications;
        $data['status'] = unserialize(APPLICATION_STATUS_ADMIN);
        $data['states'] = $this->state->find_all();
        
       $contents = $this->generate_innovation_data($applications);

        $file_name = "Submission_list";
        $title = "Submission List";
        $columns = array("No",lang('project_title'), lang('team_leader'),lang('kp_no'),lang('address'),lang('postcode'),lang('state'),lang('telp_no'),lang('email'),lang('application'),lang('team_member')."(".lang('name')."/".lang('kp_no')."/".lang('telp_no').")",lang('description'),lang('how_to_use'),lang('elements_of_innovation'),lang('impact'),lang('special_achievement'),lang('selling_price'),lang('innovation_category'),lang('myipo_protection'),lang('submission_date'), lang('average_score'));
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/submission_number_by_date');
        }
    }
    /*end*/

    function get_print_pdf_url(){
        $this->layout = FALSE;
        $id = $this->input->post('id');

        $this->generate_pdf($id, FALSE);

        $application = $this->application->find_by_id($id);
        $url = "";

        if($application){
            $url = base_url().'assets/attachment/application_file/'.$application['pdf'];
        }
        echo json_encode(array('url' => $url));
    }
}
