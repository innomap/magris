<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Contacts extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Contact us";
		$this->menu = "contact";

		$this->load->model('contact_us');

		$this->lang->load('account',$this->language);

		$this->scripts[] = 'administrator/contact';
    }

    public function index(){
    	$data['contacts'] = $this->contact_us->find(NULL, NULL, NULL, "id DESC");
		$this->load->view(PATH_TO_ADMIN.'contact/list', $data);
    }
}
