<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Innovators extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Innovator";
		$this->menu = "innovator";

		$this->load->model('innovator');
        $this->load->model('user');

		$this->lang->load('account',$this->language);

		$this->scripts[] = 'administrator/innovator';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['innovators'] = $this->innovator->get_join()->result_array();

		$this->load->view(PATH_TO_ADMIN.'innovator/list', $data);
    }

    function activate($user_id){
    	$this->layout = FALSE;
    	if($this->user->update($user_id, array('status' => USER_STATUS_ACTIVE))){
    		$this->session->set_flashdata('alert','The user has been activated.');
    	}else{
    		$this->session->set_flashdata('alert','An error occured. Please try again later.');
    	}

    	redirect(base_url().PATH_TO_ADMIN.'innovators');
    }
}
