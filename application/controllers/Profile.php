<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Profile extends Common {

	function __construct() {
		parent::__construct();

		$this->load->model('innovator');
        $this->load->model('user');
        $this->load->model('country');
        $this->load->model('state');

		$this->title = "Edit Profile";
        $this->menu = "profile";

		$this->lang->load('account',$this->language);

        $this->scripts[] = 'site/profile';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);
        if($data['innovator']){
            $split_kp_no = explode("-", $data['innovator']['kp_no']);
            $data['innovator']['kp_no_1'] = $split_kp_no[0];
            $data['innovator']['kp_no_2'] = $split_kp_no[1];
            $data['innovator']['kp_no_3'] = $split_kp_no[2];
        }
        $data['countries'] = $this->country->find_all();
        $data['states'] = $this->state->find_all();
		$this->load->view('profile/form',$data);
    }

    function save(){
        $this->layout = FALSE;
        $status = 0;

        $user_id = $this->userdata['id'];
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $kp_no_1 = $this->input->post('kp_no_1');
        $kp_no_2 = $this->input->post('kp_no_2');
        $kp_no_3 = $this->input->post('kp_no_3');
        $telp_no = $this->input->post('telp_no');
        $address = $this->input->post('address');
        $postcode = $this->input->post('postcode');
        $state_id = $this->input->post('state_id');

        if($name != ""){
            if($password != ""){
                $data['email'] = $email;
                $data['password'] = $password;
            }
            if(isset($data)){
                $this->user->update_user($user_id, $data);
            }
            $kp_no = $kp_no_1."-".$kp_no_2."-".$kp_no_3;
            $innovator = array("name" => $name,
                                "kp_no" => $kp_no,
                                "telp_no" => $telp_no,
                                "address" => $address,
                                "postcode" => $postcode,
                                "state_id" => $state_id);

            if($this->innovator->update($user_id, $innovator)){
                $this->session->set_flashdata('alert','Data profile has been updated');
            }else{
                $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
            }
        }

        redirect(base_url().'profile');
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".$attachment_name;

        return $retval;
    }

    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }
}
