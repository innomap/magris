<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Accounts extends Common {

	function __construct() {
		parent::__construct("account");

		$this->load->model('user');
		$this->load->model('innovator');
		$this->load->model('evaluator');

		$this->lang->load('account',$this->language);
		$this->menu = "account";
		$this->submenu = "home";	

		$this->scripts[] = 'site/account'; 
    }

    public function index(){
		$this->user = $this->user_session->get_user();
		if ($this->user) {
			redirect(base_url().'applications');
		}else{
			$this->title = "Home";
			$data['alert'] = $this->session->flashdata('alert');
			$this->load->view('site/index',$data);
		}
    }

	public function login(){
		$this->title = "Login";
		$data['alert'] = $this->session->flashdata('alert');
		$this->load->view('account/form_login',$data);
	}

	function registration(){
		$this->load->model('country');
		$this->load->model('state');
		$this->title = "Registration";
		$this->scripts[] = 'jquery.form';
		$this->submenu = "registration";

		$data['alert'] = $this->session->flashdata('alert');
		$data['countries'] = $this->country->find_all();
		$data['states'] = $this->state->find_all();
		$this->load->view('account/form_register',$data);
	}

	function login_auth(){
		$this->layout = FALSE;

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$this->auth($email, $password);
	}

	private function auth($email, $password){
		$response = array();

		$user = $this->user->find_one('email = "'.$email.'" AND (role_id = '.ROLE_INNOVATOR.' OR role_id = '.ROLE_EVALUATOR.')');
		if($user != NULL){
			if($this->check_password($user['email'], $password, $user['password'])){
				$data_session = array('id' => $user['id'],'email' => $user['email'], 'role_id' => $user['role_id']);
				
				$status = 0;
				if($user['status'] == USER_STATUS_ACTIVE){
					if($user['role_id'] == ROLE_INNOVATOR){
						$innovator = $this->innovator->find_one("user_id = ".$user['id']);
						if($innovator){
							$status = 1;
							$data_session['name'] = $innovator['name'];
							$this->user_session->set_user($data_session);
							redirect(base_url().'accounts');	
						}
					}else{
						$evaluator = $this->evaluator->find_one("user_id = ".$user['id']);
						if($evaluator){
							$data_session['name'] = $evaluator['name'];
							$this->user_session->set_evaluator($data_session);
							redirect(base_url().PATH_TO_EVALUATOR.'applications');
						}
					}
				}else{
					$status = -1;
				}

				if($status == 0 || $status == -1){
					if($status == 0){
						$this->session->set_flashdata('alert','Your email is not registered in our list.');
					}else{
						$this->session->set_flashdata('alert','This account must be activated. The activation link has been sent to your email address.');
					}
					
					redirect(base_url());
				}
				
			}else{
				$this->session->set_flashdata('alert','The email or password you entered is incorrect.');
				redirect(base_url());
			}
		}else{
			$this->session->set_flashdata('alert','The email or password you entered is incorrect.');
			redirect(base_url());
		}
	}

	private function check_password($email, $password, $hash) {
		$password = $this->user->get_hash($email, $password);
		if($password == $hash){
			return true;
		}else{
			return false;
		}
	}

	function register_handler(){
		$this->layout = FALSE;
		$this->load->helper('phpmailer');
		$status = 0;

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$name = $this->input->post('name');
		$kp_no_1 = $this->input->post('kp_no_1');
		$kp_no_2 = $this->input->post('kp_no_2');
		$kp_no_3 = $this->input->post('kp_no_3');
		$telp_no = $this->input->post('telp_no');
		$address = $this->input->post('address');
		$postcode = $this->input->post('postcode');
		$state_id = $this->input->post('state_id');

		if($email != "" && $name != ""){
			if($this->user->is_email_exist($email)){
				//$this->session->set_flashdata('alert','Sorry, your email has been registered.');
				$message = "Sorry, your email has been registered.";
			}else{
				$username = $this->generate_username($name);
				$data = array(
					"email" => $email,
					"password" => $password,
					"status" => USER_STATUS_ACTIVE,
					"role_id" => ROLE_INNOVATOR,
					"created_at" => date('Y-m-d H:i:s'));

				$kp_no = $kp_no_1."-".$kp_no_2."-".$kp_no_3;
				if($id = $this->user->insert_user($data)){
					$innovator = array("user_id" => $id,
										"name" => $name,
										"kp_no" => $kp_no,
										"telp_no" => $telp_no,
										"address" => $address,
										"postcode" => $postcode,
										"state_id" => $state_id);

					if($this->innovator->insert($innovator)){
						$status = 1;
						$subject = "Activation Account on MaGRIs 2016";
						$token = md5($id.",".$id);
						$content_message = "Hi {$innovator['name']}, 
				                    <br/><br/>
				                    Akaun MaGRIs anda telah berjaya didaftarkan. Klik pautan berikut untuk mengaktifkan akaun anda:
				                    <br/><br/>
				                    Pautan : ".base_url()."accounts/activate/{$id}/{$token}
				                    <br/><br/><br/>
				                    Terimakasih, <br/>
				                    <i>Pihak Sekretriat MaGRIs</i>";

			        	$mail_param = array(
				            'to' => $data['email'],
				            'subject' => $subject,
				            'message' => $content_message,
				        );
				        //$msg_email = phpmailer_send($mail_param, true);
						//$message = "Anda telah berdaftar dan emel pengaktifan akan dihantar ke email anda. Klik pautan pengaktifan tersebut untuk mengaktifan akaun MaGRIs anda.";
						$message = "Akaun MaGRIs anda telah berjaya didaftarkan dan telah diaktifkan.";
					}else{
						$message = "Sorry, an error occurred, please try again later.";	
					}
				}else{
					$message = "Sorry, an error occurred, please try again later.";
				}
			}
		}else{
			$message = "Email and Name field are required";
		}

		$result = array('status' => $status,'message' => $message);
		echo json_encode($result);
	}

	private function generate_username($name){
		return str_replace(' ', '_', $name).rand();
	}

	public function logout() {
		$this->user_session->clear();
		redirect(base_url());
	}

	function check_email_exist(){
		$this->layout = FALSE;

		$email = $this->input->post('email');
		$id = (isset($_POST['id']) ? $this->input->post('id') : 0);
		if($this->user->is_email_exist($email, $id)){
			$retval = false;
		}else{
			$retval = true;
		}

		echo json_encode(array('valid' => $retval));
	}

	private function _upload($name,$attachment,$upload_path, $thumb_path = NULL) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".$attachment_name;

        return $retval;
    }

    function forgot_password(){
    	$this->title = "Reset Password";

		$data['alert'] = $this->session->flashdata('alert');
		$this->load->view('account/form_forgot_password',$data);
    }

    function forgot_password_handler(){
		$this->load->helper('phpmailer');
		$this->load->model('forgot_password_token');

		$email = $this->input->post('email');

		$user = $this->user->find_one('email = "'.$email.'" AND status = "'.USER_STATUS_ACTIVE.'" AND role_id = '.ROLE_INNOVATOR);
		
		if(!$user){
			$this->session->set_flashdata('alert','Email is not registered as MaGRIs 2016 user');
			redirect('forgot_password');
		}

		$innovator_name = $this->innovator->get_innovator_name($user['id']);
		$token = md5($user['id'].date("Y-m-d H:i:s"));
		
		$subject = "Reset your password on MaGRIs 2016";
		$message = "Hi {$innovator_name}, 
                    <br/><br/>
                    We got a request to reset your MaGRIs 2016 password. Click the following link to create a new password : 
                    <br/><br/>
                    ".base_url()."accounts/create_new_password/{$token}
                    <br/><br/><br/>
                    If you ignore this message, your password won't be changed.
                    <br/><br/><br/>
                    Thank you, <br/>
                    MaGRIs 2016 Team";

        if($this->forgot_password_token->insert_token(array('user_id' => $user['id'],'token' => $token))){
        	$mail_param = array(
	            'to' => $email,
	            'subject' => $subject,
	            'message' => $message,
	        );
        	$msg_email = phpmailer_send($mail_param, true);
	        $this->session->set_flashdata('alert','Email has been sent to '.$email.'.');
			redirect('forgot_password');
        }
	}

	function create_new_password($token){
		$this->title = "Create New Password";
		$this->load->model('forgot_password_token');

		$data['alert'] = $this->session->flashdata('alert');
		$is_token_exist = $this->forgot_password_token->find_one("token = '".$token."'");
		$data['is_token_exist'] = $is_token_exist;
		$data['token'] = $token;
		$this->load->view('account/form_change_password',$data);
	}

	function create_new_password_handler(){
		$this->load->model('forgot_password_token');
		
		$password = $this->input->post('password');
		$token = $this->input->post('token');
		$data_token = $this->forgot_password_token->find_one("token = '".$token."'");
		$user = $this->user->find_by_id($data_token['user_id']);
		if($this->user->update_user($user['id'], array("email" => $user['email'],"password" => $password))){
			$this->forgot_password_token->delete_where("token = '".$token."'");
        	$this->session->set_flashdata('alert','New password has been created. You can login with your new password');
			redirect(base_url());
        }
	}

    function test_email(){
		$this->load->helper('phpmailer');

		$mail_param = array(
            'to' => "kiki@mobilus-interactive.com",
            'subject' => "Test MaGRIs",
            'message' => "Test email MaGRIs 2016",
            'debug' => 1
        );

        $msg_email = phpmailer_send($mail_param, true);
        echo $msg_email;
	}

	function activate($user_id, $token){
		$generate_token = md5($user_id.",".$user_id);
		if($generate_token == $token){
			$user = $this->user->find_one("id = ".$user_id." AND status = ".USER_STATUS_NON_ACTIVE);
			if($user){
				if($this->user->update($user['id'], array('status' => USER_STATUS_ACTIVE))){
					$this->session->set_flashdata('alert','Thank You! Your account has been activated. You may now log in');
					redirect(base_url());
				}
			}else{
				redirect('not_found');
			}
		}else{
			redirect('not_found');
		}
	}

	function registration_success(){
        $data['alert'] = "";
        $this->load->view('account/registration_success',$data);
    }
}
