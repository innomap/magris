<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	function __construct($module = NULL) {
		parent::__construct();

		$this->layout = DEFAULT_LAYOUT;

		$this->language = LANGUAGE_MELAYU;
		$this->submenu = "";

		if($module != "account" && $module != "site"){
			$this->userdata = $this->user_session->get_user();
			if (!$this->userdata) {
				redirect(base_url());
			}	
		}

        $this->ol_user = $this->user_session->get_user();
    }

    function generate_pdf($id, $auto_download = TRUE){
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->lang->load('application',$this->language);
        $this->load->model('application');
        $this->load->model('innovator');
        $this->load->model('category_innovation');
        $this->load->model('application_category_innovation');
        $this->load->model('application_expert_team');
        $this->load->model('application_picture');
        $this->load->model('application_link');
        $this->load->model('state');

        $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);

        if($data['application'] = $this->application->find_by_id($id)){
            $get_category = $this->application_category_innovation->find_one("application_id = ".$id);
            $data['i_category'] = $this->category_innovation->find_one("id = ".$get_category['category_innovation_id']);
            $data['i_team'] = $this->application_expert_team->find("application_id = ".$id);
            $data['i_picture'] = $this->application_picture->find("application_id = ".$id);
            $data['i_link'] = $this->application_link->find("application_id = ".$id);
            $state = $this->state->get_join("state.id = ".$data['innovator']['state_id'])->row_array();
            $data['i_state'] = $state['name'];
            $data['i_country'] = $state['country_name'];
        }

        $data['application_type'] = unserialize(APPLICATION_TYPE);
        $data['job_type'] = unserialize(JOB_TYPE);
        $data['business_model'] = unserialize(BUSINESS_MODEL);

        $filename = $id."_".str_replace(" ","_",$data['application']['title']).".pdf";

        $this->application->update($id, array(
            "pdf" => $filename,
        ));
        
        $html = $this->load->view('application/form_pdf', $data, true);

        generate_pdf($html, $filename,$auto_download); 
    }

    function download_pdf($id, $download_id){
    	if($download_id == md5($id."_".$id)){
    		$this->generate_pdf($id);
    	}
    }
}