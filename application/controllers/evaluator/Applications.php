<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_EVALUATOR.'/Common.php');
class Applications extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Applications";
		$this->menu = "application";

		$this->load->model('application');
		$this->load->model('application_status_history');
        $this->load->model('innovator');
        $this->load->model('evaluation');
        $this->load->model('evaluation_detail');
        $this->load->model('evaluator');
        $this->load->model('evaluation_focus_model');
        $this->load->model('evaluation_criteria');

		$this->lang->load('application',$this->language);

        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = PATH_TO_EVALUATOR.'application';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
    	$this->load->helper('mystring_helper');
        $this->load->model('application_evaluator');
        $this->load->model('evaluation_detail');
    	
    	$applications = $this->application_evaluator->get_list_applications("evaluator_id = ".$this->userdata['id']." AND status != ".APPLICATION_STATUS_DRAFT);

    	$list = array();
    	foreach ($applications as $key => $value) {
            $value['status'] = $value['status'];
            $value['innovator'] = $this->innovator->get_innovator_name($value['user_id']);
            $evaluation = $this->evaluation->find_one("application_id = ".$value['application_id']." AND evaluator_id = ".$this->userdata['id']);
            if($evaluation){
                $value['has_evaluation_id'] = true;
                $value['is_complete'] = $this->evaluation_detail->find_one("evaluation_id = ".$evaluation['id']);
            }else{
                $value['has_evaluation_id'] = false;
                $value['is_complete'] = false;
                $value['status'] = APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR;
            }

            $list[] = $value;
    	}

    	$data['alert'] = $this->session->flashdata('alert');
    	$data['applications'] = $list;
    	$data['status'] = unserialize(APPLICATION_STATUS_EVALUATOR);
		$this->load->view(PATH_TO_EVALUATOR.'application/list',$data);
    }

    function view($id){
        $this->load->model('round');
        $this->lang->load('application',$this->language);
        $this->load->model('application_category_innovation');
        $this->load->model('application_expert_team');
        $this->load->model('category_innovation');
        $this->load->model('application_picture');
        $this->load->model('application_link');
        $this->load->model('country');
        $this->load->model('state');

        $data['category_innovation'] = $this->category_innovation->find_all();
        $data['action_url'] = 'applications/update';
        $data['alert'] = $this->session->flashdata('alert');

        if($data['application'] = $this->application->find_by_id($id)){
            $data['i_categories'] = $this->application_category_innovation->find_one("application_id = ".$id);
            $data['i_team'] = $this->application_expert_team->find("application_id = ".$id);
            $data['innovator'] = $this->innovator->get_one_join($data['application']['user_id']);
            $data['i_link'] = $this->application_link->find("application_id = ".$id);
            $data['i_picture'] = $this->application_picture->find("application_id = ".$id);
        }
        $data['view_mode'] = 1;
        $data['round'] = $this->round->find_by_id(1);
        $data['countries'] = $this->country->find_all();
        $data['states'] = $this->state->find_all();
        $data['application_type'] = unserialize(APPLICATION_TYPE);

        $this->load->view('application/form',$data);
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_EVALUATOR.'applications');
    }

    function view_evaluation($id){
        $this->evaluation($id, 1);
    }

    function evaluation($id, $view_mode = 0){  
        $data['evaluation_focus'] = $this->get_evaluation_focus($id);
        $data['alert'] = $this->session->flashdata('alert');
        $data['application'] = $this->application->find_by_id($id);
        $data['innovator'] = $this->innovator->find_by_id($data['application']['user_id']);
        $evaluator_id = $this->userdata['id'];
        $data['evaluator'] = $this->evaluator->find_one('user_id = '.$evaluator_id);
        $data['view_mode'] = $view_mode;
        $evaluation = $this->evaluation->find_one("application_id = ".$id." AND evaluator_id = ".$this->userdata['id']);

        if($evaluation){
            $data['evaluation'] = $evaluation;
            $data['evaluation_detail'] = $this->evaluation_detail->find('evaluation_id = '.$evaluation['id']);
            $evaluator_id = $evaluation['evaluator_id'];
        }

        $this->load->view(PATH_TO_EVALUATOR.'application/evaluation_form',$data);
    }

    function evaluation_handler(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $evaluation_id = $postdata['evaluation_id'];
        $mara_round = 1;

        $data = array('application_id' => $postdata['application_id'],
                      'evaluator_id' => $this->userdata['id'],
                      'comment' => $postdata['comment'],
                      'total' => $postdata['sum_total']
                      );
        $row = $this->evaluation->find_one("application_id = ".$data['application_id']." AND evaluator_id = ".$this->userdata['id']);
        if($row){
            $evaluation_id = $row['id'];
        }

        if($evaluation_id > 0){
            if($this->evaluation->update($evaluation_id, $data)){
                if($this->evaluation_detail->delete_by_evaluation($evaluation_id)){
                    $this->insert_evaluation_detail($evaluation_id,$postdata);
                }
            }
        }else{
            $timestamp = date('Y-m-d G:i:s');
            $data['created_at'] = $timestamp;
            if($evaluation_id = $this->evaluation->insert($data)){
               $this->insert_evaluation_detail($evaluation_id,$postdata);
            }
        }

        $get_detail = $this->evaluation_detail->find_one('evaluation_id = '.$evaluation_id);
        if($get_detail){
            $this->application->update($data['application_id'], array('status' => APPLICATION_STATUS_EVALUATED));        
            $this->application_status_history->insert(array('application_id' => $data['application_id'],'round_id' => 1,'status' => APPLICATION_STATUS_EVALUATED));
            $this->session->set_flashdata('alert', "Your data has been saved.");
            redirect(base_url().PATH_TO_EVALUATOR.'applications');
        }else{
            $this->session->set_flashdata('alert', "Please complete the evaluation form");
            redirect(base_url().PATH_TO_EVALUATOR.'applications/evaluation/'.$data['application_id']);
        }
    }

    private function insert_evaluation_detail($id, $postdata){
        $evaluation_focus = $this->get_evaluation_focus($postdata['application_id']);

        foreach ($evaluation_focus as $key => $value) {
            foreach ($value['criteria'] as $i => $criteria){
                $input = $postdata['input_'.$value['id']."_".$criteria['id']];

                $data_criteria = array('evaluation_id' => $id,'eval_criteria_id' => $criteria['id'], 'point' => $input);
                $this->evaluation_detail->insert($data_criteria);
            }
        }
    }

    private function get_evaluation_focus($application_id){
        $this->load->model('evaluation_form_model');

        $form = $this->evaluation_form_model->find_one();
        $evaluation_focus = $this->evaluation_focus_model->find('evaluation_form_id = '.$form['id']);
        foreach ($evaluation_focus as $key => $value) {
            $evaluation_focus[$key]['criteria'] = $this->evaluation_criteria->find("evaluation_focus_id = ".$value['id']);
        }

        return $evaluation_focus;
    }

    /*Temporary function for testing purpose*/
    function test_evaluation_form($id, $view_mode = 0){ 
        $data['evaluation_focus'] = $this->get_evaluation_focus($id);
        $data['alert'] = $this->session->flashdata('alert');
        $data['application'] = $this->application->find_by_id($id);
        $data['innovator'] = $this->innovator->find_by_id($data['application']['user_id']);
        $evaluator_id = $this->userdata['id'];
        $data['evaluator'] = $this->evaluator->find_one('user_id = '.$evaluator_id);
        $data['view_mode'] = $view_mode;
        $evaluation = $this->evaluation->find_one("application_id = ".$id." AND evaluator_id = ".$this->userdata['id']);

        if($evaluation){
            $data['evaluation'] = $evaluation;
            $data['evaluation_detail'] = $this->evaluation_detail->find('evaluation_id = '.$evaluation['id']);
            $evaluator_id = $evaluation['evaluator_id'];
        }

        $this->load->view(PATH_TO_EVALUATOR.'application/evaluation_form',$data);
    }
}

?>