<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Applications extends Common {

	function __construct() {
		parent::__construct();

		$this->load->model('user_session');
		$this->load->model('application');
        $this->load->model('innovator');
		$this->load->model('category_innovation');
        $this->load->model('application_link');
        $this->load->model('application_picture');
        $this->load->model('round');
        $this->load->model('country');
        $this->load->model('state');

		$this->title = "Application";
        $this->menu = "application";

		$this->lang->load('application',$this->language);

        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = 'jquery.form';
        $this->scripts[] = 'site/application';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
    	$this->load->helper("mystring_helper");
        $this->load->model('application_evaluator');

    	$data['alert'] = $this->session->flashdata('alert');
        $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
    	$data['applications'] = $this->application->find("user_id = ".$this->userdata['id']);
        foreach ($data['applications'] as $key => $value) {
            $data['round'] = $this->round->find_by_id($value['round_id']);
            if($value['status'] >= APPLICATION_STATUS_EVALUATED){
                $is_evaluation_done = $this->application_evaluator->is_evaluation_done($value['id']);
                if(!$is_evaluation_done){
                    $data['applications'][$key]['status'] = APPLICATION_STATUS_ASSIGNED_TO_EVALUATOR;
                }
            }
        }
        $data['status'] = unserialize(APPLICATION_STATUS);
		$this->load->view('application/list',$data);
    }

    function add(){
    	$data['action_url'] = 'applications/store';
        $data['alert'] = $this->session->flashdata('alert');
        $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);
        $data['category_innovation'] = $this->category_innovation->find_all();
        $data['view_mode'] = 0;
        $data['round'] = $this->round->find_by_id(1);
        $data['countries'] = $this->country->find_all();
        $data['states'] = $this->state->find_all();
        $data['application_type'] = unserialize(APPLICATION_TYPE);
        $data['job_type'] = unserialize(JOB_TYPE);
        $data['business_model'] = unserialize(BUSINESS_MODEL);

    	$this->load->view('application/form', $data);
    }

    function store(){
        $this->load->model('application_status_history');
        $this->layout = FALSE;

        $postdata = $this->postdata();

        $data_application = $this->generate_data_application($postdata);
        $data_application['created_at'] = date('Y-m-d H:i:s');
        if($id = $this->application->insert($data_application)){
            //save category
            $this->save_category($id, $postdata);

            //save team member
            $this->save_team_member($id, $postdata);

            //save link
        //    $this->save_link($id, $postdata);

            //save history
            $this->application_status_history->insert(array('application_id' => $id,'round_id' => 1, 'status' => APPLICATION_STATUS_DRAFT));
            if(isset($_POST['btn_send_for_approval'])){
                $this->application->update($id, array('status' => APPLICATION_STATUS_SENT_APPROVAL,'submission_date' => date('Y-m-d H:i:s')));
                $this->application_status_history->insert(array('application_id' => $id,'round_id' => 1,'status' => APPLICATION_STATUS_SENT_APPROVAL));
            }

            //save picture
            $pict_num = count($_FILES);
            for($i=0;$i<=$pict_num;$i++){
                if(isset($_FILES['picture_'.$i])){
                    if($_FILES['picture_'.$i]['name'] != NULL){
                        $filename         = $this->generate_filename($id, $_FILES['picture_'.$i]['name']);
                        $uploaded_picture = $this->_upload($filename,'picture_'.$i,PATH_TO_APPLICATION_PICTURE,PATH_TO_APPLICATION_PICTURE_THUMB);
                        $this->application_picture->insert(array('application_id' => $id, 'filename' => $uploaded_picture));
                    }
                }
            }

            if(isset($_POST['btn_send_for_approval'])){
                $this->session->set_flashdata('alert_dialog',$id);
                redirect(base_url().'applications');
            }else{
                redirect(base_url().'applications/edit/'.$id);
            }
            
        }
    }

    private function generate_data_application($postdata){
        $data_application = array("user_id" => $this->userdata['id'],
                                        "title" => $postdata['title'],
                                        "description" => $postdata['description'],
                                        "myipo_protection" => $postdata['myipo_protection'],
                                        "myipo_protection_desc" => $postdata['myipo_protection_desc'],
                                        "how_to_use" => $postdata['how_to_use'],
                                        "special_achievement" => $postdata['special_achievement'],
                                        "application_type" => $postdata['application_type'],
                                        "impact" => $postdata['impact'],
                                        "award_purpose" => $postdata['award_purpose'],
                                        "problems_encountered" => $postdata['problems_encountered'],
                                        "innovation_novelty" => $postdata['innovation_novelty'],
                                        "community_involvement" => $postdata['community_involvement'],
                                        "previous_grants" => $postdata['previous_grants'],
                                        "organization_support" => $postdata['organization_support'],
                                        "round_id" => 1,
                                        "status" => APPLICATION_STATUS_DRAFT
                                        );
        return $data_application;
    }

    private function save_category($id, $postdata){
        $this->load->model('application_category_innovation');

        foreach ($postdata['categories'] as $key => $value) {
            $data = array('application_id' => $id,
                            'category_innovation_id' => $value);
            
            $is_exist = $this->application_category_innovation->find_one('application_id = '.$id);
            if($is_exist){
                $this->application_category_innovation->update_by_application_id($id, $data);
            }else{
                $this->application_category_innovation->insert($data);
            }
        }
    }

    private function save_team_member($id, $postdata){
        $this->load->model('application_expert_team');

        foreach ($postdata['team_member_id'] as $key => $value) {
            if(isset($postdata['i_deleted_team_member_'.$key])){
                $data_member = array('application_id' => $id,
                                'name' => $postdata['team_member_'.$key],
                                'kp_no' => $postdata['team_member_kp_'.$key],
                                'telp_no' => $postdata['team_member_telp_'.$key]);

                if($postdata['team_member_id'][$key] > 0){
                    $this->application_expert_team->update($postdata['team_member_id'][$key], $data_member);
                }else{
                    $this->application_expert_team->insert($data_member);
                }
            }else{
                if($postdata['team_member_id'][$key] > 0){
                    $team_member = $this->application_expert_team->find_by_id($postdata['team_member_id'][$key]);
                    $this->application_expert_team->delete($postdata['team_member_id'][$key]);
                }
            }
        }
    }

    private function save_link($id, $postdata){
        $this->load->model('application_link');
        foreach ($postdata['link_id'] as $key => $value) {
            if(isset($postdata['i_deleted_link_'.$key])){
                $data_member = array('application_id' => $id,
                                'url' => $postdata['link_'.$key]);

                if($postdata['link_id'][$key] > 0){
                    $this->application_link->update($postdata['link_id'][$key], $data_member);
                }else{
                    $this->application_link->insert($data_member);
                }
            }else{
                if($postdata['link_id'][$key] > 0){
                    $team_member = $this->application_link->find_by_id($postdata['link_id'][$key]);
                    $this->application_link->delete($postdata['link_id'][$key]);
                }
            }
        }
    }


    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect('applications');
    }

    function edit($id, $view_mode = 0){
        $this->load->model('application_category_innovation');
        $this->load->model('application_expert_team');

        $data['category_innovation'] = $this->category_innovation->find_all();
        $data['action_url'] = 'applications/update';
        $data['alert'] = $this->session->flashdata('alert');

        if($data['application'] = $this->application->find_by_id($id)){
            $data['i_categories'] = $this->application_category_innovation->find_one("application_id = ".$id);
            $data['i_team'] = $this->application_expert_team->find("application_id = ".$id);
            $data['i_picture'] = $this->application_picture->find("application_id = ".$id);
            $data['i_link'] = $this->application_link->find("application_id = ".$id);
        }

        $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);
        $data['view_mode'] = $view_mode;
        $data['round'] = $this->round->find_by_id(1);
        $data['countries'] = $this->country->find_all();
        $data['states'] = $this->state->find_all();
        $data['application_type'] = unserialize(APPLICATION_TYPE);
        $data['job_type'] = unserialize(JOB_TYPE);
        $data['business_model'] = unserialize(BUSINESS_MODEL);

        $this->load->view('application/form', $data);
    }

    function update(){
        $this->load->model('application_status_history');        
        $this->layout = FALSE;

        $postdata = $this->postdata();

        if($postdata['title'] != ""){
            $data_application = $this->generate_data_application($postdata);
            $id = $postdata['id'];
            $get_app = $this->application->find_one("id = ".$id);

            if(isset($_POST['btn_send_for_approval']) || $get_app['status'] == APPLICATION_STATUS_SENT_APPROVAL){
                $data_application['submission_date'] = date('Y-m-d H:i:s');
                $status = APPLICATION_STATUS_SENT_APPROVAL;
            }else{
                $status = APPLICATION_STATUS_DRAFT;
            }
            if($this->application->update($postdata["id"], $data_application)){
                //save category
                $this->save_category($id,$postdata);

                //save team member
                $this->save_team_member($id, $postdata);

                //save link
            //    $this->save_link($id, $postdata);

                //save picture
                if(isset($postdata['deleted_picture'])){
                    $deleted_pict = $postdata['deleted_picture'];
                    $deleted_pict_name = $postdata['deleted_picture_name'];
                    foreach ($deleted_pict as $key => $value) {
                        if($this->application_picture->delete($value)){
                            $this->_remove($deleted_pict_name[$key],PATH_TO_APPLICATION_PICTURE);
                            $this->_remove($deleted_pict_name[$key],PATH_TO_APPLICATION_PICTURE_THUMB);
                        }
                    }
                }

                $pict_num = count($_FILES);
                for($i=0;$i<=$pict_num;$i++){
                    if(isset($_FILES['picture_'.$i])){
                        if($_FILES['picture_'.$i]['name'] != NULL){
                            $picture_name = preg_replace('/\s+/', '_', $_FILES['picture_'.$i]['name']);
                            $filename         = $id."_".$picture_name;
                            $uploaded_picture = $this->_upload($filename,'picture_'.$i,PATH_TO_APPLICATION_PICTURE,PATH_TO_APPLICATION_PICTURE_THUMB);
                            $this->application_picture->insert(array('application_id' => $id, 'filename' => $uploaded_picture));
                        }
                    }
                }

                if($status == APPLICATION_STATUS_SENT_APPROVAL){
                    if($this->application->update($id, array('status' => APPLICATION_STATUS_SENT_APPROVAL))){
                        $this->application_status_history->insert(array('application_id' => $id,'round_id' => 1, 'status' => APPLICATION_STATUS_SENT_APPROVAL));
                    }
                    $this->session->set_flashdata('alert_dialog',$id);
                    redirect(base_url().'applications');
                }else{
                    $this->session->set_flashdata('alert','Application has been updated.');
                    redirect(base_url().'applications/edit/'.$id);
                }                
            }else{
                $this->session->set_flashdata('alert','Appication cannot be updated.');
                redirect(base_url().'applications/edit/'.$id);
            }
        }else{
            $this->session->set_flashdata('alert','Some fields are required.');
            redirect(base_url().'applications/edit/'.$id);
        }
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->application->delete($id)){
            $this->session->set_flashdata('alert','Application has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Application can not be deleted.');
        }

        redirect(base_url().'applications');
    }

    function view($id){
        $this->edit($id, 1);
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL,$is_file = 0) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        if($is_file == 0){
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';    
        }else{
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg|pdf|doc';
        }
        
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }
    
    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".rand()."_".$attachment_name;

        return $retval;
    }
}
