<?php

	$lang['action'] = "Action";
	$lang['edit'] = "Edit";
	$lang['delete'] = "Delete";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['delete_evaluator'] = "Delete Evaluator";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
	$lang['email'] = "Email";
	$lang['name'] = "Name";
	$lang['new_evaluator'] = "New Evaluator";
	$lang['evaluator'] = "Evaluator";
	$lang['password'] = "Password";
	$lang['retype_password'] = "Retype Password";
	$lang['change_password'] = "Change Password";
	$lang['group_name'] = "Group Name";
?>