<?php

	$lang['action'] = "Action";
	$lang['edit'] = "Edit";
	$lang['delete'] = "Delete";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['delete_group'] = "Delete Evaluator";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
	$lang['new_group'] = "New Group";
	$lang['group_name'] = "Group Name";
	$lang['group_description'] = "Group Description";
	$lang['form_group'] = "Form Group";
	$lang['select_evaluator'] = "Select Evaluator";
?>