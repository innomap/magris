<?php
	
	$lang['form'] = "Form";
	$lang['new_form'] = "Add Form";
	$lang['name'] = "Name";
	$lang['description'] = "Description";
	$lang['question'] = "Question";
	$lang['action'] = "Action";
	$lang['edit'] = "Edit";
	$lang['delete'] = "Delete";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['delete_form'] = "Delete Form";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
?>