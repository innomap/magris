<?php

	$lang['email'] = 'Email';
	$lang['password'] = 'Password';
	$lang['retype_password'] = 'Retype Password';
	$lang['name'] = 'Name';
	$lang['already_have_account'] = 'Already have account ?';
	$lang['register'] = 'Register';
	$lang['registration_form'] = "Registration Form";
	$lang['kp_no'] = "No KP";
	$lang['telp_no'] = "Telephone No";
	$lang['forgot_password'] = "Forgot Password?";
	$lang['reset_password'] = "Reset Password";
	$lang['send_email'] = "Send Email";
	$lang['forgot_password_msg'] = "Please provide the email address that you used when you signed up for your Maratex2016 account. We will send you an email that will allow you to reset your password.";
	$lang['page_not_found_msg'] = "Sorry, this page is no longer available.";
	$lang['save'] = "Save";
	$lang['edit_profile'] = "Edit Profile";
	$lang['registration'] = "Registration";
	$lang['address'] = "Address";
	$lang['postcode'] = "Postcode";
	$lang['subject'] = "Subject";
	$lang['comments'] = "Comments";
	$lang['submit'] = "Submit";
	$lang['cancel'] = "Cancel";
	$lang['created_at'] = "Created At";
	$lang['country'] = "Country";
	$lang['state'] = "State";
	$lang['action'] = "Action";
?>