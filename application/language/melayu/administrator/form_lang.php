<?php
	
	$lang['form'] = "Form";
	$lang['new_form'] = "Tambah Form";
	$lang['name'] = "Nama";
	$lang['description'] = "Deskripsi";
	$lang['question'] = "Pertanyaan";
	$lang['action'] = "Tindakan";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['delete_form'] = "Padam Form";
	$lang['delete_confirm_message'] = "Anda yakin akan memadam ";
?>