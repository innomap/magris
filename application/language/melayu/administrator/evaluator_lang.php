<?php 

	$lang['action'] = "Tindakan";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['delete_evaluator'] = "Padam Evaluator";
	$lang['delete_confirm_message'] = "Anda yakin akan memadam ";
	$lang['email'] = "Emel";
	$lang['name'] = "Nama";
	$lang['new_evaluator'] = "Tambah Evaluator";
	$lang['evaluator'] = "Evaluator";
	$lang['password'] = "Password";
	$lang['retype_password'] = "Ulangi Password";
	$lang['change_password'] = "Ubah Password";
	$lang['group_name'] = "Nama Kumpulan";
?>