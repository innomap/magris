<?php
	$lang['filter'] = "Filter";
	$lang['from'] = "From";
	$lang['to'] = "To";
	$lang['action'] = "Tindakan";
	$lang['mara_center'] = "Pusat Mara";
	$lang['email'] = "Emel";
	$lang['name'] = "Nama";
	$lang['all_mara_center'] = "Semua Pusat Mara";
	$lang['report_user_by_mara_center'] = "Report Submissions by Pusat Mara";
	$lang['submission_number'] = "Submission Number";
	$lang['report_top_60_scores_by_each_award_category'] = "Report Top 60 scores by Award Category";
	$lang['award_category'] = "Award Category";
	$lang['ranking'] = "Ranking";
	$lang['project_name'] = "Project Name";
	$lang['team_leader'] = "Team Leader";
	$lang['average_score'] = "Average Score";
	$lang['group_name'] = "Group Name";
	$lang['report_top_100_scores_by_evaluator_group'] = "Report Top 100 scores by Evaluator Group";
	$lang['scores_by_individual_evaluator'] = "Scores by Individual Evaluator";
?>