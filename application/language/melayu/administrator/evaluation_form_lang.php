<?php 

	$lang['action'] = "Tindakan";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['delete_evaluation_focus'] = "Delete Fokus Evaluasi";
	$lang['delete_confirm_message'] = "Anda yakin akan memadam ";
	$lang['new_evaluation_focus'] = "Tambah Fokus Penilaian";
	$lang['percentage'] = "Peratusan";
	$lang['evaluation_focus'] = "Fokus Penilaian";
	$lang['criteria'] = "Kriteria";
	$lang['question'] = "Pertanyaan";
	$lang['max_point'] = "Markah Maks";
	$lang['name'] = "Nama";
	$lang['new_evaluation_form'] = "Tambah Form Evaluasi";
	$lang['this_data'] = "data ini";
	$lang['evaluation_form'] = "Form Evaluasi";
	$lang['guidelines'] = "Panduan";
?>