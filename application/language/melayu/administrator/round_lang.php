<?php
	
	$lang['round'] = "Round";
	$lang['new_round'] = "Tambah Round";
	$lang['title'] = "Tajuk";
	$lang['deadline'] = "Deadline";
	$lang['action'] = "Tindakan";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['delete_round'] = "Padam Round";
	$lang['delete_confirm_message'] = "Anda yakin akan memadam ";
?>