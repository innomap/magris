<?php

	$lang['email'] = 'Emel';
	$lang['password'] = 'Kata Laluan';
	$lang['retype_password'] = 'Ulangi Kata Laluan';
	$lang['name'] = 'Nama';
	$lang['already_have_account'] = 'Sudah mempunyai Akaun ?';
	$lang['register'] = 'Daftar';
	$lang['registration_form'] = "Pendaftaran Sistem";
	$lang['kp_no'] = "No KP";
	$lang['telp_no'] = "No Telefon";
	$lang['forgot_password'] = "Lupa Kata Laluan?";
	$lang['reset_password'] = "Reset Kata Laluan";
	$lang['send_email'] = "Hantar Emel";
	$lang['forgot_password_msg'] = "Sila berikan alamat e-mel yang anda gunakan semasa anda mendaftar untuk akaun MaGRIs anda. Kami akan menghantar e-mel yang akan membolehkan anda untuk me-reset kata laluan anda.";
	$lang['page_not_found_msg'] = "Maaf, halaman ini tidak lagi boleh diakses.";
	$lang['save'] = "Simpan";
	$lang['edit_profile'] = "Edit Profil";
	$lang['registration'] = "Pendaftaran";
	$lang['address'] = "Alamat";
	$lang['postcode'] = "Poskod";
	$lang['subject'] = "Perkara";
	$lang['comments'] = "Pesanan";
	$lang['submit'] = "Hantar";
	$lang['cancel'] = "Padam";
	$lang['created_at'] = "Created At";
	$lang['country'] = "Negara";
	$lang['state'] = "Negeri";
	$lang['action'] = "Tindakan";
?>