<?php
    $ol_user = $this->user_session->get_user();
?>
<html>
    <head>
        <base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | MaGRIs 2017</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
    </head>
    <body class="default <?= ($this->menu == 'dashboard' || $this->menu == 'account' || $this->menu == 'site' ? 'dashboard-page' : '') ?>">
        <?php if($ol_user){ ?>
            <div class="header">
                <nav class="menu navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <?php if($this->menu != "site" && $this->menu != "account"){ ?>
                        <a class="navbar-brand" href="<?= base_url().'intro' ?>">
                            <img alt="Brand" src="<?= base_url().ASSETS_IMG.'logo_magris.png' ?>">
                        </a>
                        <?php } ?>
                    </div>
                    <div id="navbar-header" class="collapse navbar-collapse">
                        <?php if($this->menu != "site" && $this->menu != "account"){ ?>
                            <ul class="nav navbar-nav">
                                <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().'applications' ?>">Penyertaan</a></li>
                                <li class="<?= $this->menu == "profile" ? "active" : "" ?>"><a href="<?= base_url().'profile' ?>">Edit Profil</a></li>
                                <li class="<?= $this->menu == "term" ? "active" : "" ?>"><a href="<?= base_url().'term' ?>" target="_blank">Terma & Syarat</a></li>
                                <li class="<?= $this->menu == "faq" ? "active" : "" ?>"><a href="<?= base_url().'faq' ?>" target="_blank">Soalan Lazim</a></li>
                            </ul>
                        
                            <ul class="nav navbar-nav navbar-right logout-btn">
                                <li>
                                    <a href="<?= base_url().'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Logout</a>
                                </li>
                            </ul> 
                        <?php } ?>
                    </div>
                </nav>
            </div>
        <?php } ?>
        
        <div class="col-xs-12 col-sm-12 col-md-12 content">
            {{content}}
        </div>
    
        <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
        </script>
         <!-- jQuery -->
        <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
        {{scripts}}
    </body>
</html>