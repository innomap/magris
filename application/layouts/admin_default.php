<?php
    $ol_user = $this->user_session->get_admin();
?>
<html>
    <head>
        <base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | MaGRIs 2017</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
    </head>
    <body class="default <?= (!$ol_user ? 'landing-page' : '') ?>">
        <?php if($ol_user){ ?>
            <div class="header">
                <nav class="menu navbar navbar-default">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                         <a class="navbar-brand" href="<?= base_url().'intro' ?>">
                            <img alt="Brand" src="<?= base_url().ASSETS_IMG.'logo_magris.png' ?>">
                        </a>
                    </div>
                    <div id="navbar-header" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="<?= $this->menu == "dashboard" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'dashboard' ?>">Dashboard</a></li>
                            <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'applications' ?>">Submissions</a></li>
                            <li class="<?= $this->menu == "innovator" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'innovators' ?>">Innovator</a></li>
                            <li class="<?= $this->menu == "evaluator" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'evaluators' ?>">Manage Evaluator</a></li>
                            <li class="<?= $this->menu == "expert_group" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'expert_groups' ?>">Manage Group</a></li>
                            <li class="<?= $this->menu == "evaluation_form" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'evaluation_form' ?>">Manage Evaluation Form</a></li>
                            <li class="<?= $this->menu == "round" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'rounds' ?>">Manage Round</a></li>
                            <li class="dropdown <?= $this->menu == "report" ? "active" : "" ?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Report <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/registration_number_by_date' ?>">Registration number by date</a></li>
                                    <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/submission_number_by_date' ?>">Submission number by date</a></li>
                                    <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/application_number_by_category' ?>">Penyertaan by Category</a></li>
                                    <li><a href="<?= base_url().PATH_TO_ADMIN.'reports/top_score_by_evaluator_group' ?>">Top 100 Scores by Evaluator Group</a></li>
                                </ul>
                            </li>
                            <li class="<?= $this->menu == "contact" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_ADMIN.'contacts' ?>">Contact Us</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right logout-btn">
                            <li><a href="<?= base_url().PATH_TO_ADMIN.'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Logout</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        <?php } ?>
        
        {{content}}
        <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
            var adminUrl = "<?=base_url().PATH_TO_ADMIN?>";
        </script>
         <!-- jQuery -->
        <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
        {{scripts}}
    </body>
</html>