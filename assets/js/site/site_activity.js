$(function(){
   initInstagramApi();
   initInstagramApiLoadMore();
   initImgFancyBox();
});

function initInstagramApi(){
	/*var USERID = "31785838",
		ACCESS_TOKEN = "31785838.ab103e5.7e9679f5be144be2a301cbfe7ab2d19c";*/
	var USERID = "",
		ACCESS_TOKEN = "";
	var url = "https://api.instagram.com/v1/users/"+USERID+"/media/recent/?access_token="+ACCESS_TOKEN;
	callInstagramApi(url);
}

function initInstagramApiLoadMore(){
	$('.activity-pagination-wrap').on('click','.instagram-load-more', function(){
		var url = $(this).data('url');
		callInstagramApi(url);
	});
}

function callInstagramApi(url){
	$.get(url, "", function(response){
        for(var i=0; i < response.data.length; i++){
            var post = response.data[i];
            var content = '<div class="col-md-3">';
            	content += 		'<div class="col-md-12">';
            	content += 			'<a class="group" rel="group1" href="'+post.images.standard_resolution.url+'" title="'+(post.caption != null ? post.caption.text : '')+'"><img src="'+post.images.standard_resolution.url+'" width="100%"></a>';
            	content +=		'</div>';
            	content +=	'</div>';
            $('.activity-img-wrap').append(content);
        }
        
        if(typeof response.pagination.next_url == 'undefined'){
        	$('.activity-pagination-wrap').html('');
        }else{
        	$('.activity-pagination-wrap').html('<a class="btn btn-primary rounded instagram-load-more" data-url="'+response.pagination.next_url+'">Load More</a>');
        }
    },'jsonp');
}

function initImgFancyBox(){
	$("a.group").fancybox({
		'nextEffect'	:	'fade',
		'prevEffect'	:	'fade',
		helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        },
		'arrows' : true,
	});
}