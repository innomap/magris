var formRegister = $('#form-register'),
	formLogin = $('#form-login'),
	formForgotPassword = $("#form-forgot-password"),
	formCreatePassword = $("#form-create-password");

$(function(){
	initAlert();

	initValidator();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}
function initValidator(){
	//Registration Form
	formRegister.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
					remote: {
                        message: 'The email is not available',
                        url: baseUrl+'accounts/check_email_exist/',
                        data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val()
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
			name: {
				validators: {
					notEmpty: {message: 'The Name is required'}
				}
			},
			kp_no_1: {
				validators: {
					notEmpty: {message: 'The KP No is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 6,
                        min:6,
                        message: 'The field must be 6 characters'
                    },
                    callback: {
                        message: 'Age cannot be less than 18',
                        callback: function (value, validator, $field) {
                            var result = false;
                            if(value.length > 2){
                            	var year = value.substring(0,2);
                            	year = parseInt(year);
                            	result = (year < 99);
                            }
                            return result;
                        }
                    }
				}
			},
			kp_no_2: {
				validators: {
					notEmpty: {message: 'The KP No is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 2,
                        min:2,
                        message: 'The field must be 2 characters'
                    }
				}
			},
			kp_no_3: {
				validators: {
					notEmpty: {message: 'The KP No is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 4,
                        min:4,
                        message: 'The field must be 4 characters'
                    }
				}
			},
			telp_no: {
				validators: {
					notEmpty: {message: 'The Telp No is required'},
					numeric: {message: 'The field must be a number'},
				}
			},
			address: {
				validators: {
					notEmpty: {message: 'The Address is required'}
				}
			},
			postcode: {
				validators: {
					notEmpty: {message: 'The Postcode is required'}
				}
			},
		}
	}).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();

        var $form        = $(e.target),
            validator    = $form.data('bootstrapValidator'),
            submitButton = validator.getSubmitButton();

        if(submitButton.context.name == "btn_register"){
        	 $form.ajaxSubmit({
                // You can change the url option to desired target
                url: $form.attr('action'),
                dataType: 'json',
                success: function(responseText, statusText, xhr, $form) {
                    bootbox.alert(responseText.message, function() {
                    	if(responseText.status == 1){
                    		window.location.href = baseUrl+'registration_success';
                    	}else{
                    		window.location.href = baseUrl;		
                    	}
		        	});
                }
            });
        }
    });
	
	//Form Login
	formLogin.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
		}
	});
	//Form Forgot Password
	formForgotPassword.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
				}
			}
		}
	});

	//Create Password Form
	formCreatePassword.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
		}
	});
}