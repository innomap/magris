var formProfile = $('#form-profile');

$(function(){
	initAlert();
	initEditPassword();
	initValidator();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initEditPassword(){
	formProfile.on('click','.btn-change-pass',function(e){
		e.preventDefault();
	    var elem = $(this);
	    var collapse = elem.parent().parent().parent().find('.collapse');
	    collapse.collapse('toggle');
	});
}

function initValidator(){
	formProfile.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
					remote: {
                        message: 'The email is not available',
                        url: baseUrl+'accounts/check_email_exist/',
                        data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val(),
                                id : userId
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
			name: {
				validators: {
					notEmpty: {message: 'The Name is required'}
				}
			},
			kp_no_1: {
				validators: {
					notEmpty: {message: 'The KP No is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 6,
                        min:6,
                        message: 'The field must be 6 characters'
                    }
				}
			},
			kp_no_2: {
				validators: {
					notEmpty: {message: 'The KP No is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 2,
                        min:2,
                        message: 'The field must be 2 characters'
                    }
				}
			},
			kp_no_3: {
				validators: {
					notEmpty: {message: 'The KP No is required'},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 4,
                        min:4,
                        message: 'The field must be 4 characters'
                    }
				}
			},
			telp_no: {
				validators: {
					notEmpty: {message: 'The Telp No is required'},
					numeric: {message: 'The field must be a number'},
				}
			},
			address: {
				validators: {
					notEmpty: {message: 'The Address is required'}
				}
			},
			postcode: {
				validators: {
					notEmpty: {message: 'The Postcode is required'}
				}
			},
		}
	});
}
