var tableApplication = $('#table-application'),
	formApplication = $('#form-application');

$(function(){
	tableApplication.dataTable();
	initImgFancybox();

	initAlert();
	initValidator();
	initAddTeamMember();
	initDeleteTeamMember();
	initAddPicture();
	initDeletePicture();
	initAddLink();
	initDeleteLink();

	initEdit();
	initView();
	initDelete();
	initDownloadPdf();

	initPopover();
	initJobType();
	collapseJobType(formApplication.find('input[name=job_type]:checked').val());
	initMyipoProtection();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}

	if(alert_dialog != ''){
		bootbox.alert({
		   message: lang_application_submitted_msg,
		   callback: function(){
		      window.location = baseUrl+"applications/generate_pdf/"+alert_dialog;
		   }
		});
	}
}

function initValidator(){
	formApplication.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		live:'disabled',
		fields: {
			title: {
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			description: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			how_to_use: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			myipo_protection: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			picture_0: {
				validators: {
					callback: {
	                    message: 'The field is required',
	                    callback: function (value, validator, $field) {
	                        var result = false;
	                        var imgLength = formApplication.find('.img-app').size();
	                        
	                        if(imgLength > 0){
	                        	result = true;
	                        }else{
	                        	if(value != ""){
	                        		result = true;
	                        	}	
	                        }
	                        return result;
	                    }
	                }
				},
				
			},
			impact: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			problem_encountered: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			innovation_novelty: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			agreement: {
				validators: {
					callback: {
	                    message: 'The field is required',
	                    callback: function (value, validator, $field) {
	                        var result = false;
	                        var getVal = formApplication.find('.agreement:checked').val();
	                        
	                        if(getVal == 1){
	                        	result = true;
	                        }
	                        return result;
	                    }
	                }
				},
				
			},
		}
	}).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();

        var $form        = $(e.target),
            validator    = $form.data('bootstrapValidator'),
            submitButton = validator.getSubmitButton();

        if(submitButton.context.name == "btn_send_for_approval"){
        	showSubmissionAlert();
        }else{
        	formApplication.bootstrapValidator('defaultSubmit');
        }

    });

    $('#btn-save').on('click', function(){
		formApplication.bootstrapValidator('enableFieldValidators', 'title', false);
		formApplication.bootstrapValidator('enableFieldValidators', 'description', false);
		formApplication.bootstrapValidator('enableFieldValidators', 'how_to_use', false);
		formApplication.bootstrapValidator('enableFieldValidators', 'myipo_protection', false);
		formApplication.bootstrapValidator('enableFieldValidators', 'picture_0', false);
		formApplication.bootstrapValidator('enableFieldValidators', 'impact', false);
	});
}

function showSubmissionAlert(){
	bootbox.dialog({
		message: lang_send_for_approval_confirm_message+" ?",
		title: lang_send_innovation_for_approval,
		onEscape: function(){},
		size: "large",
		buttons: {
			close: {
				label: lang_cancel,
				className: "btn-default flat",
				callback: function() {
					$(this).modal('hide');
					formApplication.find('input[type=submit]').prop('disabled', false);
				}
			},
			danger: {
				label: lang_send_for_approval,
				className: "btn-success flat",
				callback: function() {
					formApplication.bootstrapValidator('defaultSubmit');				}
			}
		}
	});
}

function initEdit(){
	tableApplication.on('click','.btn-edit-application', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'applications/edit/'+currentId;
	});
}

function initDelete(){
	tableApplication.on('click','.btn-delete-application', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_application,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = baseUrl+'applications/delete/'+currentId;
					}
				}
			}
		});
	});
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initView(){
	tableApplication.on('click','.btn-view-application', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'applications/view/'+currentId;
	});

	if(typeof view_mode != 'undefined'){
		if(view_mode){
			disabledForm();
		}
	}
}

function disabledForm(){
	formApplication.find("input[type=text]").prop('readonly', true);
	formApplication.find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
	formApplication.find("button, input[type=file], input[type=submit]").hide();
	$('.btn-download-wrap').hide();
	formApplication.find('.popover-item').hide();
	formApplication.find("button.btn-back").show();
}

function initAddTeamMember(){
	var item_num = formApplication.find('.team-member-id').length;

	formApplication.on('click','.add-team-member', function(){
		if(item_num < 4){
			str =	'<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">';
			str += '<div class="col-md-11 team-member-item">';
			str +=		'<input type="hidden" name="h_team_member_pic_'+item_num+'" value="">';
			str +=		'<input type="hidden" name="i_deleted_team_member_'+item_num+'">';
			str += 		'<div class="col-md-6">';
			str += 			'<input type="text" class="form-control" name="team_member_'+item_num+'" placeholder="Nama Ahli">';
			str +=		'</div>';
			str += 		'<div class="col-md-3">';
			str += 			'<input type="text" class="form-control" name="team_member_kp_'+item_num+'" placeholder="No KP">';
			str +=		'</div>';
			str += 		'<div class="col-md-3">';
			str += 			'<input type="text" class="form-control" name="team_member_telp_'+item_num+'" placeholder="No Telefon">';
			str +=		'</div>';
			str += '</div>';
			str += '<div class="col-md-1">';
			str +=		'<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
			str += '</div>';
			$(".team-member-wrap").append(str);
			item_num++;
		}
	});
}

function initDeleteTeamMember(){
	formApplication.on('click','.delete-team-member', function(){
		$(this).parent().prev('div').remove();
		$(this).parent().remove();
	});
}

function initDownloadPdf(){
	tableApplication.on('click','.btn-download-pdf', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'innovations/generate_pdf/'+currentId;
	});
}

function initPopover(){
	$('.popover-item').popover({
		trigger:'hover',
		container:'body',
		placement: 'bottom'
	});
}

function initAddPicture(){
	$('.add-picture').on('click', function(){
		var num = $(".picture-item").length;
		if(num < 4){
			str = '<div class="col-md-11">'
			str += 		'<input type="file" class="form-control picture-item" name="picture_'+num+'">';
			str += '</div>';
			$(".picture-wrapper div.item").append(str);
		}
	});
}

function initDeletePicture(){
	$('.delete-picture').on('click', function(){
		var currentId = $(this).data('id'),
			currentName = $(this).data('name');

		formApplication.append('<input type="hidden" name="deleted_picture[]" value="'+currentId+'">');
		formApplication.append('<input type="hidden" name="deleted_picture_name[]" value="'+currentName+'">');
		$(this).parent().remove();
		formApplication.bootstrapValidator('revalidateField', 'picture_0');
	});
}

function initAddLink(){
	var item_num = formApplication.find('.link-id').length;

	formApplication.on('click','.add-link', function(){
		str =	'<input type="hidden" class="link-id" name="link_id[]" value="0">';
		str += '<div class="col-md-11 link-item">';
		str +=		'<input type="hidden" name="i_deleted_link_'+item_num+'">';
		str += 		'<input type="text" class="form-control" name="link_'+item_num+'" placeholder="http://">';
		str += '</div>';
		str += '<div class="col-md-1">';
		str +=		'<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
		str += '</div>';
		$(".link-wrap").append(str);
	});
}

function initDeleteLink(){
	formApplication.on('click','.delete-link', function(){
		$(this).parent().prev('div').remove();
		$(this).parent().remove();
	});
}


function initJobType(){
	formApplication.on('click','input[name=job_type]',function(e){
	    var elem = $(this);
	    var value = elem.val();
	    collapseJobType(value);
	});
}

function collapseJobType(value){
	if(value == 2){
    	formApplication.find('textarea[name=job_other]').show();
    }else{
    	formApplication.find('textarea[name=job_other]').hide();
    }
}

function initMyipoProtection(){
	toogleMyipoProtection();
	formApplication.on('change','input[name=myipo_protection]', function(){
		toogleMyipoProtection();
	});
}

function toogleMyipoProtection(){
	var myipo = $('input[name=myipo_protection]:checked').val();
	if(myipo == 1){
		$('textarea[name=myipo_protection_desc]').show();
	}else{
		$('textarea[name=myipo_protection_desc]').hide();
	}
}