var formContactUs = $('#form-contact-us');

$(function(){
	initAlert();

	initValidator();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}

	if(alert_contact != ''){
		formContactUs.parent().find('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}
function initValidator(){	
	formContactUs.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			email: {
				validators: {
					notEmpty: {message: 'The field is required'},
					emailAddress: {message: 'The value is not a valid email address'},
				}
			},
			subject: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			comments: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
		}
	});
}