var tableGroup = $('#table-group'),
	formGroup = $('#form-group'),
	modalGroup = $('#modal-group');

$(function(){
	tableGroup.dataTable();

	initAlert();
	initValidator();
	initEdit();
	initDelete();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initValidator(){
	formGroup.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name:{
				validators: {
					notEmpty: {message: 'The Name is required'},
				}
			},
			description:{
				validators: {
					notEmpty: {message: 'The Description is required'},
				}
			},
			'evaluator_ids[]': {
				validators: {
					choice: {
                        min: 1,
                        message: 'Please select at least 1 evaluator'
                    }
				}
			},
		}
	});
}

function initEdit(){
	tableGroup.on('click','.btn-edit-group', function(e){
		formGroup.prop('action', adminUrl+'expert_groups/update');
		var currentId = $(this).data('id');
		$.get(adminUrl+'expert_groups/edit/'+currentId, function(res){
			if(res.status == 1){
				var data = res.data;
				formGroup.find('input[name=id]').val(currentId);
				formGroup.find('input[name=name]').val(data.name);
				formGroup.find('textarea[name=description]').val(data.description);
				modalGroup.modal();
			}
		}, 'json');
	});
	modalGroup.on('hidden.bs.modal', function(){
		formGroup.attr('action', adminUrl+'expert_groups/store')
					.find('input[name=id]').val('');
		formGroup.bootstrapValidator('resetForm', true);
		formGroup.get(0).reset();
	});
}

function initDelete(){
	tableGroup.on('click','.btn-delete-group', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_group,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'expert_groups/delete/'+currentId;
					}
				}
			}
		});
	});
}