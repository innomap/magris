var tableRound = $('#table-round'),
	formRound = $('#form-round'),
	modalRound = $("#modal-round");

$(function(){
	tableRound.dataTable();

	initAlert();
	initValidator();
	initDatepicker();
	initEdit();
	initDelete();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initDatepicker(){
	$('.datepicker').datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss',
	});
}

function initValidator(){
	formRound.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			title: {
				validators: {
					notEmpty: {message: 'The Title is required'},
				}
			},
			deadline: {
				validators: {
					notEmpty: {message: 'The Deadline is required'}
				}
			},
		}
	});
}

function initEdit(){
	$('.btn-edit-round').on('click', function(e){
		formRound.prop('action', adminUrl+'rounds/update');
		var currentId = $(this).data('id');
		$.get(adminUrl+'rounds/edit/'+currentId, function(res){
			if(res.status == 1){
				var data = res.data;
				formRound.find('input[name=id]').val(currentId);
				formRound.find('input[name=title]').val(data.title);
				formRound.find('input[name=deadline]').val(data.deadline);
				formRound.find('select[name=form_id]').val(data.mara_form_id);
				modalRound.modal();
			}
		}, 'json');
	});
	modalRound.on('hidden.bs.modal', function(){
		formRound.attr('action', adminUrl+'rounds/store')
					.find('input[name=id]').val('');
		formRound.bootstrapValidator('resetForm', true);
		formRound.get(0).reset();
	});
}

function initDelete(){
	tableRound.on('click','.btn-delete-round', function(e){
		currentId = $(this).data('id');
		var title = $(this).data('title');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+title+"</strong> ?",
			title: lang_delete_round,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'rounds/delete/'+currentId;
					}
				}
			}
		});
	});
}