var tableInnovator = $('#table-innovator');

$(function(){
	tableInnovator.dataTable();
	initAlert();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}