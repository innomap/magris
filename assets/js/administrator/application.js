var tableApplication = $('#table-application'),
	formApplication = $('#form-application'),
	formApplicationList = $('#form-application-list'),
	formEvaluation = $('#form-evaluation'),
	modalAssign = $('#modal-assign'),
	formAssign = $('#form-assign'),
	tableEvaluationList = $('#table-evaluation-list'),
	formEvaluationList = $('#form-evaluation-list');

$(function(){
	initDataTableAndFilter();
	initAlert();
	initImgFancybox();

	initViewEvaluation();
	initViewEvaluationList();
	initViewApplication();
	initAssignApplication();
	initDownloadPdf();
	initApprove();
	initEdit();
	initDelete();

	initAddTeamMember();
	initDeleteTeamMember();
	initAddPicture();
	initDeletePicture();
	initAddLink();
	initDeleteLink();
	initValidator();

	initJobType();
	collapseJobType(formApplication.find('input[name=job_type]:checked').val());
	initPrintPdf();
});

function initDataTableAndFilter(){
	var dt = tableApplication.dataTable({
		"order": [[ 1, "asc" ]]
	});
	$('#dt-filter-status').on('change', function(){
		var opt = $(this).find("option:checked").text();
		if(opt == "All"){
			dt.fnFilterClear();
		}else{
			dt.fnFilter(opt, 8, true, false );
		}
	});

	$('#dt-filter-state').on('change', function(){
		var opt = $(this).find("option:checked").text();
		if(opt == "All"){
			dt.fnFilterClear();
		}else{
			dt.fnFilter(opt, 6, true, false );
		}
	});

	$('#dt-filter-year').on('change', function(){
		var opt = $(this).find("option:checked").text();
		if(opt == "All"){
			dt.fnFilterClear();
		}else{
			dt.fnFilter(opt, 7, true, false );
		}
	});
}

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initViewEvaluation(){
	tableEvaluationList.on('click','.btn-view-evaluation', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'applications/view_evaluation/'+currentId;
	});
	
	if(typeof view_mode != 'undefined'){
		if(view_mode == 1){
			calculateTotal();
			formEvaluation.find("input[type=text]").prop('readonly', true);
			formEvaluation.find("textarea").prop('disabled', true);
			formEvaluation.find("button,input[type=submit]").hide();
			formEvaluation.find("button.btn-cancel").show();
			formEvaluation.find(".popover-item").hide();
			formEvaluation.find(".point-input").prop('disabled',true);

			formEvaluation.on('click','.btn-cancel', function(e){
				window.history.back();
			});
		}
	}
}

function initViewEvaluationList(){
	tableApplication.on('click','.btn-view-evaluation-list', function(e){
		currentId = $(this).data('id');
		window.open(adminUrl+'applications/view_evaluation_list/'+currentId,'_blank');
	});

	formEvaluationList.on('click','.btn-cancel', function(){
		window.location.href = adminUrl+'applications';
	});
}

function initViewApplication(){
	tableApplication.on('click','.btn-view-application', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'applications/view_application/'+currentId;
	});

	if(typeof view_mode != 'undefined'){
		if(view_mode){
			formApplication.find("input[type=text]").prop('readonly', true);
			formApplication.find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
			formApplication.find("button, input[type=file], input[type=submit]").hide();
			$('.btn-download-wrap').hide();
			formApplication.find('.popover-item').hide();
			formApplication.find("button.btn-back").show();
		}
	}
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initAssignApplication(){	
	tableApplication.on('click','.btn-assign-application', function(){
		var currentId = $(this).data('id');
		modalAssign.find('input[name=application_id]').val(currentId);
		modalAssign.modal();

		modalAssign.on('hidden.bs.modal', function(){
			formAssign.bootstrapValidator('resetForm', true);
			formAssign.get(0).reset();
		});
	});
}

function check_value(input){
	if(isNaN(parseInt(input))){
		return 0;
	}else{
		return parseInt(input);
	}
}

function calculateTotal(){
	var sumTotal = 0;
	$('.focus-item').each(function(i){
		var percentage = $(this).data('percentage'),
			id = $(this).data('id'),
			total = 0, maxPoint = 0;

		$('.input-'+id+':checked').each(function(j){
			maxPoint += check_value($(this).data('maxpoint'));
		});

		$('.input-'+id+':checked').each(function(j){
			var point = check_value($(this).val());
			total += point;
		});

		var calTotal = total/maxPoint*percentage;
		$('#total-'+id).val(calTotal.toFixed(1));
		sumTotal += calTotal;
	});

	$('#total').val(sumTotal.toFixed(1));
	$('input[name=sum_total]').val(sumTotal.toFixed(1));
}

function initDownloadPdf(){
	tableApplication.on('click','.btn-download-pdf', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'applications/generate_pdf/'+currentId;
	});
}

function initApprove(){
	formApplicationList.on('submit', function(e){
		var ids = formApplicationList.find('input[type=checkbox]:checked').length,
			content = formApplicationList.find('input[type=checkbox]:checked'),
			currentForm = this;

		var btnSubmit = $("input[type=submit][clicked=true]").data('id');

		if(ids == 0){
			e.preventDefault();
			bootbox.alert('Please select at least an item.');
			return false;
		}

		var evaluated = 1;
		$(content).each(function(i){
			if($(this).data('is-evaluated') == 0){
				evaluated = 0;
			}
		});

		if(btnSubmit == "btn-proceed"){
			$('input[name=action]').val('proceed');
			
			if(evaluated == 0){
				e.preventDefault();
				bootbox.alert('Application not evaluated yet.');
				return false;
			}else{
				e.preventDefault();
				bootbox.dialog({
					message: "Are you sure want to proceed to next round?",
					title: "Proceed to Next",
					onEscape: function(){},
					size: "small",
					buttons: {
						close: {
							label: "Cancel",
							className: "btn-default flat",
							callback: function() {
								$(this).modal('hide');
							}
						},
						danger: {
							label: "Proceed",
							className: "btn-success flat",
							callback: function() {
								currentForm.submit();
							}
						}
					}
				});

				return false;
			}
		}else if(btnSubmit == "btn-assign"){
		}
	});

	formApplicationList.find("input[type=submit]").click(function() {
	    $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
	    $(this).attr("clicked", "true");
	});
}

function initEdit(){
	tableApplication.on('click','.btn-edit-application', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'applications/edit/'+currentId;
	});
}

function initAddTeamMember(){
	var item_num = formApplication.find('.team-member-id').length;

	formApplication.on('click','.add-team-member', function(){
		if(item_num < 4){
			str =	'<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">';
			str += '<div class="col-md-11 team-member-item">';
			str +=		'<input type="hidden" name="h_team_member_pic_'+item_num+'" value="">';
			str +=		'<input type="hidden" name="i_deleted_team_member_'+item_num+'">';
			str += 		'<div class="col-md-6">';
			str += 			'<input type="text" class="form-control" name="team_member_'+item_num+'" placeholder="Nama Ahli">';
			str +=		'</div>';
			str += 		'<div class="col-md-3">';
			str += 			'<input type="text" class="form-control" name="team_member_kp_'+item_num+'" placeholder="No KP">';
			str +=		'</div>';
			str += 		'<div class="col-md-3">';
			str += 			'<input type="text" class="form-control" name="team_member_telp_'+item_num+'" placeholder="No Telefon">';
			str +=		'</div>';
			str += '</div>';
			str += '<div class="col-md-1">';
			str +=		'<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
			str += '</div>';
			$(".team-member-wrap").append(str);
			item_num++;
		}
	});
}

function initDeleteTeamMember(){
	formApplication.on('click','.delete-team-member', function(){
		$(this).parent().prev('div').remove();
		$(this).parent().remove();
	});
}

function initAddPicture(){
	$('.add-picture').on('click', function(){
		var num = $(".picture-item").length;
		if(num < 4){
			str = '<div class="col-md-11">'
			str += 		'<input type="file" class="form-control picture-item" name="picture_'+num+'">';
			str += '</div>';
			$(".picture-wrapper div.item").append(str);
		}
	});
}

function initDeletePicture(){
	$('.delete-picture').on('click', function(){
		var currentId = $(this).data('id'),
			currentName = $(this).data('name');

		formApplication.append('<input type="hidden" name="deleted_picture[]" value="'+currentId+'">');
		formApplication.append('<input type="hidden" name="deleted_picture_name[]" value="'+currentName+'">');
		$(this).parent().remove();
		formApplication.bootstrapValidator('revalidateField', 'picture_0');
	});
}

function initAddLink(){
	var item_num = formApplication.find('.link-id').length;

	formApplication.on('click','.add-link', function(){
		str =	'<input type="hidden" class="link-id" name="link_id[]" value="0">';
		str += '<div class="col-md-11 link-item">';
		str +=		'<input type="hidden" name="i_deleted_link_'+item_num+'">';
		str += 		'<input type="text" class="form-control" name="link_'+item_num+'" placeholder="http://">';
		str += '</div>';
		str += '<div class="col-md-1">';
		str +=		'<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
		str += '</div>';
		$(".link-wrap").append(str);
	});
}

function initDeleteLink(){
	formApplication.on('click','.delete-link', function(){
		$(this).parent().prev('div').remove();
		$(this).parent().remove();
	});
}

function initValidator(){
	formApplication.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		live:'disabled',
		fields: {
			title: {
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			description: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			how_to_use: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			myipo_protection: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			picture_0: {
				validators: {
					callback: {
	                    message: 'The field is required',
	                    callback: function (value, validator, $field) {
	                        var result = false;
	                        var imgLength = formApplication.find('.img-app').size();
	                        
	                        if(imgLength > 0){
	                        	result = true;
	                        }else{
	                        	if(value != ""){
	                        		result = true;
	                        	}	
	                        }
	                        return result;
	                    }
	                }
				},
				
			},
			impact: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
		}
	}).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();

        var $form        = $(e.target),
            validator    = $form.data('bootstrapValidator'),
            submitButton = validator.getSubmitButton();

        if(submitButton.context.name == "btn_send_for_approval"){
        	showSubmissionAlert();
        }else{
        	formApplication.bootstrapValidator('defaultSubmit');
        }

    });
}

function initDelete(){
	tableApplication.on('click','.btn-delete-application', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_application,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'applications/delete/'+currentId;
					}
				}
			}
		});
	});
}

function initJobType(){
	formApplication.on('click','input[name=job_type]',function(e){
	    var elem = $(this);
	    var value = elem.val();
	    collapseJobType(value);
	});
}

function collapseJobType(value){
	if(value == 2){
    	formApplication.find('textarea[name=job_other]').show();
    }else{
    	formApplication.find('textarea[name=job_other]').hide();
    }
}

function initPrintPdf(){
    $('.btn-print-pdf').on('click', function(){
        var id = $(this).data('id') + "";
        $.post(adminUrl+'applications/get_print_pdf_url/',{id: id}, function(response){
            bootbox.dialog({closeButton: false,message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>' });
            print(response.url);
        },'json');
    });
}

function print(url){
    bootbox.hideAll();
    var _this = this,
      iframeId = 'iframeprint',
      $iframe = $('iframe#iframeprint');
    $iframe.attr('src', url);

    $iframe.load(function() {
      callPrint(iframeId);
    });
}

function callPrint(iframeId) {
    var PDF = document.getElementById(iframeId);
    PDF.focus();
    PDF.contentWindow.print();
}