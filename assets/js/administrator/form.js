var tableForm = $('#table-form'),
	formForm = $('#form-form'),
	modalForm = $("#modal-form");

$(function(){
	tableForm.dataTable();

	initAlert();
	initValidator();
	initEdit();
	initDelete();
	initModalForm();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initValidator(){
	formForm.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name: {
				validators: {
					notEmpty: {message: 'The Name is required'},
				}
			},
			description: {
				validators: {
					notEmpty: {message: 'The Description is required'}
				}
			},
			"questions[]":{
				validators:{
					notEmpty: {message: 'The Question is required'}
				}
			}
		}
	});
}

function initEdit(){
	$('.btn-edit-form').on('click', function(e){
		formForm.prop('action', adminUrl+'forms/update');
		var currentId = $(this).data('id');
		$.get(adminUrl+'forms/edit/'+currentId, function(res){
			if(res.status == 1){
				var data = res.data;
				formForm.find('input[name=id]').val(currentId);
				formForm.find('input[name=name]').val(data.name);
				formForm.find('textarea[name=description]').val(data.description);

				var content = "";
				for(var i=0; i < data.questions.length; i++){
					content += '<div class="col-md-12 no-padding question-item">';
					content += 		'<div class="col-md-11 no-padding">';
					content += 			'<input type="text" name="questions[]" class="form-control" placeholder="'+lang_question+'" value="'+data.questions[i].question+'" />';
					content +=		'</div>';
					content +=		'<div class="col-md-1">';
					if(i == 0){
						content +=		'<button type="button" class="btn btn-grey add-question"><span class="fa fa-plus"></span></button>';
					}else{
						content +=		'<button type="button" class="btn btn-danger delete-question"><span class="fa fa-times"></span></button>';
					}
					content += 		'</div>';
					content +=	'</div>';
				}

				modalForm.find('.question-wrap').html(content);
				modalForm.modal();
			}
		}, 'json');
	});
	modalForm.on('hidden.bs.modal', function(){
		formForm.attr('action', adminUrl+'forms/store')
					.find('input[name=id]').val('');
		formForm.bootstrapValidator('resetForm', true);
		formForm.get(0).reset();
		formForm.find('input, textarea').val('');
		formForm.find('.question-item').not(':first').remove();
	});
}

function initDelete(){
	tableForm.on('click','.btn-delete-form', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_form,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'forms/delete/'+currentId;
					}
				}
			}
		});
	});
}

function initModalForm(){
	modalForm.on('click','.add-question', function(){
		var content = "";

		content += '<div class="col-md-12 no-padding question-item">';
		content += 		'<div class="col-md-11 no-padding">';
		content += 			'<input type="text" name="questions[]" class="form-control" placeholder="'+lang_question+'" />';
		content +=		'</div>';
		content +=		'<div class="col-md-1">';
		content +=			'<button type="button" class="btn btn-danger delete-question"><span class="fa fa-times"></span></button>';
		content += 		'</div>';
		content +=	'</div>';

		modalForm.find('.question-wrap').append(content);
	});

	modalForm.on('click', '.delete-question', function(){
		$(this).parent().parent().remove();
	});
}