$(function(){
	initGraphNumberByDay();
});

function initGraphNumberByDay(){
	var container = "#container-num-by-day",
		title = "Application number by day",
		yAxis = "Number of Application";

	$.get(adminUrl+'dashboard/report_number_by_day_handler', "", function(response){
		drawLineChart(container, title, yAxis, response.xAxis, response.series);
	},'json');
}

function drawLineChart(container, title, yAxis, xAxis, series){
	$(container).highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: title
        },
        xAxis: {
            categories: xAxis
        },
        yAxis: {
            title: {
                text: yAxis
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: series,
        credits: {
        	enabled : false
        }
    });
}

function drawPieChart(container, title, seriesName, data){
	$(container).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: seriesName,
            colorByPoint: true,
            data: data,
        }],
        credits : {
        	enabled : false
        }
    });
}