var modalDetail = $('#modal-detail'),
    tableDetail = $('#table-detail'),
    tableList = $('#table-list');

$(function(){
    initDatepicker();
    tableList.dataTable();

    var report_type = $('.report_type').data('content');
    if(report_type == "registration-num-by-date"){
        initGraphRegistrationNumByDate();
    }else if(report_type == "submission-num-by-date"){
        initGraphSubmissionNumByDate();
    }else if(report_type == "application-num-by-category"){
        initGraphApplicationNumByCat();
    }

    $('#submit-registration-num').on('click', function(){
        initGraphRegistrationNumByDate();
    });

    $('#submit-submission-num').on('click', function(){
        initGraphSubmissionNumByDate();
    });

    $('#submit-application-num-by-cat').on('click', function(){
        initGraphApplicationNumByCat();
    });
});

function initDatepicker(){
    $('.from-datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoClose : true,
    }).on('changeDate', function(ev){        
        $('.from-datepicker').datepicker('hide');
        var startDate = new Date(ev.date.getTime() + 24*60*60*1000);
        var endDate = new Date(ev.date.getTime() + 30*24*60*60*1000);
        $('.to-datepicker').datepicker('setStartDate', startDate);
        $('.to-datepicker').datepicker('setEndDate', endDate);
        $('.to-datepicker').datepicker('update', startDate);
    });

    var date = new Date($('.from-datepicker').val());
    $('.to-datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoClose : true,
        startDate : new Date(date.getTime() + 24*60*60*1000),
        endDate : new Date(date.getTime() + 30*24*60*60*1000),
    }).on('changeDate', function(ev){                 
        $('.to-datepicker').datepicker('hide');
    });
}

function initGraphRegistrationNumByDate(){
	var container = "#container-registration-num-by-date",
		title = "Registration Number by Date",
		yAxis = "Number of User",
        form = $('#form-registration-num'),
        type = 1,
        data = {date_from: form.find('input[name="date_from"]').val(), date_to: form.find('input[name="date_to"]').val()};

	$.post(adminUrl+'reports/registration_number_by_date_handler', data, function(response){
		drawLineChart(container, title, yAxis, response.xAxis, response.series, type);
	},'json');
}

function initGraphSubmissionNumByDate(){
    var container = "#container-submission-num-by-date",
        title = "Submission Number by Date",
        yAxis = "Number of Submission",
        form = $('#form-submission-num'),
        type = 3,
        data = {date_from: form.find('input[name="date_from"]').val(), date_to: form.find('input[name="date_to"]').val()};

    $.post(adminUrl+'reports/submission_number_by_date_handler', data, function(response){
        drawLineChart(container, title, yAxis, response.xAxis, response.series, type);
    },'json');
}

function initGraphApplicationNumByCat(){
    var container = "#container-application-num-by-cat",
        title = "Application Number by Category",
        seriesName = "Application Number",
        form = $('#form-application-num-by-cat'),
        type = 5;

    $.get(adminUrl+'reports/application_number_by_category_handler', "", function(response){
        drawPieChart(container, title, seriesName, response.data, type);
    },'json');
}

function drawLineChart(container, title, yAxis, xAxis, series, type){
	$(container).highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: title
        },
        xAxis: {
            categories: xAxis
        },
        yAxis: {
            title: {
                text: yAxis
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            var category = this.category,
                                data = {date : category, type : type};

                            $.post(adminUrl+"reports/get_detail", data, function(result){
                                var content = "<table id='table-detail' class='table table-bordered table-striped'>";
                                    content +=      "<thead>";
                                    content +=          "<tr>";
                                    for (var i = 0; i < result.columns.length;i++){
                                        content +=          "<th>"+result.columns[i]+"</th>";
                                    }
                                    content +=          "</tr>";
                                    content +=      "</thead>";
                                    content +=      "<tbody>";
                                    var no = 1;
                                    for (var i = 0; i < result.data.length;i++){
                                        content += "<tr>";
                                        for (var j = 0; j < result.fields.length;j++){
                                            var field = result.fields[j];
                                            if(result.fields[j] == "no"){
                                                content += "<td>"+no+"</td>";
                                            }else if(result.fields[j] == "detail_innovation"){
                                                content += "<td><a href='"+adminUrl+"applications/view_application/"+result.data[i]['id']+"' target='_blank' class='btn btn-default btn-action' title='Detail'><span class='fa fa-search fa-lg'></span></a></td>";
                                            }else{
                                                content += "<td>"+result.data[i][field]+"</td>";
                                            }
                                        }
                                        content += "</tr>";
                                    no++;}
                                    content +=      "</tbody>";
                                    content += "</table>";
                                
                                modalDetail.find(".modal-body").html(content);
                                modalDetail.find('input[name="date"]').val(category);
                                
                                if(result.data.length <= 0){
                                    modalDetail.find('.btn-export').attr('disabled','disabled');
                                }else{
                                    modalDetail.find('.btn-export').removeAttr('disabled');
                                }
                                modalDetail.modal();
                                modalDetail.find('#table-detail').dataTable();

                            },'json');
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                }
            }
        },
        series: series,
        credits: {
        	enabled : false
        }
    });
}

function drawPieChart(container, title, seriesName, data, type){
    $(container).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            var category = this.val_id,
                                data = {category : category, type : type};
                            $.post(adminUrl+"reports/get_detail", data, function(result){
                                var content = "<table id='table-detail' class='table table-bordered table-striped'>";
                                    content +=      "<thead>";
                                    content +=          "<tr>";
                                    for (var i = 0; i < result.columns.length;i++){
                                        content +=          "<th>"+result.columns[i]+"</th>";
                                    }
                                    content +=          "</tr>";
                                    content +=      "</thead>";
                                    content +=      "<tbody>";
                                    var no = 1;
                                    for (var i = 0; i < result.data.length;i++){
                                        content += "<tr>";
                                        for (var j = 0; j < result.fields.length;j++){
                                            var field = result.fields[j];
                                            if(result.fields[j] == "no"){
                                                content += "<td>"+no+"</td>";
                                            }else if(result.fields[j] == "detail_innovation"){
                                                content += "<td><a href='"+adminUrl+"/applications/view_innovation/"+result.data[i]['innovation_id']+"' target='_blank' class='btn btn-default btn-action' title='Detail'><span class='fa fa-search fa-lg'></span></a></td>";
                                            }else{
                                                content += "<td>"+result.data[i][field]+"</td>";
                                            }
                                        }
                                        content += "</tr>";
                                    no++;}
                                    content +=      "</tbody>";
                                    content += "</table>";
                                
                                modalDetail.find(".modal-body").html(content);
                                modalDetail.find('input[name="category"]').val(category);
                                
                                if(result.data.length <= 0){
                                    modalDetail.find('.btn-export').attr('disabled','disabled');
                                }else{
                                    modalDetail.find('.btn-export').removeAttr('disabled');
                                }
                                modalDetail.modal();
                                modalDetail.find('#table-detail').dataTable();
                            },'json');
                        }
                    }
                }
            }
        },
        series: [{
            name: seriesName,
            colorByPoint: true,
            data: data,
        }],
        credits : {
            enabled : false
        }
    });
}

function drawBarChart(container, title, yAxisTitle, categories, data, type){
    $(container).highcharts({
        chart: {
            type: 'column',
            inverted: false
        },
        title: {
            text: title
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxisTitle
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            var category = this.category_id,
                                subcategory = this.subcategory_id,
                                data = {category : category, subcategory : subcategory, type : type};
                            $.post(adminUrl+"reports/get_detail", data, function(result){
                                var content = "<table id='table-detail' class='table table-bordered table-striped'>";
                                    content +=      "<thead>";
                                    content +=          "<tr>";
                                    for (var i = 0; i < result.columns.length;i++){
                                        content +=          "<th>"+result.columns[i]+"</th>";
                                    }
                                    content +=          "</tr>";
                                    content +=      "</thead>";
                                    content +=      "<tbody>";
                                    var no = 1;
                                    for (var i = 0; i < result.data.length;i++){
                                        content += "<tr>";
                                        for (var j = 0; j < result.fields.length;j++){
                                            var field = result.fields[j];
                                            if(result.fields[j] == "no"){
                                                content += "<td>"+no+"</td>";
                                            }else if(result.fields[j] == "detail_innovation"){
                                                content += "<td><a href='"+adminUrl+"/applications/view_innovation/"+result.data[i]['innovation_id']+"' target='_blank' class='btn btn-default btn-action' title='Detail'><span class='fa fa-search fa-lg'></span></a></td>";
                                            }else{
                                                content += "<td>"+result.data[i][field]+"</td>";
                                            }
                                        }
                                        content += "</tr>";
                                    no++;}
                                    content +=      "</tbody>";
                                    content += "</table>";
                                
                                modalDetail.find(".modal-body").html(content);
                                modalDetail.find('input[name="category"]').val(category);
                                modalDetail.find('input[name="subcategory"]').val(subcategory);
                                
                                if(result.data.length <= 0){
                                    modalDetail.find('.btn-export').attr('disabled','disabled');
                                }else{
                                    modalDetail.find('.btn-export').removeAttr('disabled');
                                }
                                modalDetail.modal();
                                modalDetail.find('#table-detail').dataTable();

                            },'json');
                        }
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: data
    });
}