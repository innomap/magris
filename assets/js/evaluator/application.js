var tableApplication = $('#table-application'),
	formApplication = $('#form-application')
	formEvaluation = $('#form-evaluation'),
	formListEvaluation = $('#form-list-evaluation');

var BALANCE_SCORE = 100;

$(function(){
	initDataTableAndFilter();
	initImgFancybox();

	initAlert();
	initView();
	initEvaluation();
	initCancelEvaluation();
	initViewEvaluation();
	initApproveEvaluation();
	initPopover();
});

function initDataTableAndFilter(){
	tableApplication.dataTable();
}

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initView(){
	tableApplication.on('click','.btn-view-application', function(e){
		currentId = $(this).data('id');
		window.location.href = evaluatorUrl+'applications/view/'+currentId;
	});

	if(typeof view_mode != 'undefined'){
		if(view_mode){
			formApplication.find("input[type=text]").prop('readonly', true);
			formApplication.find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
			formApplication.find("button, input[type=file], input[type=submit]").hide();
			$('.btn-download-wrap').hide();
			formApplication.find("button.btn-back").show();
		}
	}
}

function initEvaluation(){
	tableApplication.on('click','.btn-evaluate-application', function(e){
		currentId = $(this).data('id');
		window.open(evaluatorUrl+'applications/evaluation/'+currentId,'_blank');
	});

	calculateTotal();

	$('.point-input').on('click', function(){
		calculateTotal();
	});
}

function check_value(input){
	if(isNaN(parseInt(input))){
		return 0;
	}else{
		return parseInt(input);
	}
}

function calculateTotal(){
	var sumTotal = 0;
	$('.focus-item').each(function(i){
		var percentage = $(this).data('percentage'),
			id = $(this).data('id'),
			total = 0, maxPoint = 0;

		$('.input-'+id+':checked').each(function(j){
			maxPoint += check_value($(this).data('maxpoint'));
		});

		$('.input-'+id+':checked').each(function(j){
			var point = check_value($(this).val());
			total += point;
		});

		var calTotal = total/maxPoint*percentage;
		$('#total-'+id).val(calTotal.toFixed(1));
		sumTotal += calTotal;
	});

	$('#total').val(sumTotal.toFixed(1));
	$('input[name=sum_total]').val(sumTotal.toFixed(1));
}

function initCancelEvaluation(){
	formEvaluation.on('click','.btn-cancel', function(e){
		window.location.href = evaluatorUrl+'applications';
	});
}

function initViewEvaluation(){
	tableApplication.on('click','.btn-view-evaluation', function(e){
		currentId = $(this).data('id');
		window.open(evaluatorUrl+'applications/view_evaluation/'+currentId,'_blank');
	});
	
	if(typeof view_mode != 'undefined'){
		if(view_mode == 1){
			formEvaluation.find("input[type=text]").prop('readonly', true);
			formEvaluation.find("textarea").prop('disabled', true);
			formEvaluation.find("button,input[type=submit]").hide();
			formEvaluation.find("button.btn-cancel").show();
			formEvaluation.find(".popover-item").hide();
			formEvaluation.find(".point-input").prop('disabled',true);
		}
	}
}

function initApproveEvaluation(){
	formListEvaluation.on('submit', function(e){
		var ids = formListEvaluation.find('input[type=checkbox]:checked').length,
			content = formListEvaluation.find('input[type=checkbox]:checked');
		
		$(content).each(function(i){
			if($(this).data('evaluation') == 0){
				e.preventDefault();
				bootbox.alert('Please fill evaluation form before approve project.');
				return false;
			}
		});

		if(ids == 0){
			e.preventDefault();
			bootbox.alert('Please select at least an item to approve.');
			return false;
		}
	});
}

function initPopover(){
	$('.popover-item').popover({
		html:true,
		trigger:'hover',
		container:'body',
		placement: 'right'
	});
}